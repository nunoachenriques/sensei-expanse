# -*- coding: utf-8 -*-

from setuptools import setup, find_packages


with open("README.md") as f:
    README = f.read()

setup(
    name="sensei-expanse-nunoachenriques",
    version="0.3.0",
    author="Nuno A. C. Henriques",
    author_email="nunoachenriques@gmail.com",
    description="All about serving, processing, analysis, and learning from"
                " SensAI Expanse memory and collected data",
    long_description=README,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/nunoachenriques/sensei-expanse",
    license="Apache-2.0",
    classifiers=[
        "Programming Language :: Python :: 3.7",
        "License :: OSI Approved :: Apache Software License",
        "Operating System :: OS Independent",
    ],
    packages=find_packages(
        exclude=("build", "DEPRECATED", "dist", "PG_DUMP"),
        include=("cognition", "memory", "test")
    ),
    package_data={
        "cognition": [
            "pipeline.json",
            "pipeline.ini",  # NOTICE: keep it secret, PRIVATE distribution only
            "prediction.ini"  # NOTICE: keep it secret, PRIVATE distribution only
        ],
        "memory": [
            "secure/self_signed.conf",
            "secure/web_service.ini",  # NOTICE: keep it secret, PRIVATE distribution only
            "secure/web_service.json.secret",  # NOTICE: keep it secret, PRIVATE distribution only
            "secure/web_service.py",
            "secure/web_service.schema.json",
            "secure/web_service.wsgi.py",
            "secure/web_service.wsgi.vhost",
            "source/schema-create.sql"
        ],
        "test": [
            "test_expanse.ini",  # NOTICE: keep it secret, PRIVATE distribution only
            "test_learn_pipeline.copy.gz",
            "test_pipeline_collected.copy.gz",
            "test_prediction_model.copy.gz",
            "test_prediction_pipeline.copy.gz"
        ]
    },
    data_files=[
        ("", [
            "LICENSE",
            "Pipfile",
            "Pipfile.lock",
            "pipeline_do_all.py"
        ])
    ]
)
