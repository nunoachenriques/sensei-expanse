[![Apache 2.0 License](https://img.shields.io/badge/license-Apache%202.0-blue.svg "Apache 2.0 License")][1]

# SensAI Expanse --- The SensAI augmented memory and cognition

[**SensAI Expanse**](https://gitlab.com/nunoachenriques/sensei-expanse) is
the expansion of [**SensAI**](https://gitlab.com/nunoachenriques/sensei) app
resources. Includes repository with historical data, processing algorithms,
services of machine learning towards prediction of emotional valence in context,
i.e., augmented memory and cognition. Moreover, adapt to the diverse human
behaviours reflected in the data set. Do auto discovery on parameters and best
effort to process all eligible data through available learning algorithms.

A simple diagram of module dependency and data flow is depicted below.

```
                                                  [ PostgreSQL ]
                  [...]  <-->  web_service  <-->  [ repository ]
                                    ^                    ^
                                    |                    |
                                    v                    |
               pipeline        prediction                |
   _______________|_________________|_____________       |
   |           |        |       |       |        |       |
analysis  transform  learn  valence  metric  expanse  <--+
```

## Python virtual environment

Use [Pipenv](https://pipenv.readthedocs.io/en/latest/install/) but keep virtual
environment relative to project path, i.e., `/opt/sensei-expanse/.venv/`:

```bash
export PIPENV_VENV_IN_PROJECT=1
```
This is needed to simplify the `memory/secure/` Web service WSGI configuration.
See below why `apache2-dev` is required. 

```shell
# required (install as root)
sudo su -
apt install python3-dev python3.7-dev cython3 pipenv virtualenv git
apt install apache2-dev
exit

# sensei-expanse (install as user)
cd sensei-expanse
pipenv --rm
pipenv install --dev
# hdbscan and tensorflow packages have dependency issues with pipenv
# and are commented out in Pipfile. Install as below:
pipenv run pip install hdbscan~=0.8 tensorflow~=2.0.0
```

## Cognition

All about processing, analysis and learning from SensAI Expanse data towards
prediction of emotional valence in context. [Python](https://www.python.org/)
version 3.7+ is the development language. Open source amazing tools such as
[pandas](https://pandas.pydata.org/),
[scikit-learn](https://scikit-learn.org/) and
[xgboost](https://xgboost.readthedocs.io/) packages are used (see `Pipfile`).

The source code root package `cognition` with the modules:
(a) `pipeline` streamline the whole learning process towards prediction models,
encompass heuristics, i/o and processing decisions;
(b) `analysis` to gather some data aggregations, find eligible entities,
load to some algorithms, obtain insights and produce charts, i.e.,
information from data --- utility class for statistical analysis.
(c) `expanse` for database connection and extraction;
(d) `transform` for data preparation (clean, fix, reconstruct);
(e) `learn` for machine learning and emotion valence changes model;
(f) `valence` as value to class conversion utility;
(g) `metric` as measurement utility (e.g., score normalisation to \[0, 1\];
(h) `prediction` for emotional valence in context prediction using repository
models after `pipeline`.
The extract, transform and load (ETL) process from relational databases
data management inspired the `cognition` conception.
 
The code documentation includes notices about requirements for class
instantiation in several modules. Nevertheless, remember to create required
`prediction.ini` and `pipeline.ini` and fill in with your
[PostgreSQL](https://www.postgresql.org/) database (see [Source](#source))
credentials and other required settings:

```ini
[cache]
root = /opt/sensei-expanse/cache

[database]
name = FILL_IN_HERE
user = FILL_IN_HERE

[logging]
# set info level and show progress bars, false = disable all logging
verbose = true
# set debug level and keep everything displayed (e.g., progress bar leave=True)
debug = true
# Uncomment below for file output. Default to console screen.
output = pipeline.log
```

## Memory

All about data persisting (`memory`) and serving. Includes relational database
schema to implement data source model. Also, a secure Web service
implementation.

### `secure`

The authentication, session, anonymisation layer and Web service. To protect
access and guarantee simultaneously data origin, privacy and anonymity.
Establishes secure (authenticated and encrypted) connection with
[**SensAI**](https://gitlab.com/nunoachenriques/sensei), generates abstract
unique identifier (hash) to anonymise data. Connects to
[**Expanse**](https://gitlab.com/nunoachenriques/sensei-expanse) for augmented
memory and cognition.

**Create** `memory/secure/web_service.ini` and fill in with your
[PostgreSQL](https://www.postgresql.org/) database (see [Source](#source))
credentials and other required settings:

```ini
[cache]
root = /opt/sensei-expanse/cache

[database]
name = FILL_IN_HERE
user = FILL_IN_HERE

[debug]
# set debug mode on/off
mode = false
# DEBUG mode only accepts this user id. The remaining users are NOT served!
user = FILL_IN_HERE

[logging]
# set info level and show (log) additional startup information, false = disable all logging
verbose = true
# Uncomment below for file output. Default to console screen (catch by HTTP server).
# output = web_service.log

[request]
name = expanse_request
id_token_name = oauth_id_token
```

**Create** and **download** credentials from
[Google Identity Platform](https://developers.google.com/api-client-library/python/auth/web-app#creatingcred)
to `memory/secure/web_service.json.secret`. Credentials app + Web service
(see [SensAI](https://gitlab.com/nunoachenriques/sensei)):

 1. Create credentials:
    * OAuth client ID
    * Web application
 2. Name: CREDENTIALS_NAME (e.g., SensAI + Expanse)
 3. Download JSON secrets to `memory/secure/web_service.json.secret`
 4. Client ID will be used to verify ID token.

**Install** as a [Python](https://www.python.org/) application under
[Apache Web Server](https://httpd.apache.org/) using WSGI (Web Server Gateway
Interface, a standard interface between Web server software and Web applications
written in Python). The `mod_wsgi` is included in `Pipfile` (see
[Python virtual environment](#python-virtual-environment)) and compiled when
installed thus `apache2-dev` required. See `LoadModule` directive below
in `default-ssl.conf` file.

```bash
apt install apache2 apache2-dev
a2enmode ssl
a2ensite default-ssl.conf
```

**Configure** certificates for HTTPS. `self_signed.conf` may be
used (**development-only**, do not use in production):

```bash
openssl req -config self_signed.conf -new -x509 -sha256 -newkey rsa:2048 -nodes -keyout self_signed_key.pem.secret -days 365 -out self_signed_crt.pem.secret
openssl x509 -in self_signed_crt.pem.secret -text -noout
```

**Edit** `default-ssl.conf` and adapt to your deployment case:

```conf
SSLCertificateFile /opt/sensei-expanse/memory/secure/self_signed_crt.pem.secret
SSLCertificateKeyFile /opt/sensei-expanse/memory/secure/self_signed_key.pem.secret

LoadModule wsgi_module /opt/sensei-expanse/.venv/lib/python3.7/site-packages/mod_wsgi/server/mod_wsgi-py37.cpython-37m-x86_64-linux-gnu.so

WSGIDaemonProcess web_service user=www-data group=www-data python-path=/opt/sensei-expanse python-home=/opt/sensei-expanse/.venv
WSGIProcessGroup web_service
WSGIApplicationGroup %{GLOBAL}
WSGIScriptAlias /expanse /opt/sensei-expanse/memory/secure/web_service.wsgi.py

<Directory /opt/sensei-expanse/memory/secure>
    Require all granted
</Directory>
```

**Create** cache directory for temporary data and logging:

```bash
mkdir /opt/sensei-expanse/cache
chmod 1777 /opt/sensei-expanse/cache
```

**Test** configuration and **reload**:

```bash
apache2ctl configtest
systemctl reload apache2
```

### `source`

The `*.sql` files are for a RDBMS (relational database) schema implementation.
The SQL dialect is ISO-92 with some PostgreSQL extensions, i.e., PL/pgSQL.
The actual RDBMS is a [PostgreSQL](https://www.postgresql.org/) version 11+ yet
initial testing and running also in a version 10+ instance.

```bash
psql -U expanse sensei-expanse
```
```plsql
psql (11.5 (Debian 11.5-1+deb10u1))
Type "help" for help.

sensei-expanse=> \i schema-create.sql 
```

## Test

The `test` package has unit tests for SensAI `cognition` modules and Expanse
`memory/source` Web service module. 

**Create** `test/test_expanse.ini` and fill in with your database credentials.

```ini
[database]
name = FILL_IN_HERE
user = FILL_IN_HERE
```

**Run** suite (command-line)

```bash
pipenv shell
python -m unittest discover
```

**Run** specific with details (command-line)

```bash
pipenv shell
python -m unittest -v test.test_web_service test.test_learn test.test_transform
```

## Use

After some real data in the repository the below code will stream through the
pipeline towards prediction:
```python
from cognition.pipeline import Pipeline


pipeline = Pipeline()
pipeline.do_all()
```

A script file (`pipeline_do_all.py`) is available for the same purpose. Run:
```bash
pipenv run python pipeline_do_all.py
```

Additionally, for **specific eligible entity** pipeline processing:
```python
from cognition.pipeline import Pipeline


pipeline = Pipeline()
pipeline.expanse.connect(pipeline.db_settings["name"], pipeline.db_settings["user"])
try:
    pipeline.do_analysis()
    entities = [
        "entity_1_id",
        "entity_2_id"
    ]
    eligible = pipeline.analysis.eligible.query("entity in @entities")
    # eligible = pipeline.analysis.eligible.query("entity not in @entities")
    for e in eligible.itertuples():
        pipeline.do_entity(e, force_data=True, force_learn=True)
finally:
    pipeline.expanse.disconnect()
```

On debugging actions it's possible to **force** one **existent entity**
with timestamp **values** for pipeline processing:
```python
import datetime as dt
from collections import namedtuple
from cognition.pipeline import Pipeline


pipeline = Pipeline()
pipeline.expanse.connect(pipeline.db_settings["name"], pipeline.db_settings["user"])
try:
    Entity = namedtuple("Entity", ["entity","age","gender","first","last"])
    e = Entity("entity_id",
               0, 0, dt.datetime(2018, 5, 5, 5, 5, 5), dt.datetime.utcnow())
    pipeline.do_entity(e, force_data=True, force_learn=True)
finally:
    pipeline.expanse.disconnect()
```

## History

It all started in the context of a Doctoral Program in Cognitive
Science at ULisboa in 2016. The need for a functional prototype to be
able to validate the PhD thesis. The first pieces were SensAI and
CogA for the mobile device agent development!

## License

Copyright 2017 Nuno A. C. Henriques [http://nunoachenriques.net/]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

[1]: http://www.apache.org/licenses/LICENSE-2.0.html
