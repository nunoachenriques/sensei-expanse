#  Copyright 2019 Nuno A. C. Henriques [nunoachenriques.net]
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

"""
SensAI Expanse prediction after pipeline process.

See cognition folder at https://gitlab.com/nunoachenriques/sensei-expanse
"""

import configparser
import datetime as dt
import gzip
import logging as log
import os
import sys
from collections import namedtuple
from datetime import datetime
from itertools import zip_longest
from typing import Any, Dict, List, Sequence, Tuple, Union

import dill as pickle
import mgrs
import numpy as np
import pandas as pd

from cognition.expanse import Expanse
from cognition.valence import Valence


class Prediction:
    """
    Main class for obtaining insights after Expanse extracted and
    transformed data from sensors of interest for each entity.
    """

    # Available prediction modes, one function for each.
    MODE = ["all", "best", "estimator", "voting"]

    def __init__(self,
                 db_name: str = None,
                 db_user: str = None):
        """
        Constructor with initial parameters for Prediction.
        Start with a clean local cache, i.e., all data must be fetched from
        database.

        :param db_name: Database name overriding settings file (.ini).
        :param db_user: Database user overriding settings file (.ini).
        """
        # CACHE
        self.CACHE_EXPIRY = dt.timedelta(days=1)
        self.CacheModel = namedtuple("Model", ["name", "score", "estimator"])
        # { entity: { moment: datetime, features: {"f1": 0,  ...}, model: [ Model(...), ... ] }, ... }
        self.cache = dict()
        # CONFIG
        path = os.path.dirname(__file__)
        name = os.path.splitext(os.path.basename(__file__))[0]
        ini_file = os.path.join(path, f"{name}.ini")
        config = configparser.ConfigParser()
        if not config.read(ini_file):
            raise RuntimeError("CONFIGURATION file read FAILED!"
                               f" Hint: create {ini_file} file with proper values.")
        self.cache_root = config["cache"].get("root", ".")
        # LOGGING
        self.debug = config["logging"].getboolean("debug")
        self.verbose = config["logging"].getboolean("verbose")
        if self.verbose:
            if self.debug:
                log_level = log.DEBUG
            else:
                log_level = log.INFO
        else:
            log_level = log.CRITICAL
        log_settings = {
            "format": "%(asctime)s %(module)-20s: %(levelname)-8s %(message)s",
            "datefmt": "%Y-%m-%d %H:%M:%S",
            "level": log_level
        }
        log_file = config["logging"].get("output", None)
        if log_file is not None:
            os.makedirs(self.cache_root, mode=0o755, exist_ok=True)
            filename = os.path.join(self.cache_root, log_file)
            log_settings.update({"filename": filename, "filemode": "w"})
        log.basicConfig(**log_settings)
        # TensorFlow issue on logging | INFO=0, WARNING=1, ERROR=2, FATAL=3
        os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"
        # DATABASE
        self.db_settings = dict()
        self.db_settings["name"] = config["database"]["name"] if db_name is None else db_name
        self.db_settings["user"] = config["database"]["user"] if db_user is None else db_user
        # OBJECTS
        self.expanse = Expanse()
        self.dgg = mgrs.MGRS()  # Discrete Global Grid MGRS id from lat, lon...
        log.debug(f"cache root: {self.cache_root}")

    def cache_update_required(self,
                              entity: str)\
            -> bool:
        """
        Check and report if an update is required for the given entity cached
        data.

        :param entity: Entity id.
        :return: True if local cache data is old or None. False otherwise.
        """
        return (entity not in self.cache
                or (dt.datetime.utcnow() - self.cache[entity]["moment"]) > self.CACHE_EXPIRY)

    def cache_update(self,
                     entity: str)\
            -> bool:
        """
        Update local cache data for the given entity. Records are fetched from
        database.

        :param entity: Entity id.
        :return: True on success, False on failure if no data is returned from
            the database.
        """
        # CONNECT
        if not self.expanse.connect(self.db_settings["name"], self.db_settings["user"]):
            raise RuntimeError("Repository CONNECTION FAILED!"
                               " Hint: check if database server is running and serving.")
        # ESTIMATORS load
        try:
            model_records = self.expanse.model_status(entity)
            pipeline_record = self.expanse.pipeline_status(entity)
        except:
            log.critical(f"{sys.exc_info()[0]} {sys.exc_info()[1]}")
            log.shutdown()
            raise
        finally:
            self.expanse.disconnect()
        # UPDATE all
        log.debug(f"Model records for cache update: {len(model_records) if model_records is not None else 0}")
        log.debug(f"Pipeline record for cache update: {1 if pipeline_record is not None else 0}")
        if model_records is not None and pipeline_record is not None:
            self.cache[entity] = dict()
            self.cache[entity]["moment"] = dt.datetime.utcnow()
            class_count_check = pipeline_record.data_wide_extra["class_count_check"]
            self.cache[entity]["classes"] = [Valence.CLASS_VALUES[i] for i, c in enumerate(class_count_check) if c > 0]
            log.debug(f"Classes available: {self.cache[entity]['classes']}")  # ACTUAL CLASSES for prediction
            df = pd.DataFrame(data=pickle.loads(gzip.decompress(pipeline_record.data_wide)))
            df = df.drop(columns=["valence", "valence_class"])

            # TODO (a) Support "activity" and other optional feature
            #          by auto detection (e.g., column name prefix).
            #      (b) Include options to search predictions on optional features!
            self.cache[entity]["features"] = dict((f, 0) for f in df.columns.to_list())

            self.cache[entity]["model"] = []
            for record in model_records:
                self.cache[entity]["model"].append(
                    self.CacheModel(
                        record.name,
                        record.score,
                        pickle.loads(gzip.decompress(record.estimator))
                    )
                )
            return True
        else:
            return False

    def valence(self,
                entity: str,
                context: Sequence[Tuple[float, float, datetime]],
                mgrs_precision: int,
                mgrs_precision_prefix: str,
                probabilities: bool = False,
                feature_importances: bool = False)\
            -> List[Union[Dict[str, Any], None]]:
        """
        Get emotional valence predictions in context given an entity identifier,
        and required features (geographic coordinates and a timestamp).

        :param entity: Entity id.
        :param context: 2D array with diverse space and time context for
            prediction, i.e., [[latitude, longitude, moment], ...]
            (e.g., [[38.0, 9.0, dt.datetime(2020, 2, 2, 1, 0)], ...]
        :param mgrs_precision: Precision code for id generation: 1=10000,
            2=1000, 3=100 m
        :param mgrs_precision_prefix: Custom prefix in data wide MGRS features
            (e.g., "mgrs_1000")
        :param probabilities: Use predict_proba to obtain the probability of
            the sample for each class (ordered arithmetically at the output).
        :param feature_importances: Request for a mapping of feature name and
            importance value output from the estimator. If not available then
            each feature is assigned with None.
        :return: List of all available predictions (None if unavailable) for
            the given entity on each context data sample.
        """
        log_start_dt = dt.datetime.utcnow()
        log.info(f"Entity {entity[:10]} ...")
        # DATA cache update
        if self.cache_update_required(entity):
            data_ok = self.cache_update(entity)
        else:
            data_ok = (entity in self.cache)
        # TRANSFORM, PREPARE, PREDICT
        result_all = list()
        if data_ok:
            for data in context:
                # FEATURES transform data and prepare samples for prediction
                features = self.cache[entity]["features"].copy()
                mgrs_id = self.dgg.toMGRS(data[0], data[1], MGRSPrecision=mgrs_precision).decode("ASCII")
                mgrs_id_feature = f"{mgrs_precision_prefix}_{mgrs_id}"
                if mgrs_id_feature in features:  # GEO REQUIRED
                    features[mgrs_id_feature] = 1
                    # See pipeline.py for REQUIRED moment FEATURES
                    features["moment_hour"] = data[2].hour
                    features["moment_day_quarter"] = data[2].hour // 6
                    features["moment_dow"] = data[2].weekday()
                    # PREDICT for all estimators
                    result = dict()
                    sample = np.array([list(features.values())])
                    for m in self.cache[entity]["model"]:
                        result[m.name] = dict()
                        result[m.name]["score"] = float(m.score)
                        if m.name == "xgboost.sklearn.XGBClassifier":  # XGBoost requirement
                            sample = pd.DataFrame(sample, columns=list(features.keys()))
                        if probabilities:
                            result[m.name]["prediction_classes"] = self.cache[entity]["classes"]
                            # Restrict 2D array columns to actual number of classes.
                            # Case: NN model output layer is softmax with units=3 (maximum classes)
                            #       and a data set may have less than the maximum.
                            result[m.name]["prediction"] =\
                                m.estimator.predict_proba(sample)[:, :len(self.cache[entity]["classes"])]
                        else:
                            result[m.name]["prediction"] = m.estimator.predict(sample)
                        result[m.name]["prediction"] = result[m.name]["prediction"].astype(float).tolist()
                        if feature_importances:
                            feature_list = list(features.keys())
                            if hasattr(m.estimator, "feature_importances_"):
                                result[m.name]["feature_importances"] =\
                                    dict(zip(feature_list,
                                             m.estimator.feature_importances_.astype(float).flatten()))
                            else:
                                result[m.name]["feature_importances"] =\
                                    dict(zip_longest(feature_list, [None]))
                    result_all.append(result)
                else:
                    # KEEP tracking of NOT available context: result None
                    result_all.append(None)
        else:
            result_all = list([None]) * len(context)
        log.debug(f"Valence prediction: {result_all}")
        log_duration = dt.datetime.utcnow() - log_start_dt
        log.info(f"Entity {entity[:10]} OK! ({log_duration})")
        return result_all

    def valence_best(self,
                     entity: str,
                     context: Sequence[Tuple[float, float, datetime]],
                     mgrs_precision: int,
                     mgrs_precision_prefix: str,
                     probabilities: bool = False,
                     feature_importances: bool = False)\
            -> List[Union[Dict[str, Any], None]]:
        """
        Get emotional valence best score model prediction in context given
        an entity identifier, and required features (geographic coordinates
        and a timestamp).

        :param entity: Entity id.
        :param context: 2D array with diverse space and time context for
            prediction, i.e., [[latitude, longitude, moment], ...]
            (e.g., [[38.0, 9.0, dt.datetime(2020, 2, 2, 1, 0)], ...]
        :param mgrs_precision: Precision code for id generation: 1=10000,
            2=1000, 3=100 m
        :param mgrs_precision_prefix: Custom prefix in data wide MGRS features
            (e.g., "mgrs_1000")
        :param probabilities: Use predict_proba to obtain the probability of
            the sample for each class (ordered arithmetically at the output).
        :param feature_importances: Request for a mapping of feature name and
            importance value output from the estimator. If not available then
            each feature is assigned with None.
        :return: List of best available prediction (None if unavailable) for
            the given entity on each context data sample.
        """
        result_all = self.valence(entity, context,
                                  mgrs_precision, mgrs_precision_prefix,
                                  probabilities, feature_importances)
        result_all_best = list()
        for result in result_all:
            if result is None:
                result_all_best.append(None)
            else:
                result_best = None
                for k, v in iter(result.items()):
                    log.debug(f"result_best: {result_best}")
                    if result_best is None or v["score"] > list(result_best.values())[0]["score"]:
                        result_best = dict({k: v})
                result_all_best.append(result_best)
        log.debug(f"Valence BEST: {result_all_best}")
        return result_all_best

    def valence_estimator(self,
                          estimator: str,
                          entity: str,
                          context: Sequence[Tuple[float, float, datetime]],
                          mgrs_precision: int,
                          mgrs_precision_prefix: str,
                          probabilities: bool = False,
                          feature_importances: bool = False)\
            -> List[Union[Dict[str, Any], None]]:
        """
        Get emotional valence prediction by model in context given an
        entity identifier, and required features (geographic coordinates and
        a timestamp).

        :param estimator: The selected model for prediction.
        :param entity: Entity id.
        :param context: 2D array with diverse space and time context for
            prediction, i.e., [[latitude, longitude, moment], ...]
            (e.g., [[38.0, 9.0, dt.datetime(2020, 2, 2, 1, 0)], ...]
        :param mgrs_precision: Precision code for id generation: 1=10000,
            2=1000, 3=100 m
        :param mgrs_precision_prefix: Custom prefix in data wide MGRS features
            (e.g., "mgrs_1000")
        :param probabilities: Use predict_proba to obtain the probability of
            the sample for each class (ordered arithmetically at the output).
        :param feature_importances: Request for a mapping of feature name and
            importance value output from the estimator. If not available then
            each feature is assigned with None.
        :return: List of given model available prediction
            (None if unavailable) for the given entity on each context data
            sample.
        """
        result_all = self.valence(entity, context,
                                  mgrs_precision, mgrs_precision_prefix,
                                  probabilities, feature_importances)
        result_all_estimator = list()
        for result in result_all:
            if result is not None and estimator in result:
                result_all_estimator.append(result[estimator])
                log.debug(f"Valence ESTIMATOR ({estimator}): {result[estimator]}")
            else:
                result_all_estimator.append(None)
                log.debug(f"Valence ESTIMATOR ({estimator}): {None}")
        return result_all_estimator

    def valence_voting(self,
                       entity: str,
                       context: Sequence[Tuple[float, float, datetime]],
                       mgrs_precision: int,
                       mgrs_precision_prefix: str)\
            -> Sequence[Union[int, None]]:
        """
        Get emotional valence estimators average score weighted prediction in
        context given an entity identifier, and required features (geographic
        coordinates and a timestamp).

        :param entity: Entity id.
        :param context: 2D array with diverse space and time context for
            prediction, i.e., [[latitude, longitude, moment], ...]
            (e.g., [[38.0, 9.0, dt.datetime(2020, 2, 2, 1, 0)], ...]
        :param mgrs_precision: Precision code for id generation: 1=10000,
            2=1000, 3=100 m
        :param mgrs_precision_prefix: Custom prefix in data wide MGRS features
            (e.g., "mgrs_1000")
        :return: List of available estimators average score weighted prediction
            (None if unavailable) for the given entity on each context data
            sample.
        """
        result_all = self.valence(entity, context, mgrs_precision, mgrs_precision_prefix)
        result_all_voting = list()
        for result in result_all:
            if result is None:
                result_all_voting.append(None)
            else:
                result_voting = 0.0
                n_estimators = len(result)
                # EACH ESTIMATOR valence weighted by score as voting result
                for k, v in iter(result.items()):
                    result_voting += v["score"] / n_estimators * np.array(v["prediction"])
                result_all_voting.append(result_voting)
        log.debug(f"Valence VOTING: {result_all_voting}")
        result_all_voting_class = Valence.to_class(result_all_voting)
        log.debug(f"Valence VOTING class: {result_all_voting_class}")
        return result_all_voting_class
