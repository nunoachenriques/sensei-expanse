#  Copyright 2018 Nuno A. C. Henriques [nunoachenriques.net]
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

"""
Application Programming Interface with the Sensei Expanse database (PostgreSQL).
Notice: you must have an accessible PostgreSQL RDBMS with a proper schema as
expected.

See memory/source folder at https://gitlab.com/nunoachenriques/sensei-expanse

Requires: psycopg2-binary
"""

from datetime import datetime, timedelta
from typing import Any, Dict, Sequence, Tuple, Union

import psycopg2 as pg
import psycopg2.extras as pge
from psycopg2.extensions import register_adapter


class Expanse:
    """
    Main class for connecting with the Expanse database: connects,
    disconnects, extracts memory. Use the
    :instance_attribute:`Expanse.connection` after calling
    :method:`Expanse.connect()`.
    """

    # Version 3 (net/nunoachenriques/sensei/persistence/DataCache.java)
    DATABASE_STABLE_SENSEI_VERSION = (0, 1, 24)

    def __init__(self):
        register_adapter(dict, pge.Json)
        self.connection = None
        self.error = None

    def connect(self,
                name: str,
                user: str,
                password: str = None,
                host: str = None,
                port: int = None) -> bool:
        """
        Connect to the database using the given values. Use a NamedTupleCursor
        for any cursor in this connection.

        :return: True on success, False otherwise.
        :raise psycopg2.Error: On error.
        """
        try:
            self.connection = pg.connect(
                cursor_factory=pge.NamedTupleCursor,
                dbname=name, user=user, password=password, host=host, port=port
            )
        except pg.Error as e:
            self.error = e
            self.disconnect()
            raise
        return self.connection is not None

    def disconnect(self):
        """
        Disconnect if connected and set connection to None.
        """
        if self.connection is not None:
            self.connection.close()
            self.connection = None

    # GET *********************************************************************

    def extract(self,
                entity: str,
                moment_begin: datetime,
                moment_end: datetime)\
            -> Union[Sequence[Any], None]:
        """
        Extract data as in select rows from database restricted by entity
        identifier, and bounded by moment_begin and moment_end. Example::

            import datetime as dt
            from cognition.expanse import Expanse

            expanse = Expanse()
            expanse.connect("database_name", "database_user")
            tuples = expanse.extract(
                "xxx",
                dt.datetime(2020, 2, 2, 20, 2, 0, 0),
                dt.datetime(3030, 3, 3, 3, 30, 0, 0)
            )

        :return: A list of (id, moment, moment_utc, sensor_type,
            sensor_attribute, sensor_value, sensor_value_extra) namedTuple for
            all available entities. None if empty.
        """
        with self.connection as connection:
            with connection.cursor() as cursor:
                cursor.callproc("extract_collected", (entity, moment_begin, moment_end))
                if cursor.rowcount > 0:
                    return cursor.fetchall()
                else:
                    return None

    def extract_from_version(self,
                             entity: str,
                             version: Tuple[int, int, int],
                             interval: timedelta)\
            -> Union[Sequence[Any], None]:
        """
        Extract data as in select rows from database restricted by entity
        identifier, and bounded by version and time interval. Example::

            from cognition.expanse import Expanse

            expanse = Expanse()
            expanse.connect("database_name", "database_user")
            tuples = expanse.extract_from_version(
                "xxx", (0, 1, 26), dt.timedelta(minutes=10)
            )

        :return: A list of (id, entity, moment, moment_utc, sensor_type,
            sensor_attribute, sensor_value, sensor_value_extra) namedTuple for
            all available entities. None if empty.
        """
        with self.connection as connection:
            with connection.cursor() as cursor:
                cursor.callproc("moment_from_min_eligible_version", (entity, list(version)))
                if cursor.rowcount == 1:
                    version_moment = cursor.fetchone()[0]
                    version_moment_plus_interval = version_moment + interval
                    cursor.callproc("extract_collected", (entity, version_moment, version_moment_plus_interval))
                    if cursor.rowcount > 0:
                        return cursor.fetchall()
                    else:
                        return None
                else:
                    return None

    def collected(self,
                  sensor_type: str,
                  sensor_attribute: str)\
            -> Union[Sequence[Any], None]:
        """
        Get collected data for all entities restricted by sensor type and
        sensor attribute. Example::

            from cognition.expanse import Expanse

            expanse = Expanse()
            expanse.connect("database_name", "database_user")
            valence_reports = expanse.collected("app.human.smile", "polarity")

        :return: A list of (id, entity, moment, moment_utc, sensor_type,
            sensor_attribute, sensor_value, sensor_value_extra) namedTuple for
            all available entities. None if empty.
        """
        with self.connection as connection:
            with connection.cursor() as cursor:
                cursor.callproc("extract_collected", (sensor_type, sensor_attribute))
                if cursor.rowcount > 0:
                    return cursor.fetchall()
                else:
                    return None

    def entity_data_moments(self)\
            -> Union[Sequence[Any], None]:
        """
        Get data moments (first, last, last_from_now, duration) for all
        available entities. Example::

            from cognition.expanse import Expanse

            expanse = Expanse()
            expanse.connect("database_name", "database_user")
            entity_data_moments = expanse.entity_data_moments()

        :return: A list of (entity, first, last, last_from_now, duration)
            namedTuple for all available entities. None if empty.
        """
        with self.connection as connection:
            with connection.cursor() as cursor:
                cursor.callproc("entity_data_moments")
                if cursor.rowcount > 0:
                    return cursor.fetchall()
                else:
                    return None

    def entity_demographics(self,
                            entity: str = None)\
            -> Union[Sequence[Any], None]:
        """
        Get demographics (age and gender) for all or a specific entity if id is
        given. Example::

            from cognition.expanse import Expanse

            expanse = Expanse()
            expanse.connect("database_name", "database_user")
            all_demographics = expanse.entity_demographics()
            entity_demographics = expanse.entity_demographics("xxx")

        :return: A list of (entity, age, gender) namedTuple for all entities,
            if entity exists and data is available, None otherwise.
        """
        with self.connection as connection:
            with connection.cursor() as cursor:
                if entity is None:
                    cursor.callproc("entity_demographics")
                else:
                    cursor.callproc("entity_demographics", {"entity___p": entity})
                if cursor.rowcount > 0:
                    return cursor.fetchall()
                else:
                    return None

    def entity_demographics_valence_reports_data_moments(self)\
            -> Union[Sequence[Any], None]:
        """
        Get a list of entity demographics, (emotional) valence reports and key
        data moments for each and all available entities. Example::

            from cognition.expanse import Expanse

            expanse = Expanse()
            expanse.connect("database_name", "database_user")
            stats = expanse.entity_demographics_valence_reports_data_moments()

        :return: A list of (entity, age, gender, negative, neutral, positive,
            first, last, last_from_now, duration) namedTuple for all available
            entities. None if empty.
        """
        with self.connection as connection:
            with connection.cursor() as cursor:
                cursor.callproc("entity_demographics_valence_reports_data_moments")
                if cursor.rowcount > 0:
                    return cursor.fetchall()
                else:
                    return None

    def gender_names(self,
                     language: str = "eng")\
            -> Union[Dict[str, str], None]:
        """
        Get gender names dictionary of (id, name) for a specific language.
        Example::

            from cognition.expanse import Expanse

            expanse = Expanse()
            expanse.connect("database_name", "database_user")
            gender_names = expanse.gender_names(language="eng")

        :param language: Language ISO 639-2 alpha-3 (bibliographic) code
            (e.g., "eng" for English).
        :return: A dictionary of (id, name) for all gender options, if
            translation is available, None otherwise.
        """
        with self.connection as connection:
            with connection.cursor() as cursor:
                cursor.callproc("gender_names", {"language___p": language})
                if cursor.rowcount > 0:
                    return dict([(gid, name) for gid, name in cursor.fetchall()])
                else:
                    return None

    def moment_of_version(self,
                          entity: str,
                          version: Tuple[int, int, int])\
            -> Union[datetime, None]:
        """
        Get the moment of the given version restricted by entity identifier.
        Example::

            from cognition.expanse import Expanse

            expanse = Expanse()
            expanse.connect("database_name", "database_user")
            moment = expanse.moment_from_version("xxx", (0, 1, 24))

        :return: datetime value if entity and version exist, None otherwise.
        """
        with self.connection as connection:
            with connection.cursor() as cursor:
                cursor.callproc("moment_from_min_eligible_version", (entity, list(version)))
                if cursor.rowcount == 1:
                    return cursor.fetchone()[0]
                else:
                    return None

    def model_status(self,
                     entity: str)\
            -> Union[Sequence[Any], None]:
        """
        Get all entity learning models, i.e., name, model, score and moment.
        restricted by entity identifier. Example::

            from cognition.expanse import Expanse

            expanse = Expanse()
            expanse.connect("database_name", "database_user")
            try:
                records = expanse.model_status("xxx")
            finally:
                expanse.disconnect()
            if records is not None:
                for record in records:
                    print(record.name)  # named tuple
                    ...

        :return: A list with name, model, score and moment of models
            already set. None if non-existent.
        """
        with self.connection as connection:
            with connection.cursor() as cursor:
                cursor.callproc("model_status", (entity,))
                if cursor.rowcount > 0:
                    return cursor.fetchall()
                else:
                    return None

    def pipeline_status(self,
                        entity: str)\
            -> Union[Any, None]:
        """
        Get the moment, status and data from the last pipeline process with
        success restricted by entity identifier. Example::

            from cognition.expanse import Expanse

            expanse = Expanse()
            expanse.connect("database_name", "database_user")
            pipeline_status = expanse.pipeline_status("xxx")
            # named tuple
            pipeline_status.data_wide_moment

        :return: data_wide, data_wide_shape, data_wide_moment, data_wide_extra,
            status, status_moment, duration if all set. None otherwise.
        """
        with self.connection as connection:
            with connection.cursor() as cursor:
                cursor.callproc("pipeline_status", (entity,))
                if cursor.rowcount == 1:
                    return cursor.fetchone()
                else:
                    return None

    def valence_colours(self)\
            -> Dict[str, Tuple[float, float, float]]:
        """
        Get valence colours dictionary of name and value (name, RGB).
        Example::

            from cognition.expanse import Expanse

            expanse = Expanse()
            expanse.connect("database_name", "database_user")
            valence_colours = expanse.valence_colours()

        :return: A dictionary of (name, (R, G, B)) for all valence colours.
        """
        with self.connection as connection:
            with connection.cursor() as cursor:
                cursor.callproc("valence_colours")
                return dict([(name, tuple(rgb)) for name, rgb in cursor.fetchall()])

    # SET *********************************************************************

    def model_status_update(self,
                            entity: str,
                            name: str,
                            estimator: bytes,
                            score: float,
                            moment: datetime,
                            fit_duration: timedelta,
                            score_duration: timedelta,
                            extra: Dict)\
            -> None:
        """
        Set or update an entity model status from current learning data process.
        Restricted by entity identifier. Example::

            from cognition.expanse import Expanse

            expanse = Expanse()
            expanse.connect("database_name", "database_user")
            expanse.model_status_update(
                "xxx",
                "sklearn.dummy.DummyClassifier", model.pkl.gz,
                0.9378512, dt.datetime(3030, 3, 3, 3, 30, 0, 1),
                dt.timedelta(minutes=2, seconds=23),
                dt.timedelta(minutes=1, seconds=23),
                {"split": [0.8, 0.2]}
            )
        """
        with self.connection as connection:
            with connection.cursor() as cursor:
                cursor.callproc("model_status_update",
                                (entity, name, estimator, score, moment, fit_duration, score_duration, extra))

    def pipeline_status_update(self,
                               entity: str,
                               data_wide: bytes,
                               data_wide_shape: Sequence,
                               data_wide_moment: Union[datetime, None],
                               data_wide_extra: Union[Dict, None],
                               status: str,
                               status_moment: datetime,
                               duration: Union[timedelta, None])\
            -> None:
        """
        Set the moment, status and data from the current pipeline process
        restricted by entity identifier. Example::

            from cognition.expanse import Expanse

            expanse = Expanse()
            expanse.connect("database_name", "database_user")
            expanse.pipeline_status_update(
                "xxx",
                data_wide.pkl.gz, [12, 3], dt.datetime(3030, 3, 3, 3, 30, 0), {"imbalance": 0.0},
                "RUNNING", dt.datetime(3030, 3, 3, 3, 30, 1),
                dt.timedelta(seconds=30)
            )
        """
        with self.connection as connection:
            with connection.cursor() as cursor:
                cursor.callproc("pipeline_status_update",
                                (entity,
                                 data_wide, data_wide_shape, data_wide_moment, data_wide_extra,
                                 status, status_moment, duration))

    def pipeline_status_step_update(self,
                                    entity: str,
                                    data_wide_extra: Union[Dict, None],
                                    status: str,
                                    status_moment: datetime,
                                    duration: Union[timedelta, None])\
            -> None:
        """
        Set the moment of the current status step from the pipeline process
        restricted by entity identifier. Update status and moment only if data
        already set (e.g., last processing step). Example::

            from cognition.expanse import Expanse

            expanse = Expanse()
            expanse.connect("database_name", "database_user")
            expanse.pipeline_status_update(
                "xxx",
                data_wide.pkl.gz, [12, 3], dt.datetime(3030, 3, 3, 3, 30, 0), None,
                "RUNNING", dt.datetime(3030, 3, 3, 3, 30, 1),
                None
            )
            ...
            expanse.pipeline_status_step_update(
                "xxx",
                {"imbalance": 0.0},
                "COMPLETED", dt.datetime(3030, 3, 3, 3, 30, 2)
                dt.timedelta(seconds=30)
            )
        """
        with self.connection as connection:
            with connection.cursor() as cursor:
                cursor.callproc("pipeline_status_step_update",
                                (entity, data_wide_extra, status, status_moment, duration))
