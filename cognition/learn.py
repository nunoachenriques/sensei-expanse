#  Copyright 2018 Nuno A. C. Henriques [nunoachenriques.net]
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

"""
SensAI Expanse data load to some classification algorithms and obtain insights
and prediction information from data. The train and test split must be a time
series continuum, i.e., no shuffle because of continuous events time correlation
(e.g., event n at moment m feel happy, event n+1 at moment ~m location
where the valence is valid). For all sensors of interest.

See cognition folder at https://gitlab.com/nunoachenriques/sensei-expanse
"""

import copy
import datetime as dt
import gzip
import io
import os
import sys
from datetime import timedelta
from multiprocessing import Pool
from typing import Any, Callable, Dict, List, Tuple, Union

import dill as pickle
import h5py
import pandas as pd
import skopt
import tensorflow as tf
from hdbscan import HDBSCAN
from joblib import Memory
from tqdm.auto import tqdm


class Learn:
    """
    Main class for obtaining insights from the Sensei Expanse extracted and
    transformed data of each sensor.
    """

    HDBSCAN_SAMPLE_SIZE_MIN = 3  # Coordinates sample size minimum
    HDBSCAN_AUTO_PARAMS_SAMPLE_MIN = 1000  # Coordinates sample size minimum for auto params

    def __init__(self,
                 debug: bool = False,
                 verbose: bool = True,
                 max_cpu: int = 1,
                 memory_cache_path: str = "/tmp/sensei-expanse/learn"):
        """
        Constructor with initial parameters. Processing cores maximum is the
        minimum between `max_cpu` and `max_cpu_allowed`.

        :param debug: Keep everything displayed (e.g., progress bar leave=True).
        :param verbose: Show progress bar if True.
        :param max_cpu: Maximum number of CPU set by user for multiprocessing.
        :param memory_cache_path: For HDBSCAN cache and others.
        """
        self.debug = debug
        self.verbose = verbose
        self.max_cpu = min(max_cpu, self.max_cpu_allowed())
        self.hdbscan_settings = {
            "memory": Memory(location=os.path.join(memory_cache_path, "hdbscan"), verbose=0),
            "metric": "haversine",
            "allow_single_cluster": True,
            "cluster_selection_method": "eom",
            "core_dist_n_jobs": self.max_cpu
        }
        self.cache_root = memory_cache_path

    @staticmethod
    def max_cpu_allowed(spare: bool = False)\
            -> int:
        """
        Get the number of processing cores allowed to the current process
        leaving one out if `spare` is set.

        :return: Number of CPU the current process is restricted to minus
            one if spare is set.
        """
        return len(os.sched_getaffinity(0)) - (1 if spare else 0)

    def _hdbscan_fit_mcs_range(self,
                               coordinates: Any,
                               ms: int,
                               mcs_range: range)\
            -> Dict[str, List[int]]:
        """
        Calculate the number of clusters for each minimum cluster size in the
        given range and for a specific samples minimum.

        :param coordinates: Two dimensional array with pairs of values
            (e.g., [[0, 0], [1, 1]]) in radians.
        :param ms: The minimum samples needed for a cluster core.
        :param mcs_range: The minimum cluster size range to explore.
        :return: A list with number of clusters for each minimum cluster size
            in the given range.
        """
        cluster_result = []
        for mcs in mcs_range:
            hdbscan = HDBSCAN(min_samples=ms,
                              min_cluster_size=mcs,
                              **self.hdbscan_settings).fit(coordinates)
            cluster_result.append(hdbscan.labels_.max() + 1)
        return dict({str(ms): cluster_result})

    def hdbscan_auto_params(self,
                            coordinates: Any,
                            hdbscan_min_samples: List[int])\
            -> Tuple[int, int, range, Dict[str, List[int]]]:
        """
        Run HDBSCAN clustering algorithm on the different min_samples
        provided in order to find the best min_cluster_size parameter.
        Moreover, uses ``multiprocessing`` one CPU for each ``min_samples``
        value. Number of clusters is obtained using
        ``cluster_result[f"{min_samples_preference}"][mcs_i]``

        :param coordinates: Two dimensional array with pairs of values
            (e.g., [[0, 0], [1, 1]]) in radians.
        :param hdbscan_min_samples: Values to compare (e.g., [10, 20, 50, 100]).
        :return: min_cluster_size, mcs_i, mcs_range, cluster_result
        """
        #  Downsize joblib to a SINGLE JOB due multiprocessing already in use.
        self.hdbscan_settings["core_dist_n_jobs"] = 1
        # RUN clustering:
        #  1. Different minimum samples values needed for a cluster core.
        #  2. Minimum cluster size ranging from 2 to 10% of sample size
        #     (100% if sample size < required minimum) and step from 1 to 10.
        sample_size = len(coordinates)
        if sample_size < Learn.HDBSCAN_SAMPLE_SIZE_MIN:
            raise RuntimeWarning(f"Sample size of {sample_size} < {Learn.HDBSCAN_SAMPLE_SIZE_MIN}")
        mcs_range_start = 2
        mcs_range_stop = max(int(sample_size * 0.10), min(sample_size, Learn.HDBSCAN_AUTO_PARAMS_SAMPLE_MIN))
        mcs_range_step = min(mcs_range_stop // mcs_range_start, 10)
        mcs_range = range(mcs_range_start, mcs_range_stop, mcs_range_step)  # min cluster size
        cluster_result = {}
        # FIT (sync multiprocessing, lock for all to terminate)
        with Pool(processes=self.max_cpu) as pool:
            pool_result = pool.starmap(
                func=self._hdbscan_fit_mcs_range,
                iterable=[(coordinates, ms, mcs_range) for ms in hdbscan_min_samples]
            )
        pool_result = pool_result.__iter__()
        for ms in hdbscan_min_samples:
            cluster_result[str(ms)] = pool_result.__next__()[str(ms)]
        # FIND (best) min cluster size
        #  Where the several minimum samples results cross first (all equal).
        df = pd.DataFrame(data=cluster_result)
        mcs_i = 0
        min_cluster_size = sys.maxsize
        for mcs_i, intersected in df.nunique(axis=1).eq(1).items():
            min_cluster_size = mcs_range[mcs_i]
            if intersected:
                break
        return min_cluster_size, mcs_i, mcs_range, cluster_result

    def hdbscan_geo_cluster(self,
                            coordinates: Any,
                            hdbscan_min_samples: int,
                            hdbscan_min_cluster_size: int,
                            hdbscan_prediction_data: bool)\
            -> Tuple[Any, int]:
        """
        Run HDBSCAN clustering algorithm on the provided parameters
        in order to find the best location clusters for the given
        coordinates.

        :param coordinates: Two dimensional array with pairs of values
            (e.g., [[0, 0], [1, 1]]) in radians.
        :param hdbscan_min_samples: Minimum samples needed for a cluster core.
        :param hdbscan_min_cluster_size: Minimum cluster size in samples.
        :param hdbscan_prediction_data: True or False, with or without.
        :return: HDBSCAN clustering results, total of clusters (excl. -1).
        """
        geo_cluster = HDBSCAN(min_samples=hdbscan_min_samples,
                              min_cluster_size=hdbscan_min_cluster_size,
                              prediction_data=hdbscan_prediction_data,
                              **self.hdbscan_settings).fit(coordinates)
        return geo_cluster, geo_cluster.labels_.max() + 1

    def geo_clusters(self,
                     coordinates: Any,
                     hdbscan_min_samples: List[int],
                     hdbscan_min_samples_pref: int,
                     hdbscan_auto_params_callback: Union[Callable[[int, int, range, Dict[str, List[int]]], Any], None]
                     = None)\
            -> Tuple[Any, int]:
        """
        Aggregate hyper parameter tuning and HDBSCAN coordinates fit in one
        function call. See :ref:`hdbscan_auto_params` and
        :ref:`hdbscan_geo_cluster` for detailed information. Prediction data
        is NOT generated.

        :param coordinates: Two dimensional array with pairs of values
            (e.g., [[0, 0], [1, 1]]) in radians.
        :param hdbscan_min_samples: List of possible minimum samples needed
            for a cluster core.
        :param hdbscan_min_samples_pref: Preference used for minimum samples.
        :param hdbscan_auto_params_callback: Auto params final result callback.
        :return: HDBSCAN clustering results, total of clusters (excl. -1).
        """
        with tqdm(total=2,
                  leave=self.debug,
                  disable=not self.verbose) as bar:
            bar.set_description_str("HDBSCAN geo clusters")
            bar.set_postfix_str("Auto parameters discovering...")
            hap_mcs, hap_mcs_i, hap_mcs_range, hap_result = self.hdbscan_auto_params(
                coordinates=coordinates,
                hdbscan_min_samples=hdbscan_min_samples
            )
            if hdbscan_auto_params_callback is not None:
                hdbscan_auto_params_callback(hap_mcs, hap_mcs_i, hap_mcs_range, hap_result)
            auto_params = f"min_samples={hdbscan_min_samples_pref}, min_cluster_size={hap_mcs}"
            bar.update()
            bar.set_postfix_str(f"Clustering... {auto_params}")
            cluster_model, cluster_n = self.hdbscan_geo_cluster(
                coordinates=coordinates,
                hdbscan_min_samples=hdbscan_min_samples_pref,
                hdbscan_min_cluster_size=hap_mcs,
                hdbscan_prediction_data=False
            )
            bar.update()
            bar.set_postfix_str(f"cluster_n={cluster_n} {auto_params}")
        return cluster_model, cluster_n

    @staticmethod
    def eligible_split(x_train,
                       y_train,
                       splitter: Union[Callable, None],
                       n_classes: int = 1,
                       n_split_min: int = 2,
                       n_split_max: int = 5)\
            -> Union[object, None]:
        """
        Check data for required number of classes and number of samples per
        class greater or equal to number of split. The split search is within
        given bounds of n_split_min and n_split_max.

        Return the splitter with the discovered maximum number of splits
        (within bounds) or None.

        :param x_train: Features train data from X y split.
        :param y_train: Target train data from X y split.
        :param splitter: A sklearn.model_selection split mechanism.
        :param n_classes: Minimum number of classes to require.
        :param n_split_min: Minimum number of splits.
        :param n_split_max: Maximum number of splits.
        :return: A splitter with the discovered maximum number of splits
            (within given bounds) on success. None on search failure.
        """
        eligible_splitter = None
        y_class = y_train.reset_index().reindex(columns=["valence_class"])
        for n_splits in range(n_split_max, n_split_min - 1, -1):
            eligible_class_values = \
                not y_train.reset_index().groupby(by=["valence_class"]).nunique()["index"].lt(n_splits).any()
            if eligible_class_values:
                eligible_class_n = False
                splitter_instance = splitter(n_splits=n_splits)
                for i, (train_index, validate_index) in enumerate(splitter_instance.split(X=x_train, y=y_train)):
                    eligible_class_n = (len(y_class.loc[train_index].groupby(by=["valence_class"])) >= n_classes)
                    if not eligible_class_n:
                        break
                if eligible_class_n:
                    eligible_splitter = splitter(n_splits=n_splits)
                    break
        return eligible_splitter

    @staticmethod
    def fit_callback_early_stop(search_result: Dict)\
            -> Union[bool, None]:
        """
        Scikit-Optimize Bayesian search cross validation early stop.
        Simplistic (fallible) method using patience as three steps with the
        same score result.

        :param search_result: Data from the optimizer.
        :return: True on early stop. None otherwise.
        """
        n_digits = 8
        search_score = round(search_result["fun"], n_digits)
        search_patience = 3
        if len(search_result["func_vals"]) >= search_patience:
            early_stop = True
            for search_result_score in search_result["func_vals"][:-(search_patience + 1):-1]:
                if search_score != round(search_result_score, n_digits):
                    early_stop = False
            if early_stop:
                return True

    def model_estimator(self,
                        estimator: Dict[str, Any],
                        scoring: Callable,
                        x_train: Any,
                        x_test: Any,
                        y_train: Any,
                        y_test: Any,
                        cv_splitter: Union[object, None],
                        early_stop: bool = False)\
            -> Tuple[object, float, float, timedelta, timedelta]:
        """
        Auto-tune hyper parameters with cross validation for specific model.
        Do learn with best model parameters. Using Bayesian optimization from
        scikit-optimize package as a replacement for the expensive scikit-learn
        GridSearchCV::

         https://scikit-optimize.github.io/notebooks/sklearn-gridsearchcv-replacement.html

        :param estimator: Algorithm "default", "search" and "search_n_iter" parameters.
        :param scoring: Metric for scoring GridSearchCV model fit.
        :param x_train: Features train data from X y split.
        :param x_test: Features test data from X y split.
        :param y_train: Target train data from X y split.
        :param y_test: Target test from X y split.
        :param cv_splitter: Cross validation splitter function.
        :param early_stop: If True then use Learn.fit_callback_early_stop().
        :return: Best model (e.g., sklearn object) after parameters search,
            fit score, test score, time to fit model, time to test model.
        """
        name = estimator["default"].__class__.__qualname__  # Class name
        with tqdm(total=2,
                  leave=self.debug,
                  disable=not self.verbose) as bar:
            bar.set_description_str(f"{name} X {len(x_train)}/{len(x_test)}")
            # SEARCH for BEST HYPERPARAMETERS and LEARN
            bar.set_postfix_str("Fitting (incl. searching hyperparameters)...")
            log_start_dt = dt.datetime.utcnow()
            estimator_search = skopt.BayesSearchCV(
                estimator=estimator["default"],
                search_spaces=estimator["search"],
                scoring=scoring,
                n_iter=self.max_cpu if estimator["search_n_iter"] is None else estimator["search_n_iter"],
                n_jobs=self.max_cpu,
                iid=False,
                refit=True,
                cv=cv_splitter,
                verbose=0
            )
            if early_stop:
                estimator_search.fit(x_train, y_train, callback=Learn.fit_callback_early_stop)
            else:
                estimator_search.fit(x_train, y_train)
            best_estimator = estimator_search.best_estimator_
            fit_score = estimator_search.best_score_
            log_fit_duration = dt.datetime.utcnow() - log_start_dt
            bar.update()
            # TEST SCORE after train towards best FIT
            bar.set_postfix_str("Testing best model...")
            log_start_dt = dt.datetime.utcnow()
            test_score = best_estimator.score(x_test, y_test)
            log_test_duration = dt.datetime.utcnow() - log_start_dt
            bar.update()
            bar.set_postfix_str(f"{scoring} score={test_score:.2f}")
        return best_estimator, fit_score, test_score, log_fit_duration, log_test_duration

    @staticmethod
    def estimator_full_name(estimator: object)\
            -> str:
        """
        Get the fully qualified path + class name for the given model instance.

        :param estimator: Instance of the model.
        :return: Fully qualified path + class name for given model instance.
        """
        return ".".join([estimator.__module__, estimator.__class__.__qualname__])

    @staticmethod
    def estimator_to_serialization(estimator: object)\
            -> Tuple[str, bytes]:
        """
        Convert a scikit-learn and cognition.learn.KerasClassifier object to
        serialized bytes (pickled and GZipped) for storage anywhere
        (e.g., database).

        :param estimator: scikit-learn and cognition.learn.KerasClassifier.
        :return: A tuple with estimator full name
            (e.g., sklearn.dummy.DummyClassifier) and serialized bytes
            (pickled and GZipped).
        """
        estimator_name = Learn.estimator_full_name(estimator)
        estimator_pkl = pickle.dumps(estimator)
        estimator_pkl_gz = gzip.compress(estimator_pkl, compresslevel=6)
        return estimator_name, estimator_pkl_gz

    @staticmethod
    def estimator_from_serialization(estimator: bytes)\
            -> object:
        """
        Convert a scikit-learn and cognition.learn.KerasClassifier estimator
        serialized bytes (pickled and GZipped) to the original object.

        :param estimator: Serialized bytes (pickled and GZipped).
        :return: scikit-learn or cognition.learn.KerasClassifier original object.
        """
        estimator_pkl = gzip.decompress(estimator)
        estimator_object = pickle.loads(estimator_pkl)
        return estimator_object

    @staticmethod
    def neural_network_model(input_shape: Tuple,
                             units: int,
                             dropout_rate: float)\
            -> tf.keras.models.Sequential:
        """
        A neural network model using TensorFlow Keras API. The model is built
        using Sequential class and compiled with an optimizer and loss function.
        It's ready to be used in KerasClassifier build_fn. Layers::

         Dense(ReLU) > Dropout >  Dense(ReLU) > Dropout > Dense(Softmax)

        ReLU (rectifier) used as activation, Dropout as fast regularization and
        Softmax to assure an output of a probability distribution over predicted
        output classes.

        It's compiled with Adam optimizer and SparseCategoricalCrossentropy
        loss function::

         optimizer=tf.keras.optimizers.Adam(amsgrad=True)
         loss=tf.keras.losses.SparseCategoricalCrossentropy()

        :param input_shape: For the first layer, should be (x_train_n_features,).
        :param units: Number of nodes, output space dimension.
        :param dropout_rate: The fraction of units to drop as regularization
            method to avoid overfitting.
        :return: A TensorFlow Keras API Sequential model built and compiled with
            optimizer and loss ready for scikit-learn wrapper.
        """
        model = tf.keras.models.Sequential(
            layers=[
                tf.keras.layers.Dense(units, activation="relu", input_shape=input_shape, name="first"),
                tf.keras.layers.Dropout(dropout_rate, name="first_dropout"),
                tf.keras.layers.Dense(units, activation="relu", name="second"),
                tf.keras.layers.Dropout(dropout_rate, name="second_dropout"),
                tf.keras.layers.Dense(3, activation="softmax", name="output")
            ],
            name="MLPClassifier"
        )
        model.compile(
            optimizer=tf.keras.optimizers.Adam(amsgrad=True),
            loss=tf.keras.losses.SparseCategoricalCrossentropy(),
            metrics=["accuracy"]
        )
        return model


class KerasClassifier(tf.keras.wrappers.scikit_learn.KerasClassifier):
    """
    TensorFlow Keras API neural network classifier.

    Workaround the tf.keras.wrappers.scikit_learn.KerasClassifier serialization
    issue using BytesIO and HDF5 in order to enable pickle dumps.

    Adapted from: https://github.com/keras-team/keras/issues/4274#issuecomment-519226139
    """

    def __getstate__(self):
        state = self.__dict__
        if "model" in state:
            model = state["model"]
            model_hdf5_bio = io.BytesIO()
            with h5py.File(model_hdf5_bio, mode="w") as file:
                model.save(file)
            state["model"] = model_hdf5_bio
            state_copy = copy.deepcopy(state)
            state["model"] = model
            return state_copy
        else:
            return state

    def __setstate__(self, state):
        if "model" in state:
            model_hdf5_bio = state["model"]
            with h5py.File(model_hdf5_bio, mode="r") as file:
                state["model"] = tf.keras.models.load_model(file)
        self.__dict__ = state
