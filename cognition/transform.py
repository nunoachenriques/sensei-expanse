#  Copyright 2018 Nuno A. C. Henriques [nunoachenriques.net]
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

"""
SensAI Expanse memory transformations such as long to wide, reconstruction of
discarded data to save memory, and more. For all sensors of interest. Moreover,
transform over reconstructed data to be ready for classification and prediction
algorithms.

See cognition folder at https://gitlab.com/nunoachenriques/sensei-expanse

Requires: numpy, pandas
"""

import itertools
from datetime import datetime
from typing import Sequence, Tuple, Union, Any

import numpy as np
import pandas as pd
from numpy import ndarray
from pandas import DataFrame


class Ready:
    """
    Main class for transforming the Sensei Expanse extracted data of each
    sensor to be ready for further processing (e.g., clustering algorithms).
    """

    def __init__(self,
                 tuples: Union[Sequence[Any], None],
                 columns_name: Union[Sequence[str], None] = None,
                 valence_fill: int = 360):
        """
        Create the Reconstruct object with the given parameters. Example::

            import cognition.expanse as ce
            from cognition.transform import Ready

            e = ce.Expanse()
            e.connect(...)
            extracted = e.extract(...)
            ready = Ready(tuples=extracted)

        :param tuples: Data tuples from Expanse database.
        :param columns_name: The column names from the tuples query.
        :param valence_fill: Valence value fill limit in minutes.
        """
        self.reconstruct = Reconstruct(tuples=tuples, columns_name=columns_name, valence_fill=valence_fill)

    def moment_last_data(self)\
            -> Union[datetime, None]:
        """
        Get timestamp of the last data tuple.

        :return: UTC timestamp of last data tuple. None if empty.
        """
        if self.reconstruct.df.empty:
            return None
        else:
            return self.reconstruct.df.max(axis=0)["moment_utc"]

    def geopositioning(self,
                       freq: str = None,
                       fill_limit: Union[int, None] = None)\
            -> Tuple[DataFrame, ndarray]:
        """
        Helper to get the geopositioning data ready for information extraction.
        Location data is processed in multi sets: lat, lon exclusive and lat,
        lon exclusive in radians. Example::

            from cognition.transform import Ready

            ready = Ready(...)
            geo_latlon, geo_latlon_rad = ready.geopositioning()

        :param freq: (re)sampling (time) frequency for sensor alignment
            (None = Reconstruct.SAMPLING_FREQUENCY_M).
        :param fill_limit: Value (e.g., 900=15 min if 1s freq); 0 for no fill;
            None = Reconstruct.VALENCE_FILL_LIMIT_M.
        :return: The sensor data (geo_latlon, geo_latlon_rad) ready for
            information extraction. Tuple with empty (DataFrame, ndarray)
            if no eligible data.
        """
        freq_default = self.reconstruct.SAMPLING_FREQUENCY_M if freq is None else freq
        fill_limit_default = self.reconstruct.MEMORY_FILL_LIMIT_M if fill_limit is None else fill_limit
        # LOCATION reconstruct (resample freq)
        geopositioning = self.data(sensor="app.sensor.geopositioning",
                                   freq=freq_default,
                                   fill_limit=fill_limit_default)
        # GEO lat lon extract, clean, prepare, ...
        geo_latlon = pd.DataFrame()
        geo_latlon_rad = np.array([])
        # if EMPTY then FAIL FAST
        if not geopositioning.empty:
            geo_latlon = (
                geopositioning
                .rename(columns={"geopositioning_latitude": "latitude", "geopositioning_longitude": "longitude"})
                .reindex(columns=["latitude", "longitude"])
                .dropna(axis=1, how="all")  # Drop columns with all NaN
                .dropna(how="any")  # Drop rows with at least one NaN
            )
            # ...to radians float 64
            if not geo_latlon.empty:
                geo_latlon_rad = np.radians(
                    geo_latlon
                    .reset_index()  # Remove "moment" from index
                    .reindex(columns=["latitude", "longitude"])
                    .astype(dtype="float64", copy=True)
                )
        return geo_latlon, geo_latlon_rad

    def valence(self,
                freq: str = None,
                fill_limit: Union[int, None] = None)\
            -> DataFrame:
        """
        Helper to get the emotional valence sensor data ready for information
        extraction. Actual sensors: smile, chat polarity, tweet polarity.
        Also an extra sensor with mean value of all. Example::

            from cognition.transform import Ready

                ready = Ready(...)
                valence_all_sensors = ready.valence()

        :param freq: (re)sampling (time) frequency for sensor alignment
            (None = self.SAMPLING_FREQUENCY_M).
        :param fill_limit: Value (e.g., 900=15 min if 1s freq); 0 for no fill;
            None = self.VALENCE_FILL_LIMIT_M.
        :return: The sensor data ready for information extraction.
            An empty DataFrame if no data.
        """
        freq_default = self.reconstruct.SAMPLING_FREQUENCY_M if freq is None else freq
        fill_limit_default = self.reconstruct.VALENCE_FILL_LIMIT_M if fill_limit is None else fill_limit
        # Ground truth
        smile = self.data(sensor="app.human.smile", freq=freq_default, fill_limit=fill_limit_default)
        # Sentiment analysis by VADER (chat)
        chat = self.data(sensor="app.human.chat", attr="polarity", freq=freq_default, fill_limit=fill_limit_default)
        # Sentiment analysis by VADER (tweet)
        tweet = self.data(sensor="app.human.tweet", freq=freq_default, fill_limit=fill_limit_default)
        # EMOTIONAL VALENCE (NEW) SENSOR after average all smile, chat, tweet
        valence_all_sensors = pd.DataFrame()
        reindex_columns = []
        # if EMPTY then FAIL FAST
        if not smile.empty:
            smile = smile.drop(smile.query(f"smile_polarity < {self.reconstruct.VALENCE_MIN}"
                                           f" or smile_polarity > {self.reconstruct.VALENCE_MAX}").index)
            if not smile.empty:
                valence_all_sensors = smile
                reindex_columns.append("smile_polarity")
        if not chat.empty:
            chat = chat.drop(chat.query(f"chat_polarity < {self.reconstruct.VALENCE_MIN}"
                                        f" or chat_polarity > {self.reconstruct.VALENCE_MAX}").index)
            if not chat.empty:
                valence_all_sensors = valence_all_sensors.join(other=chat, on="moment")
                reindex_columns.append("chat_polarity")
        if not tweet.empty:
            tweet = tweet.drop(tweet.query(f"tweet_polarity < {self.reconstruct.VALENCE_MIN}"
                                           f" or tweet_polarity > {self.reconstruct.VALENCE_MAX}").index)
            if not tweet.empty:
                valence_all_sensors = valence_all_sensors.join(other=tweet, on="moment")
                reindex_columns.append("tweet_polarity")
        if not valence_all_sensors.empty:
            valence_all_sensors = valence_all_sensors.reindex(columns=reindex_columns)
            valence_all_sensors = valence_all_sensors.assign(valence=valence_all_sensors.mean(axis=1))
        return valence_all_sensors

    def data(self,
             sensor: str,
             attr: str = None,
             freq: str = None,
             fill_limit: Union[int, None] = None)\
            -> DataFrame:
        """
        Helper to get the sensor data ready for information extraction.
        Reconstruct memory for the specified sensor, i.e., clean and fix junk
        (e.g., duplicate) data in some cases. Fill values forward after an up
        sampling with freq using fill_limit (same limit used in SensAI to force
        a value after memory saving from repeated). Fill `NaN` after limit and
        before next value. Example::

            from cognition.transform import Ready

            ready = Ready(...)
            activity = ready.data(sensor="app.sensor.activity")

        Database Sensei Expanse sensor type identifiers::

            sensei-expanse=> select distinct(sensor_type) as sensor from collected order by sensor;
                           sensor
            ------------------------------------
            android.sensor.light
            android.sensor.linear_acceleration
            android.sensor.magnetic_field
            android.sensor.proximity
            app.agent.empathy
            app.agent.feed
            app.agent.notification
            app.human.chart
            app.human.chat
            app.human.main
            app.human.smile
            app.human.tweet
            app.sensor.activity
            app.sensor.battery
            app.sensor.connectivity
            app.sensor.geopositioning
            app.sensor.wifiscan
            (17 rows)

        :param sensor: Sensor full qualified name (e.g., app.sensor.tweet)
        :param attr: Sensor attribute name to filter (e.g., polarity).
            None = all attributes.
        :param freq: (re)sampling (time) frequency for sensor alignment
            (None = Reconstruct.SAMPLING_FREQUENCY_M).
        :param fill_limit: None for no limit;
            Value (e.g., 900=15 min if 1s freq); 0 for no fill.
        :return: The sensor data ready for information extraction.
             An empty DataFrame if no data.
       """
        rdf = pd.DataFrame()
        # EMPTY START (fail fast)
        if self.reconstruct.df.empty:
            return rdf
        # PREPARE
        split: Sequence[str] = sensor.rsplit(".", 1)
        prefix: str = split[0]
        name: str = split[1]
        # FREQUENCY (for resample) FORCE default if None
        freq_default = self.reconstruct.SAMPLING_FREQUENCY_M if freq is None else freq
        # PROCESS
        if prefix == "app.agent":  # empathy, feed, notification
            rdf = self.reconstruct.app_agent(sensor=sensor, freq=freq_default)
        elif sensor == "app.human.chart" \
                or (sensor == "app.human.chat" and attr == "transition") \
                or sensor == "app.human.main":
            rdf = self.reconstruct.app_transition(sensor=sensor, freq=freq_default)
        elif sensor == "app.human.chat" and attr == "polarity":
            rdf = self.reconstruct.chat_polarity(sensor=sensor, freq=freq_default, fill_limit=fill_limit)
        elif sensor == "app.human.smile":
            rdf = self.reconstruct.smile(sensor=sensor, freq=freq_default, fill_limit=fill_limit)
        elif sensor == "app.human.tweet":
            rdf = self.reconstruct.tweet_polarity(sensor=sensor, freq=freq_default, fill_limit=fill_limit)
        elif sensor == "app.sensor.activity":
            rdf = self.reconstruct.activity(sensor=sensor, freq=freq_default)
        elif sensor == "app.sensor.wifiscan":
            rdf = self.reconstruct.wifiscan(sensor=sensor, freq=freq_default)
        else:  # sensor == android.sensor.*, app.sensor.{battery, connectivity, geopositioning}
            rdf = self.reconstruct.android(sensor=sensor, freq=freq_default, fill_limit=fill_limit)
        if not rdf.empty:
            rdf.columns = [f"{name}_{i}" for i in rdf.columns]  # rename
        return rdf


#             *****************************************************************
# RECONSTRUCT *****************************************************************
#             *****************************************************************


class Reconstruct:
    """
    Main class for reconstructing the Sensei Expanse extracted data of each
    sensor.
    """

    def __init__(self,
                 tuples: Union[Sequence[Any], None],
                 columns_name: Union[Sequence[str], None] = None,
                 valence_fill: int = 360):
        """
        Create the DataFrame that will be used in all transformations.
        Input the resulting data tuples and the column names.
        It will mash in a pandas.DataFrame that is used for all the
        transformation functions. Column "entity" is removed.

        CogSci evidence that emotional `valence_fill` duration is variable from
        seconds to hours::

          Verduyn, P., Delaveau, P., Rotgé, J.-Y., Fossati, P., & Van Mechelen, I. (2015).
          Determinants of Emotion Duration and Underlying Psychological and Neural Mechanisms.
          Emotion Review, 7(4), 330–335. https://doi.org/10.1177/1754073915590618

        :param tuples: Data tuples from Expanse database.
        :param columns_name: The column names from the tuples query.
        :param valence_fill: Valence value fill limit in minutes.
        """
        self.SAMPLING_FREQUENCY_M: str = "1min"  # Default resampling frequency in minutes
        # Threshold from device resources (memory) saving
        # https://gitlab.com/nunoachenriques/sensei/
        #  blob/master/app/src/main/java/net/nunoachenriques/sensei/Sensei.java
        self.MEMORY_FILL_LIMIT_M: int = 15  # Minutes
        # Last emotional valence report valid for `valence_fill` time (e.g., 6 hours) unless reported otherwise
        self.VALENCE_FILL_LIMIT_M: int = valence_fill
        # Polarity range limit
        # https://gitlab.com/nunoachenriques/sensei/
        #  blob/master/app/src/main/java/net/nunoachenriques/sensei/emotion/Polarity.java
        self.VALENCE_MIN: float = -1.0
        self.VALENCE_MAX: float = 1.0
        if columns_name is None:
            self.df = pd.DataFrame(data=tuples)
        else:
            self.df = pd.DataFrame(data=tuples, columns=pd.Index(data=columns_name))

    @staticmethod
    def long_to_wide(data: DataFrame,
                     extra_attr: Sequence[str] = None)\
            -> DataFrame:
        """
        Reconstruct the data from long to wide. Raise error if duplicate data.
        Extra value attribute is processed and joined as a column. This is
        overfitted to SensAI Expanse sensor data case where there is
        "sensor_value" and also a "sensor_value_extra" may be of interest.
        Example::

            df = transform.long_to_wide(data=df)

            df = transform.long_to_wide(
                data=df,
                extra_attr=["c_value_name", "b_value_name"]
            )

        :param data: Data in long format, i.e., columns attributes in rows.
        :param extra_attr: Attribute name with extra value of interest.
        :return: Data transformed from long to wide, all attributes in columns.
            Extra values will be suffix with "_extra".
        """
        data_copy = data.copy(deep=True)
        data_wide = data_copy.pivot(index="moment", columns="sensor_attribute", values="sensor_value")
        if extra_attr is not None:
            data_extra_wide = data.pivot(index="moment", columns="sensor_attribute", values="sensor_value_extra")
            data_wide = data_wide.join(other=data_extra_wide[extra_attr], on="moment", rsuffix="_extra")
        return data_wide

    def activity(self,
                 sensor: str,
                 freq: str)\
            -> DataFrame:
        """
        Reconstruct memory for the specified sensor, i.e., clean and fix junk
        (e.g., duplicate) data in some cases and fills in `NaN` using
        fill_limit (same limit used in SensAI to force a value after memory
        saving from repeated). Fill after an up sampling with freq.

        Version: 0.1.26-

        Problem: ENTER and EXIT, transitioning between activities, with equal
                 timestamps. Also, Google's bug of repeated events.

        Solution: ADD one millisecond to timestamp for each event constituted by
                  three rows (attributes). Iterate until no more duplicates.

        :param sensor: The sensor (unique identifier) name.
        :param freq: (re)sampling (time) frequency for sensor alignment.
        :return: The sensor memory reconstructed, i.e., cleaned, fixed,
            resampled, long to wide. An empty DataFrame if no data.
        """
        df_fixed: DataFrame = (
            self.df
            .query(f"sensor_type == ['{sensor}']")
            .sort_values(by=["id"], ascending=True)
        )
        rdf = pd.DataFrame()
        # EMPTY START (fail fast)
        if df_fixed.empty:
            return rdf
        # CLEAN missing triple pattern
        pattern = ["type", "transition", "elapsed"]
        pattern_len = len(pattern)
        idx = [df_fixed.index[(i - pattern_len):i]  # index labels
               for i in range(pattern_len, len(df_fixed))  # pattern range
               if all(df_fixed["sensor_attribute"].iloc[i - pattern_len:i] == pattern)]  # if pattern matches
        df_fixed = df_fixed.loc[list(itertools.chain.from_iterable(idx)), :]

        # TIMESTAMP duplicates fix
        def moment_fixed(data, frequency) -> Any:
            return data + np.repeat(np.array(range(len(data) // frequency), dtype="timedelta64[ms]"), repeats=frequency)

        while df_fixed.duplicated(subset=["moment", "sensor_attribute", "sensor_value"]).any():
            df_fixed = df_fixed.assign(moment=lambda df: moment_fixed(df["moment"], 3))
        # PIVOT long to wide (if not EMPTY)
        if not df_fixed.empty:
            rdf = (
                self.long_to_wide(data=df_fixed, extra_attr=["transition", "type"])
                    .resample(rule=freq)
                    .fillna(method="ffill")
            )
        return rdf

    def android(self,
                sensor: str,
                freq: str,
                fill_limit: Union[int, None],
                columns: str = "sensor_attribute")\
            -> DataFrame:
        """
        Reconstruct memory for the specified sensor, i.e., clean and fix junk
        (e.g., duplicate) data in some cases. Fill values forward after an up
        sampling with freq using fill_limit (same limit used in SensAI to force
        a value after memory saving from repeated). Fill `NaN` after limit and
        before next value.

        :param sensor: The sensor (unique identifier) name.
        :param freq: (re)sampling (time) frequency for sensor alignment.
        :param fill_limit: None for no limit;
            Value (e.g., 900=15 min if 1s freq); 0 for no fill.
        :param columns: Data column value to be used as pivot new columns.
        :return: The sensor data reconstructed, i.e., cleaned, fixed,
            resampled, long to wide.
        """
        rdf: DataFrame = (
            self.df
                .query(f"sensor_type == ['{sensor}']")
                .drop_duplicates(subset=["moment", "sensor_attribute", "sensor_value"], keep="last")
                .pivot(index="moment", columns=columns, values="sensor_value")
                .resample(rule=freq)
                .fillna(method="ffill", limit=fill_limit)
        )
        return rdf

    def app_agent(self,
                  sensor: str,
                  freq: str)\
            -> DataFrame:
        """
        Reconstruct memory for the specified sensor, i.e., clean and fix junk
        data (e.g., duplicate). Up sample with freq:

         * `app.agent.feed`: fill with `NaN` between valid values.
         * other: keep first value.

        :param sensor: The sensor (unique identifier) name.
        :param freq: (re)sampling (time) frequency for sensor alignment.
        :return: The sensor memory reconstructed, i.e., cleaned, fixed,
            resampled.
        """
        rdf: DataFrame = (
            self.df
                .query(f"sensor_type == ['{sensor}']")
                .rename(columns={"sensor_value": "value", "sensor_value_extra": "value_extra"})
                .reindex(columns=["moment", "value", "value_extra"])
                .set_index(keys="moment")
        )
        if sensor == "app.agent.feed":
            return rdf.resample(rule=freq).fillna(method="ffill")
        else:
            return rdf.reindex(columns=["moment", "value"]).resample(rule=freq).first()

    def app_transition(self,
                       sensor: str,
                       freq: str)\
            -> DataFrame:
        """
        Reconstruct memory and fills in `NaN` between transitions (ENTER,
        EXIT). The fill is done over an up sampling with freq.

        :param sensor: The sensor (unique identifier) name.
        :param freq: (re)sampling (time) frequency for sensor alignment.
        :return: The app activity transition memory reconstructed.
        """
        rdf: DataFrame = (
            self.df.query(f"sensor_type == ['{sensor}'] and sensor_attribute == ['transition']")
                   .rename(columns={"sensor_value": "transition", "sensor_value_extra": "transition_name"})
                   .reindex(columns=["moment", "transition", "transition_name"])
                   .set_index(keys="moment")
                   .resample(rule=freq)
                   .fillna(method="ffill")
        )
        return rdf

    def chat_polarity(self,
                      sensor: str,
                      freq: str,
                      fill_limit: int)\
            -> DataFrame:
        """
        Reconstruct memory for the specified sensor, i.e., clean and fix junk
        (e.g., duplicate) data in some cases and fills in `NaN` values using
        fill_limit (ad hoc value). Fill forward after an up sampling with freq.

        :param sensor: The sensor (unique identifier) name.
        :param freq: (re)sampling (time) frequency for sensor alignment.
        :param fill_limit: None for no limit;
            Value (e.g., 900=15 min if 1s freq).
        :return: The sensor memory reconstructed, i.e., cleaned, fixed,
            resampled.
        """
        rdf: DataFrame = (
            self.df.query(f"sensor_type == ['{sensor}'] and sensor_attribute == ['polarity']")
                   .drop_duplicates(subset=["moment", "sensor_value"], keep="last")
                   .rename(columns={"sensor_value": "polarity", "sensor_value_extra": "language"})
                   .reindex(columns=["moment", "polarity", "language"])
                   .set_index(keys="moment")
                   .resample(rule=freq)
                   .fillna(method="ffill", limit=fill_limit)
        )
        return rdf

    def smile(self,
              sensor: str,
              freq: str,
              fill_limit: int)\
            -> DataFrame:
        """
        Reconstruct memory for the specified sensor, i.e., clean and fix junk
        (e.g., duplicate) data in some cases and fills in `NaN` values using
        fill_limit (ad hoc value). Fill forward after an up sampling with freq.

        :param sensor: The sensor (unique identifier) name.
        :param freq: (re)sampling (time) frequency for sensor alignment.
        :param fill_limit: None for no limit;
            Value (e.g., 900=15 min if 1s freq).
        :return: The sensor memory reconstructed, i.e., cleaned, fixed,
            resampled, long to wide.
        """
        rdf: DataFrame = (
            self.df.query(f"sensor_type == ['{sensor}']")
                   .drop_duplicates(subset=["moment", "sensor_attribute"], keep="last")
                   .rename(columns={"sensor_value": "polarity"})
                   .reindex(columns=["moment", "polarity"])
                   .set_index(keys="moment")
                   .resample(rule=freq)
                   .fillna(method="ffill", limit=fill_limit)
        )
        return rdf

    def tweet_polarity(self,
                       sensor: str,
                       freq: str,
                       fill_limit: int)\
            -> DataFrame:
        """
        Reconstruct memory for the specified sensor, i.e., clean and fix junk
        (e.g., duplicate) data in some cases and fills in NaN values using
        fill_limit (ad hoc value). Fill forward after an up sampling with freq.

        :param sensor: The sensor (unique identifier) name.
        :param freq: (re)sampling (time) frequency for sensor alignment.
        :param fill_limit: None for no limit;
            Value (e.g., 900=15 min if 1s freq).
        :return: The sensor memory reconstructed, i.e., cleaned, fixed,
            resampled.
        """
        rdf: DataFrame = (
            self.df.query(f"sensor_type == ['{sensor}'] and sensor_attribute == ['polarity']")
                   .drop_duplicates(subset=["moment", "sensor_value"], keep="last")
                   .rename(columns={"sensor_value": "polarity", "sensor_value_extra": "language"})
                   .reindex(columns=["moment", "polarity", "language"])
                   .set_index(keys="moment")
                   .resample(rule=freq)
                   .fillna(method="ffill", limit=fill_limit)
        )
        return rdf

    def wifiscan(self,
                 sensor: str,
                 freq: str)\
            -> DataFrame:
        """
        Reconstruct memory for the specified sensor, i.e., clean and fix junk
        (e.g., duplicate) data in some cases and fills in NaN values wide, fill
        forward previous value without limit. Process::

         1. Select all non-zero values.
         2. Remove eventual duplicates: same moment, access point (AP)
            more than once.
         3. Keep only more frequent: greater or equal to 10 times.
         4. Long to wide on moment x AP with RSSI values and NaN fill
            where missing.
         5. Resample with freq value, fill wide and fill forward.

        :param sensor: The sensor (unique identifier) name.
        :param freq: (re)sampling (time) frequency for sensor alignment.
        :return: The sensor memory reconstructed, i.e., cleaned, fixed,
            resampled, long to wide.
        """
        rdf: DataFrame = (
            self.df.query(f"sensor_type == ['{sensor}'] and sensor_attribute == 'rssi' and sensor_value != 0")
                   .drop_duplicates(subset=["moment", "sensor_value_extra"], keep="first")
        )
        count = rdf["sensor_value_extra"].value_counts()
        rdf = (
            rdf[rdf["sensor_value_extra"].isin(count[count >= 10].index)]
            .pivot(index="moment", columns="sensor_value_extra", values="sensor_value")
            .resample(rule=freq)
            .fillna(method="ffill")
        )
        return rdf

    # def _deprecated__reconstruct_activity_special(self) \
    #         -> DataFrame:
    #     """
    #     JOIN TRANSITION PAIRS, i.e., ENTER and EXIT of same activity.
    #
    #      Clean:
    #       1. First row being EXIT.
    #       2. Consecutive (junk) two or more ENTER.
    #       3. Elapsed in nanoseconds. Convert duration to seconds.
    #
    #      Result (sample):
    #                             duration type   type_name
    #     moment
    #
    #     2018-10-18 21:03:55.382      184  7.0     WALKING
    #
    #     2018-10-18 21:06:59.981      185  3.0       STILL
    #
    #     2018-10-18 21:10:05.247      475  7.0     WALKING
    #
    #     2018-10-18 21:18:01.100      396  0.0  IN_VEHICLE
    #
    #     2018-10-18 21:24:35.378      323  7.0     WALKING
    #
    #     2018-10-18 21:29:58.450      448  3.0       STILL
    #
    #     2018-10-18 21:37:26.993       62  7.0     WALKING
    #
    #     2018-10-18 21:38:29.006     6510  3.0       STILL
    #
    #     2018-10-18 23:26:59.501      113  7.0     WALKING
    #
    #     2018-10-18 23:28:53.013    31526  3.0       STILL
    #
    #     :return: The result in a DataFrame.
    #     """
    #     a_data = []
    #     previous_t = None
    #     for t in self.activity(sensor="app.sensor.activity", rule="1S").reset_index().itertuples():
    #         if t.transition == 1.0:  # EXIT (=1.0)
    #             if previous_t is not None and previous_t.transition == 0.0:  # ENTER (=0.0)
    #                 # Duration: nanoseconds (10^-9) to seconds (10^0) -> 10^9
    #                 duration = (t.elapsed - previous_t.elapsed) // 1000000000
    #                 a_data.append((previous_t.moment, duration, t.type, t.type_extra))
    #         previous_t = t
    #     return pd.DataFrame.from_records(memory=a_data,
    #                                      index="moment",
    #                                      columns=["moment", "duration", "type", "type_name"])
