#  Copyright 2019 Nuno A. C. Henriques [nunoachenriques.net]
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

"""
SensAI Expanse pipeline to process data and learn from it. Concept::

 do_all: do_analysis, do_entity inside eligible entities iterator.

  1. do_analysis: extract statistics, analysis, list all eligible entities,
     repeat on_demand (e.g., periodic call)

 do_entity iterator: check for eligible data time interval, status cache,
 calculate extract loop from available data timestamps.

  2. do_extract: extract data from an entity at an eligible time interval.
     Moreover, update cache status before extract.
  3. do_transform: reconstruct data due to the app storage savings,
     resample data, align sensors at a proper time granularity (e.g., 1 min).
  4. do_learn_location: find best HDBSCAN parameters, location clusters,
     map to real world discrete global grid using MGRS.
  5. do_data_wide: data for further processing, one-hot encoding,
     and wide alignment. Update cache status with data.

     Loop between 2. and 5. until all available data is ready.

  6. do_learn_model: includes do_data_x_y_split_train_test (split) on X y ready
     for machine learning, hyper parameters and N splits auto discovery,
     fit classifiers, learn model, update model cache.

See cognition folder at https://gitlab.com/nunoachenriques/sensei-expanse
"""

import configparser
import datetime as dt
import gzip
import json
import logging as log
import math
import os
import sys
from datetime import datetime, timedelta
from typing import Any, Callable, Dict, List, Sequence, Tuple, Union

import dill as pickle
import mgrs
import numpy as np
import pandas as pd
import xgboost
from pandas import DataFrame, Series
from sklearn import dummy
from sklearn import linear_model
from sklearn import metrics
from sklearn import model_selection
from tqdm.auto import tqdm

from cognition.analysis import Analysis
from cognition.expanse import Expanse
from cognition.learn import Learn, KerasClassifier
from cognition.transform import Ready
from cognition.valence import Valence


class Pipeline:
    """
    Main class for obtaining insights from the Expanse extracted and
    transformed data from sensors of interest for each entity. The agent
    has the ability to analyze and adapt pipeline processing for different
    human behaviours on moving and sharing emotional valence. This adaptation
    is restricted by some boundaries in order to obtain viable data for the
    machine learning algorithms. The general process is depicted below::

     Data analysis -> Eligible entity
                         iterator
                             |  eligible data time interval
                             v
                        Cache status
                             |
                             v
                          Extract
                        Reconstruct
                         Transform
                           Loop      update cache status
                             |
                             v
                        Learn model  update cache status
                             |
                             v
                        Cache status
    """

    # DEFINE
    NAME = "SensAI Expanse"
    DATA_INTERVAL_MIN = dt.timedelta(days=7)  # New data collected at least...
    DATA_INTERVAL_MAX = dt.timedelta(days=28)  # Process at most...
    DATA_SPLIT_TEST = 0.2
    DATA_SPLIT_TRAIN = 1.0 - DATA_SPLIT_TEST
    RANDOM_SEED = 42
    # CLUSTERING
    HDBSCAN_MIN_SAMPLES_PREF = 10
    HDBSCAN_MIN_SAMPLES_SEARCH = [1, HDBSCAN_MIN_SAMPLES_PREF, 100]
    # MGRS https://en.wikipedia.org/wiki/Military_Grid_Reference_System
    MGRS_PRECISION_100 = 3  # 3 = 100 m sq. side (MGRS)
    MGRS_PRECISION_1000 = 2  # 2 = 1000 m sq. side (MGRS)
    MGRS_PRECISION_10000 = 1  # 1 = 10000 m sq. side (MGRS)
    MGRS_PRECISION = MGRS_PRECISION_1000  # Elected due to reasonable cell size and number of cells
    MGRS_PRECISION_PREFIX = "mgrs_1000"
    # PROCESS
    STATUS = ["processing", "ready"]

    def __init__(self,
                 db_name: str = None,
                 db_user: str = None,
                 data_interval_min: timedelta = None,
                 data_interval_max: timedelta = None):
        """
        Constructor with initial parameters for Analysis, Expanse,
        Transform, Learn. Create ``self.transformed`` which will include the
        ``self.do_transform()`` processing results.

        :param db_name: Database name overriding settings file (.ini).
        :param db_user: Database user overriding settings file (.ini).
        :param data_interval_min: Minimum time interval for collected data to
            be eligible for processing. Overrides Pipeline.DATA_INTERVAL_MIN
        :param data_interval_max: Maximum time interval for collected data
            batch processing. Overrides Pipeline.DATA_INTERVAL_MAX
        """
        # DATA INTERVAL OVERRIDE
        if data_interval_min is not None:
            Pipeline.DATA_INTERVAL_MIN = data_interval_min
        if data_interval_max is not None:
            Pipeline.DATA_INTERVAL_MAX = data_interval_max
        # CONFIG
        path = os.path.dirname(__file__)
        name = os.path.splitext(os.path.basename(__file__))[0]
        ini_file = os.path.join(path, f"{name}.ini")
        config = configparser.ConfigParser()
        if not config.read(ini_file):
            raise RuntimeError("CONFIGURATION file read FAILED!"
                               f" Hint: create {ini_file} file with proper values.")
        self.cache_root = config["cache"].get("root", ".")
        # LOGGING
        self.debug = config["logging"].getboolean("debug")
        self.verbose = config["logging"].getboolean("verbose")
        if self.verbose:
            if self.debug:
                log_level = log.DEBUG
            else:
                log_level = log.INFO
        else:
            log_level = log.CRITICAL
        log_settings = {
            "format": "%(asctime)s %(module)-20s: %(levelname)-8s %(message)s",
            "datefmt": "%Y-%m-%d %H:%M:%S",
            "level": log_level
        }
        log_file = config["logging"].get("output", None)
        if log_file is not None:
            os.makedirs(self.cache_root, mode=0o755, exist_ok=True)
            filename = os.path.join(self.cache_root, log_file)
            log_settings.update({"filename": filename, "filemode": "w"})
        log.basicConfig(**log_settings)
        # TensorFlow issue on logging | INFO=0, WARNING=1, ERROR=2, FATAL=3
        os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"
        # DATABASE
        self.db_settings = dict()
        self.db_settings["name"] = config["database"]["name"] if db_name is None else db_name
        self.db_settings["user"] = config["database"]["user"] if db_user is None else db_user
        # SETTINGS for algorithms and more...
        with open(os.path.join(path, f"{name}.json")) as settings:
            self.settings = json.load(settings)
        # DATA
        self.transformed = dict()  # Share several results (e.g., self.do_transform)
        self.expanse = Expanse()
        self.analysis = None
        self.transform = None
        self.learn = Learn(verbose=self.verbose,
                           max_cpu=Learn.max_cpu_allowed(spare=True),
                           memory_cache_path=os.path.join(self.cache_root, "learn"))
        self.moment_last_data = None
        self.max_cpu = self.learn.max_cpu
        log.debug(f"max_cpu: {self.max_cpu} | db_name: {self.db_settings['name']}  | cache_root: {self.cache_root}")
        # GPU + TensorFlow
        if self.debug:
            Pipeline.debug_tensorflow_gpu()

    @staticmethod
    def debug_tensorflow_gpu()\
            -> bool:
        """
        Check if TensorFlow is built with GPU support and then if it is
        available.

        :return: True if built with support and GPU available. False otherwise.
        """
        import tensorflow as tf
        if tf.test.is_built_with_cuda():
            if tf.test.is_gpu_available():
                log.debug(f"GPU device name: {tf.test.gpu_device_name()}")
                return True
            else:
                log.debug("GPU NOT AVAILABLE or WITHOUT required CUDA capability!")
                return False
        else:
            log.debug("TensorFlow build WITHOUT GPU support!")
            return False

    @staticmethod
    def entity_short_id(entity_id: str,
                        suffix: Union[str, None] = "...")\
            -> str:
        """
        Show only first ten characters of entity id with optional trailing.

        :param entity_id: Entity long id string.
        :param suffix: Short identifier result trailing characters.
        :return: Entity id ten-character prefix with optional trailing characters.
        """
        return f"{entity_id[:10]}{suffix if suffix is not None else ''}"

    def status_step_to_cache(self,
                             entity: str,
                             status: str,
                             duration: Union[timedelta, None],
                             data_wide_extra: Union[Dict, None])\
            -> None:
        """
        Update pipeline cache status with current data processing step and
        moment.

        :param entity: Entity id.
        :param status: Name of the current status step.
        :param duration: Pipeline process duration.
        :param data_wide_extra: Extra information (e.g., {"lrid": 0.0}).
        """
        if not self.transformed["data_wide"].empty:  # ONLY after first data update, i.e., status_to_cache()
            self.expanse.pipeline_status_step_update(
                entity=entity,
                data_wide_extra=data_wide_extra,
                status=status,
                status_moment=dt.datetime.utcnow(),
                duration=duration
            )
        else:
            self.status_to_cache(
                entity=entity,
                status=status,
                duration=duration,
                data_wide_extra=data_wide_extra
            )

    def status_to_cache(self,
                        entity: str,
                        status: str,
                        duration: Union[timedelta, None] = None,
                        data_wide_extra: Union[Dict, None] = None)\
            -> None:
        """
        Update pipeline cache with current data wide pickle and GZip.
        Include current status step and moment.

        :param entity: Entity id.
        :param status: Name of the current status step.
        :param duration: Pipeline process duration.
        :param data_wide_extra: Extra information (e.g., {"lrid": 0.0}).
        """
        # DATA WIDE pickle and GZip
        data_wide_pkl = pickle.dumps(self.transformed["data_wide"])
        data_wide_pkl_gz = gzip.compress(data_wide_pkl, compresslevel=6)
        # DATA WIDE shape (n_tuples, n_features + 2_targets)
        #  Includes: "valence" and "valence_class" as targets to drop on split
        data_wide_shape = list(self.transformed["data_wide"].shape)
        # UPDATE cache (an open connection is expected)
        self.expanse.pipeline_status_update(
            entity=entity,
            data_wide=data_wide_pkl_gz,
            data_wide_shape=data_wide_shape,
            data_wide_moment=self.moment_last_data,
            data_wide_extra=data_wide_extra,
            status=status,
            status_moment=dt.datetime.utcnow(),
            duration=duration
        )

    def status_from_cache(self,
                          entity: str)\
            -> None:
        """
        Get data wide in cache if available and update pipeline start
        and moment of the last data processed.

        :param entity: Entity id.
        """
        # GET cache (an open connection is expected)
        status = self.expanse.pipeline_status(entity)
        if status is None:
            self.moment_last_data = None
            self.transformed["data_wide"] = pd.DataFrame()
            self.transformed["data_wide_extra"] = None
        else:
            self.moment_last_data = status.data_wide_moment
            self.transformed["data_wide"] = pd.DataFrame(data=pickle.loads(gzip.decompress(status.data_wide)))
            self.transformed["data_wide_extra"] = status.data_wide_extra

    def model_to_cache(self,
                       entity: str,
                       estimator: object,
                       score: float,
                       fit_duration: timedelta,
                       score_duration: timedelta,
                       extra: Dict)\
            -> None:
        """
        Update model cache with current model after processing.
        Include score, current moment, fully qualified model name.

        :param entity: Entity id.
        :param estimator: Estimator object after do_learn_model().
        :param score: Estimator best score.
        :param fit_duration: Model fit process duration.
        :param score_duration: Model score process duration.
        :param extra: Extra data such as X y split weights (e.g., [0.8, 0.2]).
        """
        # SERIALIZE model
        estimator_name, estimator_bytes = Learn.estimator_to_serialization(estimator)
        # UPDATE cache (an open connection is expected)
        self.expanse.model_status_update(
            entity=entity,
            name=estimator_name,
            estimator=estimator_bytes,
            score=score,
            moment=dt.datetime.utcnow(),
            fit_duration=fit_duration,
            score_duration=score_duration,
            extra=extra
        )

    def do_all(self,
               hdbscan_auto_params_cb:
                   Union[Callable[[int, int, range, Dict[str, List[int]]], Any], None] = None,
               hdbscan_geo_clusters_cb:
                   Union[Callable[[Any, Any, int], Any], None] = None,
               force_data: bool = False,
               force_learn: bool = False)\
            -> None:
        """
        Do the whole process. Connect to the persistent (long-term) memory.
        Extract entity list and statistics. Additional data columns are
        generated (e.g., age range) useful to statistics. Iterate over existing
        entities to extract data, transform with reconstruction and resampling,
        prepare for machine learning, load into learning algorithms,
        get information, update intelligence model::

                  |-> do_entity() pipeline over eligible entities
                  |
         Analysis | Extract > Transform > Learn

        :param hdbscan_auto_params_cb: Callback for clustering auto params.
        :param hdbscan_geo_clusters_cb: Callback for location clustering.
        :param force_data: Ignore pipeline status cache and process entity available data.
        :param force_learn: Ignore data wide requirements and redo ML processing.
        """
        log_start_dt = dt.datetime.utcnow()
        log.info(f"All ... force_data={force_data} force_learn={force_learn}")
        # CONNECT
        if not self.expanse.connect(self.db_settings["name"], self.db_settings["user"]):
            raise RuntimeError("Repository CONNECTION FAILED!"
                               " Hint: check if database server is running and serving.")
        try:
            # ANALYSIS
            self.do_analysis()
            # ENTITY PIPELINE
            #  Filter by REQUIRED duration minimum
            eligible = self.analysis.eligible.query(f"duration >= '{Pipeline.DATA_INTERVAL_MIN}'")
            log.info(f"All eligible entities: {len(eligible)}")
            with tqdm(eligible.sort_values(by=["last_from_now"], ascending=False).itertuples(),
                      total=len(eligible),
                      disable=not self.verbose) as eligible_bar:
                eligible_bar.set_description_str("Eligible")
                # LOOP all eligible ENTITIES sorted by fresh data
                for e in eligible_bar:
                    eligible_bar.set_postfix_str(e.entity)
                    self.do_entity(e, hdbscan_auto_params_cb, hdbscan_geo_clusters_cb, force_data, force_learn)
                    eligible_bar.update()
                    eligible_bar.set_postfix_str("")
        except:
            log.critical(f"{sys.exc_info()[0]} {sys.exc_info()[1]}")
            log.shutdown()
            raise
        finally:
            self.expanse.disconnect()
            log.info(f"All OK! ({dt.datetime.utcnow() - log_start_dt})")

    def do_analysis(self)\
            -> None:
        """
        Extract indicators from persistent (long-term) memory,
        prepare some statistics. Additional data columns are generated
        (e.g., age range). Populates ``self.analysis``::

         ...
         from cognition.pipeline import Pipeline


         pipeline = Pipeline()
         if not pipeline.expanse.connect(pipeline.db_settings["name"],
                                         pipeline.db_settings["user"]):
             raise RuntimeError("Repository CONNECTION FAILED!"
                                " Hint: check if database server is serving.")
         try:
             pipeline.do_analysis()
             analysis = pipeline.analysis
         finally:
             pipeline.expanse.disconnect()
         ...
        """
        log_start_dt = dt.datetime.utcnow()
        with tqdm(total=5,
                  disable=not self.verbose) as bar:
            bar.set_description_str("Analysis")
            log.info(f"{bar.desc} ...")
            # EXTRACT LOV
            bar.set_postfix_str("LOV extracting...")
            log.debug(f"{bar.desc} {bar.postfix}")
            gender_name = self.expanse.gender_names(language="eng")
            valence_colour = self.expanse.valence_colours()
            bar.update()
            # EXTRACT entities demographics
            bar.set_postfix_str("Demographics extracting...")
            log.debug(f"{bar.desc} {bar.postfix}")
            demographics = pd.DataFrame(data=self.expanse.entity_demographics())
            bar.update()
            # EXTRACT entities data moments
            bar.set_postfix_str("Data moments extracting...")
            log.debug(f"{bar.desc} {bar.postfix}")
            data_moments = pd.DataFrame(data=self.expanse.entity_data_moments())
            bar.update()
            # EXTRACT entities valence reports
            bar.set_postfix_str("Valence reports extracting...")
            log.debug(f"{bar.desc} {bar.postfix}")
            valence_reports = pd.DataFrame(data=self.expanse.collected("app.human.smile", "polarity"))
            bar.update()
            # ANALYSIS
            bar.set_postfix_str("Instantiate...")
            log.debug(f"{bar.desc} {bar.postfix}")
            self.analysis = Analysis(
                demographics=demographics,
                moments=data_moments,
                collected=valence_reports,
                gender_name=gender_name,
                valence_colour=valence_colour
            )
            bar.update()
            bar.set_postfix_str("")
            log.info(f"{bar.desc} OK! ({dt.datetime.utcnow() - log_start_dt})")

    def do_entity(self,
                  eligible,
                  hdbscan_auto_params_cb:
                      Union[Callable[[int, int, range, Dict[str, List[int]]], Any], None] = None,
                  hdbscan_geo_clusters_cb:
                      Union[Callable[[Any, Any, int], Any], None] = None,
                  force_data: bool = False,
                  force_learn: bool = False)\
            -> None:
        """
        From given eligible entity, check cache for up to date data,
        extract next data, transform with reconstruction and resampling,
        prepare for machine learning, data wide status and last data moment
        persistence update, load into learning algorithms, get information,
        update intelligence model::
        
         Extract > Transform > Learn
        
        :param eligible: A named tuple with entity, age, gender, first, last...
        :param hdbscan_auto_params_cb: Callback for clustering auto params.
        :param hdbscan_geo_clusters_cb: Callback for location clustering.
        :param force_data: Ignore pipeline status cache and process entity available data.
        :param force_learn: Ignore data wide requirements and redo ML processing.
        """
        log_start_dt = dt.datetime.utcnow()
        log.info(f"Entity {Pipeline.entity_short_id(eligible.entity)}")
        # Force data extraction OR Get data status from cache
        if force_data:
            self.moment_last_data = None
            self.transformed["data_wide"] = pd.DataFrame()
            self.transformed["data_wide_extra"] = None
        else:
            self.status_from_cache(eligible.entity)
        # Pipeline CACHE STATUS update
        self.status_to_cache(eligible.entity, self.STATUS[0])  # PROCESSING
        # Force machine learning for all models
        if force_learn and "valence_class" in self.transformed["data_wide"].columns:
            new_data = True
        else:
            new_data = False
        # LOOP over all available (new) DATA per entity in batches (save memory resources)
        log_prefix = "Extract+Reconstruct+Transform"
        extract_interval_ok = False
        moment_begin, moment_end = self.do_extract_moment_begin_end(eligible, self.moment_last_data)
        if moment_begin is not None:
            if self.moment_last_data is not None:
                data_t_interval = eligible.last - self.moment_last_data
            else:
                data_t_interval = eligible.last - moment_begin
            # LOOP only if data collected interval is Pipeline.DATA_INTERVAL_MIN at least
            if data_t_interval >= Pipeline.DATA_INTERVAL_MAX:
                extract_loop_total =\
                    data_t_interval // Pipeline.DATA_INTERVAL_MAX +\
                    (1 if (data_t_interval % Pipeline.DATA_INTERVAL_MAX) >= Pipeline.DATA_INTERVAL_MIN else 0)
            elif data_t_interval >= Pipeline.DATA_INTERVAL_MIN:
                extract_loop_total = 1
            else:
                extract_loop_total = 0
            log.debug(f"{log_prefix} iterations: {extract_loop_total} (total={data_t_interval})")
            if extract_loop_total > 0:
                extract_interval_ok = True
                with tqdm(total=extract_loop_total,
                          disable=not self.verbose) as bar:
                    bar.set_description_str(log_prefix)
                    while self.do_extract(eligible, moment_begin, moment_end):
                        self.moment_last_data = moment_end  # Unless below processing is successful
                        if self.do_transform():
                            self.do_learn_location(Pipeline.HDBSCAN_MIN_SAMPLES_SEARCH,
                                                   Pipeline.HDBSCAN_MIN_SAMPLES_PREF,
                                                   hdbscan_auto_params_cb,
                                                   hdbscan_geo_clusters_cb)
                            if self.do_data_wide():
                                self.moment_last_data = self.transform.moment_last_data()
                                new_data = True
                        # UPDATE ALWAYS for the case of zero tuples and move forward
                        self.status_to_cache(eligible.entity, self.STATUS[0])  # PROCESSING
                        bar.update()
                        moment_begin, moment_end = self.do_extract_moment_begin_end(eligible, self.moment_last_data)
                    bar.set_postfix_str(f"Data wide={self.transformed['data_wide'].shape}"
                                        f" {Pipeline.entity_short_id(eligible.entity)}"
                                        f" {eligible.age} {eligible.gender}")
                    log.info(f"{bar.desc} {bar.postfix}")
        if not extract_interval_ok:
            log.info(f"{log_prefix}: time INTERVAL NOT ELIGIBLE")
        # LEARN model
        data_wide_extra = self.transformed["data_wide_extra"]  # Previous data or None
        if new_data:
            data_wide_extra = self.do_learn_model(eligible)
        log_duration = dt.datetime.utcnow() - log_start_dt
        self.status_step_to_cache(eligible.entity, self.STATUS[1], log_duration, data_wide_extra)  # READY
        log.info(f"Entity {Pipeline.entity_short_id(eligible.entity)} OK! ({log_duration})")

    def do_extract_moment_begin_end(self,
                                    eligible,
                                    moment_last_data: Union[datetime, None])\
            -> Tuple[Union[Any, None], Union[Any, None]]:
        """
        Calculate the begin and end moments to extract data. Restrict to entity
        first and last data records, also to last moment of extracted data.
        Moreover, restrict to minimum and maximum time deltas for each pipeline
        batch processing.

        :param eligible: A named tuple with entity, age, gender, first, last...
        :param moment_last_data: The last data extracted timestamp.
        :return: moment_begin or None, moment_end or None.
        """
        # MOMENT BEGIN
        if moment_last_data is None:  # 1st time processing
            moment_begin = self.expanse.moment_of_version(
                eligible.entity,
                Expanse.DATABASE_STABLE_SENSEI_VERSION
            )  # None if no eligible data
        else:  # nth time processing (next batch)
            moment_begin = moment_last_data + dt.timedelta(milliseconds=1)  # t+1
        # MOMENT END
        moment_end = None
        if moment_begin is not None and moment_begin < eligible.last:
            interval = min(eligible.last - moment_begin, Pipeline.DATA_INTERVAL_MAX)
            if interval >= Pipeline.DATA_INTERVAL_MIN:
                moment_end = moment_begin + interval
        return moment_begin, moment_end

    def do_extract(self,
                   eligible,
                   moment_begin,
                   moment_end)\
            -> bool:
        """
        Extract a bulk of eligible entity data and load into
        ``self.transform.Ready``.
        Data will be all fetched using a time window. The interval shifts
        between first and last data moments. First may be pipeline last
        successful update. Last may be first plus ``Pipeline.DATA_INTERVAL_MAX``
        restriction::

         ...
         extracted = self.expanse.extract(eligible.entity, moment_begin, moment_end)
         self.transform = Ready(tuples=extracted)
         ...

        Moreover, if no data to extract then False is returned. True is
        returned on successful extraction. Exception raised on error.

        :param eligible: A named tuple with entity, age, gender, first, last...
        :param moment_begin: Data first moment to extract.
        :param moment_end: Data last moment to extract.
        :return: True on further processing, False otherwise.
        """
        with tqdm(total=2,
                  leave=self.debug,
                  disable=not self.verbose) as bar:
            bar.set_description_str("Extract")
            info = f"[{moment_begin}, {moment_end}]"\
                   f" {Pipeline.entity_short_id(eligible.entity)} {eligible.age} {eligible.gender}"
            # EXTRACT data
            bar.set_postfix_str("Data extracting...")
            if moment_begin is not None and moment_end is not None:
                extracted = self.expanse.extract(eligible.entity, moment_begin, moment_end)
                tuples_n = 0 if extracted is None else len(extracted)
                bar.update()
                # READY load
                bar.set_postfix_str("Data loading...")
                self.transform = Ready(tuples=extracted)
                bar.update()
                bar.set_postfix_str(f"{tuples_n} tuples {info}")
                log.debug(f"Extracted: {tuples_n:9d} tuples ({moment_end - moment_begin})")
                return True
            else:
                bar.set_postfix_str(f"NOT ELIGIBLE moments {info}")
                return False

    def do_transform(self)\
            -> bool:
        """
        Reconstruct data for the required sensors. Process selected sensors
        and ready data for further transformation (e.g., ML X y features).

        :return: True if all required sensors have data ready. False otherwise.
        """
        with tqdm(total=2,
                  leave=self.debug,
                  disable=not self.verbose) as bar:
            bar.set_description_str("Transform")
            bar.set_postfix_str("Valence all sensors...")
            self.transformed["valence_all_sensors"] = self.transform.valence()
            bar.update()
            log.debug(f"Transform valence length={len(self.transformed['valence_all_sensors'])}")
            bar.set_postfix_str("Geopositioning...")
            self.transformed["geo_latlon"], self.transformed["geo_latlon_rad"] = self.transform.geopositioning()
            bar.update()
            log.debug(f"Transform geopositioning length={len(self.transformed['geo_latlon'])}")
            if self.settings["context"]["activity"]["enabled"]:
                bar.set_postfix_str("Human activity...")
                self.transformed["activity"] = self.transform.data(sensor="app.sensor.activity")
                bar.update()
                log.debug(f"Transform activity length={len(self.transformed['activity'])}")

            # TODO accelerometer may improve valence!! Check medical articles about it...

            bar.set_postfix_str("")
        # TRUE only if REQUIRED SENSORS have data ready
        return (
            not self.transformed["valence_all_sensors"].empty and  # VALENCE
            not self.transformed["geo_latlon"].empty and (self.transformed["geo_latlon_rad"].size > 0)  # LOCATION
        )

    def do_learn_location(self,
                          hdbscan_min_samples: List[int],
                          hdbscan_min_samples_pref: int,
                          hdbscan_auto_params_cb:
                              Union[Callable[[int, int, range, Dict[str, List[int]]], Any], None] = None,
                          hdbscan_geo_clusters_cb:
                              Union[Callable[[Any, Any, int], Any], None] = None)\
            -> None:
        """
        Location clustering using HDBSCAN algorithm with auto minimum cluster
        size (hyper parameter tuning). Map to a Discrete Global Grid (DGG): MGRS
        (discard outliers: -1) 
        
        :param hdbscan_min_samples: List of min_samples values to use in auto params.
        :param hdbscan_min_samples_pref: min_samples value preferred for clustering.
        :param hdbscan_auto_params_cb: Callback for auto params.
        :param hdbscan_geo_clusters_cb: Callback for clustering.
        """
        with tqdm(total=2,
                  leave=self.debug,
                  disable=not self.verbose) as bar:
            bar.set_description_str("Learn location")
            # HDBSCAN GEO CLUSTERS (include hyper parameters auto tuning)
            bar.set_postfix_str("Geo clusters...")
            geo_cluster, geo_cluster_n = self.learn.geo_clusters(
                    coordinates=self.transformed["geo_latlon_rad"],
                    hdbscan_min_samples=hdbscan_min_samples,
                    hdbscan_min_samples_pref=hdbscan_min_samples_pref,  # ...in the radius of a point to be a core.
                    hdbscan_auto_params_callback=hdbscan_auto_params_cb
             )
            self.transformed["geo_latlon"]["cluster"] = geo_cluster.labels_
            # The strength with which each sample is a member of its assigned
            # cluster. Noise points have probability zero.
            self.transformed["geo_latlon"]["cluster_probability"] = geo_cluster.probabilities_
            if hdbscan_geo_clusters_cb is not None:
                hdbscan_geo_clusters_cb(geo_cluster,
                                        self.transformed["geo_latlon"],
                                        geo_cluster_n)
            bar.update()
            log.debug(f"Learn location clusters total={geo_cluster_n}")
            # MAP to a Discrete Global Grid (DGG): MGRS (discard outliers: -1)
            bar.set_postfix_str("World grid mapping...")
            dgg = mgrs.MGRS()

            def to_mgrs(lat, lon, precision) -> str:
                return dgg.toMGRS(lat, lon, MGRSPrecision=precision).decode("ASCII")

            self.transformed["geo_learn"]: DataFrame =\
                self.transformed["geo_latlon"].loc[self.transformed["geo_latlon"]["cluster"] > -1]
            self.transformed["geo_learn"] =\
                self.transformed["geo_learn"].assign(
                    mgrs_100=np.vectorize(to_mgrs)(
                        self.transformed["geo_learn"]["latitude"],
                        self.transformed["geo_learn"]["longitude"],
                        Pipeline.MGRS_PRECISION_100
                    ),
                    mgrs_1000=np.vectorize(to_mgrs)(
                        self.transformed["geo_learn"]["latitude"],
                        self.transformed["geo_learn"]["longitude"],
                        Pipeline.MGRS_PRECISION_1000
                    ),
                    mgrs_10000=np.vectorize(to_mgrs)(
                        self.transformed["geo_learn"]["latitude"],
                        self.transformed["geo_learn"]["longitude"],
                        Pipeline.MGRS_PRECISION_10000
                    )
                )
            bar.update()
            log_info = (f"{len(self.transformed['geo_learn'])} out of"
                        f" {len(self.transformed['geo_latlon_rad'])} coordinates"
                        f" {self.transformed['geo_learn']['mgrs_100'].nunique()}"
                        f"/{self.transformed['geo_learn']['mgrs_1000'].nunique()}"
                        f"/{self.transformed['geo_learn']['mgrs_10000'].nunique()}"
                        f" grid areas (100m/1km/10km)")
            log.debug(f"Learn location {log_info}")
            bar.set_postfix_str(log_info)

    def do_data_wide(self)\
            -> bool:
        """
        Use ``self.transformed`` data for further processing, one-hot encoding,
        and wide alignment. Features::

         [ MGRS 1, ..., MGRS N ] [ HOUR ] [ DAY QUARTER ] [ DAY OF WEEK ]

        :return: True if success, False otherwise (rare cases of location data
            not shared).
        """
        # FEATURES AS CATEGORICAL (ONE-HOT ENCODING)
        mgrs_on = pd.get_dummies(
            data=self.transformed["geo_learn"][Pipeline.MGRS_PRECISION_PREFIX].astype("category", copy=True),
            prefix=Pipeline.MGRS_PRECISION_PREFIX,
            drop_first=False
        )
        moment = self.transformed["geo_learn"].index.to_series()
        # [0, 23]
        moment_hour_on = pd.DataFrame(
            data=[hour for hour in moment.dt.hour],
            index=moment.index,
            columns=["moment_hour"]
        )
        # [0, 3] : 0=[0, 5] 1=[6, 11] 2=[12, 17] 3=[18, 23] : 24 / 4 = 6
        moment_day_quarter_on = pd.DataFrame(
            data=[(hour // 6) for hour in moment.dt.hour],
            index=moment.index,
            columns=["moment_day_quarter"]
        )
        # [0, 6] : [Monday, Sunday]
        moment_dow_on = pd.DataFrame(
            data=[dow for dow in moment.dt.dayofweek],
            index=moment.index,
            columns=["moment_dow"]
        )

        # NOTICE: Week of Year, Month, Quarter ONLY useful if data duration > 3 years

        # NOTICE: self.transformed["activity"] MAY be EMPTY OR NO transition...
        if "activity" in self.transformed and "activity_transition" in self.transformed["activity"].columns:
            activity_on = pd.get_dummies(data=(self.transformed["activity"]
                                               .query("activity_transition == 0.0")["activity_type_extra"]
                                               .astype("category", copy=True)),
                                         prefix="activity",
                                         drop_first=False)
        else:
            activity_on = pd.DataFrame()
        # FEATURES ALIGNMENT
        data_wide = (
            mgrs_on
            .join(other=moment_hour_on, on="moment")
            .join(other=moment_day_quarter_on, on="moment")
            .join(other=moment_dow_on, on="moment")
            .join(other=activity_on, on="moment")
            .join(other=self.transformed["valence_all_sensors"]["valence"], on="moment")
            .reset_index()  # "moment" from index to column
            .drop(columns=["moment"])
            .dropna(axis=1, how="all")  # Drop columns with only NaN
            .dropna(how="any")  # Drop rows with at least one NaN
        )
        log.debug(f"Data wide {data_wide.shape} (iteration)")
        # CHECK required VALENCE column and COMPLETE with CLASS
        if "valence" in data_wide.columns:
            # VALENCE to VALENCE_CLASS
            data_wide["valence_class"] = Valence.to_class(data_wide["valence"])
            # UPDATE (append)
            #  NOTICE: beware of fill NaN with 0 for the new MGRS locations when ADDING NEW features!
            self.transformed["data_wide"] = self.transformed["data_wide"].append(data_wide, sort=True).fillna(0)
            log.debug(f"Data wide {self.transformed['data_wide'].shape} (total)")
            return True
        else:
            return False

    def do_data_x_y_split_train_test(self,
                                     time_series: bool,
                                     random_seed: Union[int, None])\
            -> Tuple[Any, Any, Any, Any]:
        """
        Do a train/test split of data wide for machine learning algorithms X y.
        Drop "valence" and "valence_class" targets from X features. y is only
        "valence_class" multi-class target.

        :param time_series: Check if split can be shuffled or not (time series).
        :param random_seed: Freeze random seed value for reproducibility.
        :return: X_train, X_test, y_train, y_test
        """
        x = self.transformed["data_wide"].drop(columns=["valence", "valence_class"]).astype(dtype="int32", copy=True)
        y = self.transformed["data_wide"]["valence_class"]
        # X y STANDARD SCALING NOT NEEDED! Got FEATURES with INTRINSIC SCALE (e.g., moment_hour)
        # X y train/test SPLIT
        x_train, x_test, y_train, y_test =\
            model_selection.train_test_split(
                x, y,
                test_size=Pipeline.DATA_SPLIT_TEST,
                shuffle=not time_series,
                random_state=random_seed
            )
        return x_train, x_test, y_train, y_test

    @staticmethod
    def likelihood_ratio_imbalance_degree(data: Series)\
            -> float:
        """
        A metric for how much imbalanced is the distribution of target classes
        values.

        Adapted from https://github.com/sahutkarsh/likelihood-ratio-imbalance-degree

        Zhu, R., Wang, Z., Ma, Z., Wang, G., & Xue, J. H. (2018).
        LRID: A new metric of multi-class imbalance degree based on likelihood-ratio test.
        Pattern Recognition Letters, 116, 36–42. https://doi.org/10.1016/j.patrec.2018.09.012

        :param data: Target values series, i.e., classes distribution.
        :return: LRID between 0.0 (full balanced) and negative infinity.
        """
        c = data.nunique()
        size = len(data)
        prob = [(count / size) for _, count in data.value_counts().items()]
        lrid = -2 * np.sum([(p * math.log(p * c)) for p in prob])
        return lrid

    @staticmethod
    def do_data_class_imbalance_check(x_train: DataFrame,
                                      x_test: DataFrame,
                                      y_train: Series,
                                      y_test: Series,
                                      classes: List,
                                      n_samples_min: int = 2,
                                      n_classes_min: int = 2)\
            -> Tuple[bool, Dict, DataFrame, DataFrame, Series, Series]:
        """
        Check data set classification classes imbalance degree and count.
        Filter data set dropping all samples from target class with total count
        less than n_samples_min. Finally check if resulting classes list has
        at least n_classes_min from eligible ones.

        With default values, three-class count [0, 0, 42] is not eligible.
        A samples count of [0, 2, 42] is eligible.

        :param x_train: Features train data from X y split.
        :param x_test: Features test data from X y split.
        :param y_train: Target train data from X y split.
        :param y_test: Target test from X y split.
        :param classes: Target classes values list (e.g., Valence.CLASS_VALUES)
        :param n_samples_min: Number of samples required for each target class.
        :param n_classes_min: Number of classes required to check successfully.
        :return: True on success, dictionary with class count and imbalance
            degree for split values and check values, filtered data set ready.
        """
        info = dict()
        # TRAIN
        y_train_hist, _ = np.histogram(y_train, bins=classes + [classes[-1]])
        # numpy.int64 NOT JSON serializable https://bugs.python.org/issue24313
        class_count_split = list(map(int, y_train_hist))
        lrid_split = Pipeline.likelihood_ratio_imbalance_degree(y_train)
        info.update({"class_count_split": class_count_split, "lrid_split": round(lrid_split, 2)})
        # CHECK data REQUIRED each class n_samples_min
        class_count_check = list(map(lambda x: 0 if x < n_samples_min else int(x), y_train_hist))
        info.update({"class_count_check": class_count_check})
        # FILTER DATA to keep only ELIGIBLE CLASSES
        classes_to_keep = [Valence.CLASS_VALUES[i] for (i, c) in enumerate(class_count_check) if c > 0]
        train_index_to_keep = y_train.isin(classes_to_keep)
        x_train_check = x_train[train_index_to_keep]
        y_train_check = y_train[train_index_to_keep]
        lrid_check = Pipeline.likelihood_ratio_imbalance_degree(y_train_check)
        info.update({"lrid_check": round(lrid_check, 2)})
        # CHECK RESULT n_classes_min REQUIRED
        check = (len(class_count_check) - class_count_check.count(0)) >= n_classes_min
        # TEST
        test_index_to_keep = y_test.isin(classes_to_keep)
        x_test_check = x_test[test_index_to_keep]
        y_test_check = y_test[test_index_to_keep]
        # only after classes restricted
        y_test_hist, _ = np.histogram(y_test, bins=classes + [classes[-1]])
        # numpy.int64 NOT JSON serializable https://bugs.python.org/issue24313
        class_count_test = list(map(int, y_test_hist))
        lrid_test = Pipeline.likelihood_ratio_imbalance_degree(y_test)
        info.update({"class_count_test": class_count_test, "lrid_test": round(lrid_test, 2)})
        return check, info, x_train_check, x_test_check, y_train_check, y_test_check

    def do_algorithm_parameters(self,
                                x_train_len: int,
                                x_train_n_features: int,
                                random_seed: int)\
            -> Union[Dict, None]:
        """
        Process and prepare specific parameters to the enabled algorithms.
        Some fine tuning due to data set characteristics adaptation.
        Includes algorithm object instantiation.

        NOTICE: scikit-learn require settings conversion to tuple! See code...

        :param x_train_len: Number of training samples.
        :param x_train_n_features: Number of training samples features.
        :param random_seed: To obtain a deterministic behaviour on all runs.
        :return: A dictionary of algorithm object instance and tuned parameters
            for search and cross validation. None if no algorithm is available.
        """
        # ALGORITHMS
        algorithms = self.settings["algorithms"]
        for k in list(algorithms.keys()):
            if not algorithms[k]["enabled"]:
                del algorithms[k]  # remove not enabled
        # DEFAULT INSTANCE and SEARCH PARAMETERS FINE TUNE
        for algorithm in algorithms:
            if algorithm == "dummy":  # BASELINE
                algorithms[algorithm]["default"] = dummy.DummyClassifier(
                    random_state=random_seed)
            elif algorithm == "lr":  # LINEAR
                algorithms[algorithm]["default"] = linear_model.LogisticRegression(
                    random_state=random_seed, multi_class="auto")
                algorithms[algorithm]["search"]["C"] = tuple(algorithms[algorithm]["search"]["C"])
                # Adaptation to shared data on human behaviour
                # https://scikit-learn.org/stable/modules/linear_model.html#logistic-regression
                if x_train_len < 1000 or x_train_n_features > 100:
                    algorithms[algorithm]["search"]["solver"] = ["liblinear"]
                else:
                    algorithms[algorithm]["search"]["solver"] = ["saga"]
            elif algorithm == "xgboost":  # NON LINEAR ENSEMBLE strongly EXPLAINABLE
                algorithms[algorithm]["default"] = xgboost.sklearn.XGBClassifier(
                    random_state=random_seed)
                # https://xgboost.readthedocs.io/en/latest/tutorials/param_tuning.html
                algorithms[algorithm]["search"]["max_depth"] = tuple(algorithms[algorithm]["search"]["max_depth"])
                algorithms[algorithm]["search"]["learning_rate"] =\
                    tuple(algorithms[algorithm]["search"]["learning_rate"])
                algorithms[algorithm]["search"]["gamma"] = tuple(algorithms[algorithm]["search"]["gamma"])
                algorithms[algorithm]["search"]["max_delta_step"] =\
                    tuple(algorithms[algorithm]["search"]["max_delta_step"])
            elif algorithm == "nn":  # CONNECTIONIST (ANN) weakly EXPLAINABLE
                algorithms[algorithm]["default"] = KerasClassifier(
                    build_fn=Learn.neural_network_model,
                    input_shape=(x_train_n_features,)
                )
                # https://arxiv.org/pdf/1206.5533.pdf
                algorithms[algorithm]["search"]["dropout_rate"] = tuple(algorithms[algorithm]["search"]["dropout_rate"])
                algorithms[algorithm]["search"]["batch_size"] = tuple(algorithms[algorithm]["search"]["batch_size"])
                algorithms[algorithm]["search"]["verbose"] = tuple(algorithms[algorithm]["search"]["verbose"])
        return algorithms if algorithms != {} else None

    def do_make_scorer(self)\
            -> Callable:
        """
        Make a scorer from a performance metric defined in settings.
        Only the first one enabled is considered for now. If none then
        default to F1 score weighted.

        :return: A scikit-learn metrics.make_scorer(...)
        """
        settings_metric = self.settings["metric"]
        for metric_name in settings_metric:
            if settings_metric[metric_name]["enabled"]:
                kwargs = settings_metric[metric_name]["kwargs"]
                if metric_name == "f1_score":
                    return metrics.make_scorer(metrics.f1_score, **kwargs)
                elif metric_name == "matthews_corrcoef":
                    return metrics.make_scorer(metrics.matthews_corrcoef, **kwargs)
        # DEFAULT F1 score weighted
        return metrics.make_scorer(metrics.f1_score, average="weighted")

    @staticmethod
    def metrics_extra(estimator: object,
                      x_test: Any,
                      y_test: Any,
                      classes: Sequence)\
            -> Dict:
        """
        Calculate extra metrics, i.e., confusion matrix and classification
        report. Return result inside a dictionary for storage and further
        processing.

        :param estimator: Model estimator after training, i.e., fit.
        :param x_test: Features test data from X y split.
        :param y_test: Target test from X y split.
        :param classes: List of class labels.
        :return: A dictionary with metrics values.
        """
        result = dict()
        y_pred = estimator.predict(x_test)
        result.update({
            "confusion_matrix": metrics.confusion_matrix(y_test, y_pred, classes).tolist(),
            "classification_report": metrics.classification_report(y_test, y_pred, classes, output_dict=True)
        })
        return result

    def do_learn_model(self,
                       eligible)\
            -> Union[Dict, None]:
        """
        Do data X y and split for cross validation. Do learn with a set of
        classification algorithm estimators. Do model persistence update.
        Moreover, auto-tune hyper parameters for each model:

         Learn > Update model

        :param eligible: A named tuple with entity, age, gender, first, last...
        :return: A dictionary with data wide extra information (e.g., {"lrid": 0.0}).
        """
        log_prefix = "Learn model"
        # DATA split X y
        # x_train_ts, x_test_ts, y_train_ts, y_test_ts =\
        #     self.do_data_x_y_split_train_test(time_series=True, random_seed=None)
        x_train, x_test, y_train, y_test =\
            self.do_data_x_y_split_train_test(time_series=False, random_seed=Pipeline.RANDOM_SEED)
        # DATA class imbalance degree and count check with required transformation
        check, info, x_train, x_test, y_train, y_test =\
            Pipeline.do_data_class_imbalance_check(x_train, x_test, y_train, y_test, classes=Valence.CLASS_VALUES)
        x_train_len = len(x_train)
        x_test_len = len(x_test)
        # EXTRA information for database pipeline
        data_wide_extra = dict({"eligible": check})
        data_wide_extra.update(info)
        imbalance_info = f"class_count_check={info['class_count_check']}, LRID_check={round(info['lrid_check'], 2)}"
        if not check:
            info = f"{log_prefix} NOT ELIGIBLE {imbalance_info}"
            log.debug(info)
            if self.verbose:
                tqdm.write(info)
            return data_wide_extra
        else:
            log.debug(f"{log_prefix} {imbalance_info}")
        # ALGORITHM + PARAMETERS
        algorithms = self.do_algorithm_parameters(x_train_len, x_train.shape[1], Pipeline.RANDOM_SEED)
        if algorithms is None:
            info = "NO ALGORITHMS enabled. Hint: check settings, i.e., pipeline.json"
            log.warning(info)
            if self.verbose:
                tqdm.write(info)
            return data_wide_extra
        # SCORE
        scorer = self.do_make_scorer()
        data_wide_extra.update({"metric": scorer.__dict__["_score_func"].__name__})
        # ESTIMATOR and MODEL
        with tqdm(algorithms,
                  disable=not self.verbose) as algorithms_bar:
            algorithms_bar.set_description_str("Learn")
            for algorithm in algorithms_bar:
                algorithms_bar.set_postfix_str(f"X {x_train_len}/{x_test_len} {imbalance_info}"
                                               f" {algorithms[algorithm]['default'].__doc__.splitlines(False)[0]}")
                # Auto find best cross validation N splits for splitter
                cv_splitter = Learn.eligible_split(
                    x_train=x_train,
                    y_train=y_train,
                    splitter=model_selection.StratifiedKFold,
                    n_classes=algorithms[algorithm]["split_n_class_min"]
                )
                # Splitter REQUIRED for CROSS VALIDATION
                if cv_splitter is None:
                    info = f"{algorithm}: NO LEARN due to CV splitter UNAVAILABLE!"
                    log.debug(f"{log_prefix} {info}")
                    if self.verbose and self.debug:
                        tqdm.write(f"{log_prefix} {info}")
                    continue
                best_estimator, fit_score, test_score, fit_duration, test_duration =\
                    self.learn.model_estimator(
                        estimator={
                            "default": algorithms[algorithm]["default"],
                            "search": algorithms[algorithm]["search"],
                            "search_n_iter": algorithms[algorithm]["search_n_iter"]
                        },
                        scoring=scorer,
                        x_train=x_train,
                        x_test=x_test,
                        y_train=y_train,
                        y_test=y_test,
                        cv_splitter=cv_splitter,
                        early_stop=algorithms[algorithm]["early_stop"]
                    )
                # EXTRA
                extra = Pipeline.metrics_extra(best_estimator, x_test, y_test, Valence.CLASS_VALUES)
                extra.update({"split": [Pipeline.DATA_SPLIT_TRAIN, Pipeline.DATA_SPLIT_TEST]})
                # MODEL persistence update
                self.model_to_cache(entity=eligible.entity, estimator=best_estimator, score=float(test_score),
                                    fit_duration=fit_duration, score_duration=test_duration,
                                    extra=extra)
                fit_duration = dt.timedelta(seconds=round(fit_duration.total_seconds()))
                test_duration = dt.timedelta(seconds=round(test_duration.total_seconds()))
                algorithm_info = f"{algorithm:5s} ({x_train_len}/{x_test_len}):"\
                                 f" score={test_score:.2f} duration={fit_duration} (fit) + {test_duration} (test)"
                algorithms_bar.set_postfix_str(f"{imbalance_info} {algorithm_info}")
                log.debug(f"{log_prefix} {algorithm_info}")
        return data_wide_extra
