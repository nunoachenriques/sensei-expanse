#  Copyright 2019 Nuno A. C. Henriques [nunoachenriques.net]
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

"""
SensAI Expanse metric utilities.

See cognition folder at https://gitlab.com/nunoachenriques/sensei-expanse
"""


def normal_score(value: float,
                 value_min: float = -1.0,
                 value_max: float = 1.0) \
        -> float:
    """
    Convert value from given codomain (default: [-1.0, 1.0]) to [0.0, 1.0].
    Codomain default for Matthews correlation coefficient.

    :param value: Score to convert.
    :param value_min: Codomain minimum.
    :param value_max: Codomain maximum.
    :return: Value normalized to [0.0, 1.0] codomain.
    """
    if not value_min <= value <= value_max:
        raise ValueError(f"{value} not in [{value_min}, {value_max}]")
    return (value - value_min) / (value_max - value_min) * (1.0 - 0.0) + 0.0
