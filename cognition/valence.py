#  Copyright 2019 Nuno A. C. Henriques [nunoachenriques.net]
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

"""
SensAI Expanse emotional valence polarity utilities.

See cognition folder at https://gitlab.com/nunoachenriques/sensei-expanse
"""

from typing import Sequence, Tuple, Union


class Valence:
    """
    Main class for emotional valence polarity utilities. Classification
    intervals::

     [-1.0, -0.5] = Negative class = -1
     (-0.5,  0.5) = Neutral class  =  0
     [ 0.5,  1.0] = Positive class =  1

    Moreover, valence colours in hexadecimal from RGB::

     Negative: #0D47A1
     Neutral: #2E7D32
     Positive: #F57F17
    """

    CLASS = {"Negative": -1, "Neutral": 0, "Positive": 1}
    CLASS_VALUES = list(CLASS.values())
    COLOUR_HEX = {"Negative": "#0D47A1", "Neutral": "#2E7D32", "Positive": "#F57F17"}
    COLOUR_HEX_VALUES = list(COLOUR_HEX.values())
    NEGATIVE_LEFT = -1.0
    NEGATIVE_RIGHT = -0.5
    POSITIVE_LEFT = 0.5
    POSITIVE_RIGHT = 1.0
    NEUTRAL_LEFT_O = NEGATIVE_RIGHT
    NEUTRAL_RIGHT_O = POSITIVE_LEFT

    @staticmethod
    def to_class(valence: Sequence[float])\
            -> Sequence[Union[int, None]]:
        """
        Convert each valence value to one of the available classes.

        :param valence: List of emotional valence values.
        :return: Each valence converted to one of the available classes.
            None if the value is None or out of bounds.
        """
        valence_class = list()
        for v in valence:
            if v is None:
                valence_class.append(None)
            elif Valence.NEGATIVE_LEFT <= v <= Valence.NEGATIVE_RIGHT:
                valence_class.append(Valence.CLASS["Negative"])
            elif Valence.NEUTRAL_LEFT_O < v < Valence.NEUTRAL_RIGHT_O:
                valence_class.append(Valence.CLASS["Neutral"])
            elif Valence.POSITIVE_LEFT <= v <= Valence.POSITIVE_RIGHT:
                valence_class.append(Valence.CLASS["Positive"])
            else:
                valence_class.append(None)
        return valence_class

    @staticmethod
    def to_class_and_colour(valence: Sequence[float])\
            -> Sequence[Union[Tuple[int, str], None]]:
        """
        Convert each valence value to an (int, str) tuple::

         (one of the available classes, class colour in hexadecimal).

        :param valence: List of emotional valence values.
        :return: Each valence converted to an (int, str) tuple comprised of
            one of the available classes and respective colour.
            None if the value is None or out of bounds.
        """
        valence_class_and_colour = list()
        for v in valence:
            if v is None:
                valence_class_and_colour.append(None)
            elif Valence.NEGATIVE_LEFT <= v <= Valence.NEGATIVE_RIGHT:
                valence_class_and_colour.append((Valence.CLASS["Negative"], Valence.COLOUR_HEX["Negative"]))
            elif Valence.NEUTRAL_LEFT_O < v < Valence.NEUTRAL_RIGHT_O:
                valence_class_and_colour.append((Valence.CLASS["Neutral"], Valence.COLOUR_HEX["Neutral"]))
            elif Valence.POSITIVE_LEFT <= v <= Valence.POSITIVE_RIGHT:
                valence_class_and_colour.append((Valence.CLASS["Positive"], Valence.COLOUR_HEX["Positive"]))
            else:
                valence_class_and_colour.append(None)
        return valence_class_and_colour
