#  Copyright 2019 Nuno A. C. Henriques [nunoachenriques.net]
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

"""
SensAI Expanse data load to some algorithms and obtain insights and produce
charts, i.e., information from data. Utility class for statistical analysis.

See cognition folder at https://gitlab.com/nunoachenriques/sensei-expanse
"""

import datetime as dt
from datetime import timedelta
from typing import Any, Dict, List, Tuple

import numpy as np
import pandas as pd
from matplotlib.figure import Figure
from numpy import ndarray
from pandas import DataFrame


class Analysis:
    """
    Main class for obtaining insights and produce charts from SensAI Expanse
    extracted and transformed data. Keep in mind the specific types of data
    required in this context: age and gender, emotional valence three types
    (negative, neutral, positive), and also collected data moments such as
    duration for each entity.
    """

    def __init__(self,
                 demographics: DataFrame,
                 moments: DataFrame,
                 collected: DataFrame,
                 gender_name: Dict[str, str],
                 valence_colour: Dict[str, Tuple[float, float, float]],
                 duration_range: List[timedelta] = None,
                 age_range_step: int = 10,
                 freq: str = "1min"):
        """
        Constructor with initial parameters including data to process and
        analyze. Complete data (merge `collected`) with demographics, moments,
        and range interval bins calculated and assigned as new columns.
        Also, replace `gender` id with name and resample `collected` by `freq`.
        Example::

            from cognition.analysis import Analysis
            from cognition.expanse import Expanse

            expanse = Expanse()
            analysis = Analysis(
                demographics=expanse.entity_demographics(),
                moments=expanse.entity_data_moments(),
                collected=expanse.collected("app.human.smile", "polarity"),
                gender_name=expanse.gender_names(language="eng"),
                valence_colour=expanse.valence_colours()
            )

        **REQUIRED**:

         * :attr:`demographics`: at least, three columns per entity
           and with the following names and types::

            [entity, age, gender, ...]
             "uid" , 47 , "XX"  , ...

           Where each `entity` must be an unique identifier, string or numeric,
           `age` is numeric and `gender` a category and replaced using
           `gender_name`.

         * :attr:`moments`: at least, two columns per entity and with the
           following names and types::

            [entity, duration, ...]
             "uid" , 1 days  , ...

           Where each `entity` must be an unique identifier, string or numeric,
           and `duration` is a `datetime.timedelta` interval.

         * :attr:`collected`: at least, three columns per entity and with the
           following names and types::

            [entity, moment           , sensor_value, ...]
             "uid" , 2019-01-01 00:00 , -1.0        , ...

           Where each `entity` must be an unique identifier, string or numeric,
           and `moment` is a `datetime.datetime` timestamp.

        **NEW** DataFrame containers and **ASSIGNED** extra columns:

         * :attr:`moments`: equal to `moments`. One new aggregation column
           assigned::

            [entity, duration, ..., duration_range_bin]
             "uid" , 1 days  , ..., [1 days, 7 days)

           Where `duration_range_bin` contains the calculated
           bin from default range or inferred by :py:attribute:`duration_range`
           values and using `duration` data.

         * :attr:`entities`: all entities from input `demographics` merged
           with `moments` after duration range bin::

            [entity, age, gender, ..., duration, duration_range_bin]
             "uid" , 47 , "XX"  , ..., 1 days  , [1 days, 7 days)

         * :attr:`demographics_in`: `entities` restricted to age and gender
           filled in.

         * :attr:`data`: `collected` resampled by `freq` retaining only the
           last sensor_value for each time. Five new columns are assigned::

            [..., hour, weekday, negative, neutral, positive]
             ..., 23  , 3      , 1.0     , NaN    , NaN

           Where `hour` is [0, 23] and `weekday` is [0, 6], 0 = Monday.

         * :attr:`eligible`: `demographics_in` restricted to entities with
           reported data. Two new aggregation columns are assigned::

            [entity, age, gender, ..., age_range_bin, age_range_dic_bin]
             "uid" , 47 , "XX"  , ..., [40, 50)     , [30, 70)

           Where `age_range_bin` contains the calculated bin from age range
           inferred by `age` values. `age_range_dic_bin` contains one of
           the two possible dichotomic bins after median over `age` values
           and `age_range_step`.

         * :attr:`ineligible`: `entities` not in `eligible`.

         * :attr:`population_count`: `eligible` versus `ineligible` counts.

         * :attr:`data_eligible`: `data` restricted to `eligible` entities.

         * :attr:`data_report_count`: `data_eligible` restricted to columns
           ["entity", "negative", "neutral", "positive"],
           aggregated by count, and several new columns assigned:

            * Three new columns from valence processing::

               [..., negative_p, neutral_p, positive_p]
                ..., 0.1       , 0.3      , 0.6

            * All columns from `eligible`::

               [..., gender, age, age_range_bin, age_range_dic_bin]
                ..., "XX"  , 47 , [40, 50)     , [30, 70)

            * All columns from `moments`::

               [..., first,               last,
                ..., 2018-05-08 19:09:40, 2019-02-08 23:33:29,
                last_from_now, duration         , duration_range_bin  ]
                -17:07:39    , 276 days 04:23:49, [28 days, 2800 days)

         * :attr:`gender_name`: equal to `gender_name`.

         * :attr:`valence_colour`: equal to `valence_colour`.

        :param demographics: Demographic data for each entity including age
            and gender columns. See
            sensei-expanse/memory/source/schema-create.sql
            entity_demographics().
        :param moments: Aggregated data for each entity including duration
            column. See sensei-expanse/memory/source/schema-create.sql
            entity_data_moments().
        :param collected: All data of some sensor type restricted by attribute
            (e.g., emotional valence reports).
            See sensei-expanse/memory/source/schema-create.sql collected()
        :param gender_name: Dictionary of (id, name) for all types of gender.
        :param valence_colour: Dictionary of (name, (R, G, B)) for all valence
            colours. Actually, three names are required: negative, neutral,
            positive.
        :param duration_range: The range for duration bins column. If None
            then defaults to [0, 1 day, 1 week, 4 weeks, 400 weeks].
        :param age_range_step: Interval length for each bin.
        :param freq: Frequency for Pandas.Grouper(freq=freq, key="moment")
            apply to collected on Pandas.DataFrame.groupby(). Outliers will
            be smoothed retaining only the last sensor_value for each freq.
        """
        # MOMENTS
        if duration_range is None:
            self.duration_range = [dt.timedelta(hours=0),
                                   dt.timedelta(days=1),
                                   dt.timedelta(weeks=1),
                                   dt.timedelta(weeks=4),
                                   dt.timedelta(weeks=400)]  # No infinity in Python datetime :-(
        else:
            self.duration_range = duration_range
        self.moments = moments.assign(
            duration_range_bin=pd.cut(x=moments["duration"],
                                      bins=self.duration_range,
                                      right=False)
        )
        # ENTITIES (all)
        self.entities = demographics.merge(right=self.moments, on="entity")
        # ENTITIES with DEMOGRAPHICS filled IN
        self.demographics_in = (
            self.entities
            .dropna(how="any", subset=["age", "gender"])
            .replace(to_replace={"gender": gender_name})
        )
        # COLLECTED resampled, last value in freq, EXCLUDE zero reports
        self.data = (
            collected
            .groupby(by=["entity", pd.Grouper(freq=freq, key="moment")])
            .tail(n=1)
            .assign(hour=collected["moment"].dt.hour,
                    weekday=collected["moment"].dt.weekday,
                    yearday=collected["moment"].dt.dayofyear,
                    yearweek=collected["moment"].dt.weekofyear,
                    month=collected["moment"].dt.month,
                    quarter=collected["moment"].dt.quarter,
                    negative=collected["sensor_value"].eq(-1),
                    neutral=collected["sensor_value"].eq(0),
                    positive=collected["sensor_value"].eq(1))
            .replace({"negative": False, "neutral": False, "positive": False}, np.nan)
        )
        # ENTITIES with DEMOGRAPHICS filled IN and VALENCE REPORTED (>0)
        self.eligible = (
            self.demographics_in
            .query(f"entity in {list(self.data['entity'].unique())}")
        )
        _, age_range_bins = self.age_range_bins(age_data=self.eligible["age"],
                                                age_range_step=age_range_step)
        _, age_range_dic_bins = self.age_range_bins(age_data=self.eligible["age"],
                                                    age_range_step=age_range_step,
                                                    dichotomic=True)
        self.eligible = self.eligible.assign(
            age_range_bin=age_range_bins,
            age_range_dic_bin=age_range_dic_bins,
        )
        # ENTITIES not in eligible
        # noinspection PyUnusedLocal
        eligible_entities = list(self.eligible['entity'].to_numpy())  # USED inside query
        self.ineligible = (
            self.entities
            .query("entity not in @eligible_entities")
        )
        # POPULATION
        ineligible_count = self.ineligible.count()
        self.population_count = pd.DataFrame(
            data={
                "Eligible": {
                    "complete": self.eligible["entity"].count()
                },
                "Ineligible": {
                    "no report": ineligible_count["age"],
                    "no demographics": ineligible_count["entity"] - ineligible_count["age"]
                }
            }
        )
        # COLLECTED restricted by ELIGIBLE
        self.data_eligible = (
            self.data
            .query("entity in @eligible_entities")
        )
        # COLLECTED restricted by ELIGIBLE and AGGREGATED
        self.data_report_count = (
            self.data_eligible[["entity", "negative", "neutral", "positive"]]
            .groupby(by="entity")
            .count()
            .assign(negative_p=lambda x: x["negative"] / (x["negative"] + x["neutral"] + x["positive"]),
                    neutral_p=lambda x: x["neutral"] / (x["negative"] + x["neutral"] + x["positive"]),
                    positive_p=lambda x: x["positive"] / (x["negative"] + x["neutral"] + x["positive"]))
            .merge(right=self.eligible, on="entity")
        )
        # OTHERS
        self.gender_name = gender_name
        self.valence_colour = valence_colour

    @staticmethod
    def age_range_bins(age_data: DataFrame,
                       age_range_step: int = 10,
                       dichotomic: bool = False)\
            -> Tuple[ndarray, Any]:
        """
        Determine the min, max values from data. Calculate a range with a given
        step (default 10) and fix range lower and upper limits by step units
        (e.g., `[10 20 30 40]` if step is 10). Finally, generate the interval
        bins: not dichotomic
        (e.g., `[10, 20), [20, 30), [30, 40)` if  10 <= ages < 40); dichotomic
        (e.g., `[10, 24), [24, 40)` if  10 <= ages < 40 and median = 24).

        :param age_data: All ages in rows.
        :param dichotomic: Special case of age_range_bin with only two bins median divided.
        :param age_range_step: Interval length for each bin.
        :return: Age range, age range bins.
        """
        age_min, age_max = age_data.min(), age_data.max()
        age_range_min = age_min - (age_min % age_range_step)
        age_range_max = age_max + (age_range_step - age_max % age_range_step)
        if dichotomic:
            age_median = int(age_data.median())
            age_range = [x for x in map(int, [age_range_min, age_median, age_range_max])]
        else:
            age_range = np.arange(start=age_range_min,
                                  stop=age_range_max + 1,
                                  step=age_range_step, dtype=int)
        return age_range, pd.cut(x=age_data, bins=age_range, right=False, duplicates="drop")

    def plt_valence_by_age_gender(self,
                                  df: DataFrame,
                                  figure: Figure,
                                  dichotomic: bool = False,
                                  bar_width: int = 0.8,
                                  bar_edgecolour: str = "w")\
            -> Dict[str, DataFrame]:
        """
        Plots a chart with two levels of aggregation by age range and gender.
        Moreover, stack the three valence parts with respective colours
        (check constructor) for each part on each bar. All aggregation values
        are calculated from the input data. New columns are assigned with
        the calculated data::

            [..., negative_p, neutral_p, positive_p]
             ..., 0.167     , 0.25     , 0.583

        **NOTICE** that this DataFrame requires, at least, five columns
        per entity with the following names and types::

            [age_range_bin, gender, negative, neutral, positive]
             [10, 20)     , "XX"  , 20      , 30     , 70

        Where `age_range_bin` is an interval bin category, `gender` is another
        category (e.g., string), and `negative`, `neutral`, `positive` are
        report counts for each valence part per entity.

        :param df: Pandas DataFrame with values and the columns referred above.
        :param figure: matplotlib.figure.Figure to plot in.
        :param dichotomic: Special case of age_range_bin with only two bins median divided.
        :param bar_width: Each bar width (matplotlib).
        :param bar_edgecolour: Each bar edge colour (matplotlib 'w' for White).
        :return: Plot data by gender.
        """
        # PREPARE
        ax = figure.gca()
        population = df.shape[0]
        # Discover age range based on data existent bins and dichotomy
        age_range_bin_min = df["age_range_bin"].dropna().min()
        age_range_bin_max = df["age_range_bin"].dropna().max()
        if dichotomic:
            age_median = age_range_bin_min.right
            age_range = [x for x in map(int, [age_range_bin_min.left, age_median, age_range_bin_max.right])]
            ax.set_title("Emotional valence report\n"
                         f"Percentage by age range dichotomy (median={age_median}) and gender\n"
                         f"(population={population})")
        else:
            age_range = np.arange(start=age_range_bin_min.left,
                                  stop=age_range_bin_max.right + 1,
                                  step=age_range_bin_min.right - age_range_bin_min.left)
            ax.set_title("Emotional valence report\n"
                         "Percentage by age range and gender\n"
                         f"(population={population})")
        # Axis X
        x = np.arange(len(age_range) - 1)
        x_ticks = pd.IntervalIndex.from_breaks(breaks=age_range, closed="left")
        # Data
        age_range_gender_valence_count = df[
            ["age_range_bin", "gender", "negative", "neutral", "positive"]
        ]
        gender_nunique = age_range_gender_valence_count["gender"].nunique(dropna=True)
        gender_unique = sorted(age_range_gender_valence_count["gender"].dropna().unique())
        bar_width /= gender_nunique
        data = {}
        # PLOT
        for i, g in enumerate(iterable=gender_unique, start=0):
            data[f"{g}"] = (
                age_range_gender_valence_count
                .query(f"gender == '{g}'")
                .groupby(by=["age_range_bin"])
                .sum()
                .assign(negative_p=lambda x: x["negative"] / (x["negative"] + x["neutral"] + x["positive"]),
                        neutral_p=lambda x: x["neutral"] / (x["negative"] + x["neutral"] + x["positive"]),
                        positive_p=lambda x: x["positive"] / (x["negative"] + x["neutral"] + x["positive"]))
                .fillna(0)
            )
            ax.bar(x=x + bar_width * i, height=data[f"{g}"]["negative_p"],
                   bottom=0, width=bar_width,
                   color=self.valence_colour["negative"], edgecolor=bar_edgecolour,
                   label=f"{g} negative")
            ax.bar(x=x + bar_width * i, height=data[f"{g}"]["neutral_p"],
                   bottom=data[f"{g}"]["negative_p"], width=bar_width,
                   color=self.valence_colour["neutral"], edgecolor=bar_edgecolour,
                   label=f"{g} neutral")
            ax.bar(x=x + bar_width * i, height=data[f"{g}"]["positive_p"],
                   bottom=data[f"{g}"]["negative_p"] + data[f"{g}"]["neutral_p"], width=bar_width,
                   color=self.valence_colour["positive"], edgecolor=bar_edgecolour,
                   label=f"{g} positive")
        ax.legend(labels=["negative", "neutral", "positive"])
        ax.set_xlabel("Age range and gender")
        ax.set_xticks(ticks=x + bar_width / gender_nunique)
        # \N{NO-BREAK SPACE}
        # https://matplotlib.org/api/ticker_api.html?highlight=ticker#matplotlib.ticker.EngFormatter
        xtl_gender = ("{:^8}\N{NO-BREAK SPACE}\N{NO-BREAK SPACE}" * gender_nunique).format(*gender_unique)
        ax.set_xticklabels(labels=[f"{xtl_gender}\n"
                                   f"{xtl_age_range}" for xtl_age_range in x_ticks])
        ax.set_ylabel("Valence")
        figure.text(x=0, y=0,
                    s=f"Data expiry: {dt.datetime.utcnow().replace(microsecond=0)}",
                    **{"horizontalalignment": "left", "fontweight": "light"})
        return data

    def tidy(self,
             dichotomic: bool = True)\
            -> None:
        """
        Update data frames neatly, i.e., drop age range not used and prepare
        the age range in use for `eligible` and `data_report_count`. Also,
        add gender and proper age range bin to `data_eligible`.

        :param dichotomic: Special case of age_range_bin with only two bins median divided.
        """
        to_canonical = "age_range_bin"
        if dichotomic:
            to_keep = "age_range_dic_bin"
            to_drop = "age_range_bin"
        else:
            to_keep = "age_range_bin"
            to_drop = "age_range_dic_bin"
        # PROTECT from 2+ consecutive calls
        if to_keep in self.eligible.columns.to_list():
            self.eligible = (
                self.eligible
                    .drop(columns=to_drop)
                    .rename(columns={to_keep: to_canonical})
            )
            self.data_eligible = (
                self.data_eligible
                    .merge(right=self.eligible[["entity", to_canonical, "gender"]], on="entity")
            )
            self.data_report_count = (
                self.data_report_count
                    .drop(columns=to_drop)
                    .rename(columns={to_keep: to_canonical})
            )
