-- Copyright 2017 Nuno A. C. Henriques [nunoachenriques.net]
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.


-- Sensei Expanse repository schema (PostgreSQL 10.6+)

--
--                                R O L E S
--

grant expanse to ws;

--
--                          D A T A   T A B L E S
--

-- LIST OF VALUES (LOV)

create table colour (
 name text           not null,
 rgb  float ARRAY[3] not null,
 primary key (name)
);
comment on table colour is
 'Chromatic communication by the agent in app and expanse, i.e., name and RGB'
 ' (colour space) specification using range [0.0, 1.0] for each component.'
 ' Some colours are for denoting emotional valence in interaction'
 ' (e.g., name = "positive").';
-- Supported colours
insert into colour (name, rgb) values
 ('negative', '{0.050980392, 0.278431373, 0.631372549}'),  -- {13, 71, 161}/255
 ('neutral', '{0.180392157, 0.490196078, 0.196078431}'),  -- {46, 125, 50}/255
 ('positive', '{0.960784314, 0.498039216, 0.090196078}')  -- {245, 127, 23}/255
;

create table "language" (
 alpha3b varchar(3)  not null,
 alpha3t varchar(3),
 alpha2  varchar(2),
 name    text        not null,
 primary key (alpha3b),
 unique (name)
);
comment on table "language" is
 'ISO 639 alpha-3 bibliographic, alpha-3 terminology (when given), alpha-2'
 ' (when given) codes, and English name:'
 ' https://www.loc.gov/standards/iso639-2/langhome.html';
-- Supported languages
insert into "language" (alpha3b, alpha3t, alpha2, name) values
 ('eng', null, 'en', 'English'),
 ('por', null, 'pt', 'Portuguese')
;

create table gender (
 id text not null,
 primary key (id)
);
comment on table gender is
 'Identify gender option. See gender___translation for gender names in a'
 ' specific language (e.g., eng, por).';
-- Supported gender
insert into gender (id) values
 ('1'),
 ('12'),
 ('2'),
 ('0')
;

create table gender___translation (
 "language" varchar(3) not null,
 gender     text       not null,
 name       text       not null,
 primary key ("language", gender),
 foreign key("language") references "language" (alpha3b) on delete cascade
                                                         on update cascade,
 foreign key (gender) references gender (id) on delete cascade
                                             on update cascade
);
comment on table gender is
 'Match gender name for the gender option in a specific language. See gender.';
-- Supported gender translations
insert into gender___translation ("language", gender, name) values
 ('eng', '1', 'Female'),
 ('eng', '12', 'Intersex'),
 ('eng', '2', 'Male'),
 ('eng', '0', 'Neutral'),
 ('por', '1', 'Feminino'),
 ('por', '12', 'Intersexo'),
 ('por', '2', 'Masculino'),
 ('por', '0', 'Neutro')
;

-- CORE

create table entity (
 id       text not null,
 birthday date null,
 gender   text null,
 primary key (id)
);
comment on table entity is
 'Entity is a human with an anonymous id. Some useful data (e.g., gender) for'
 ' clustering analysis.';

create table collected (
 id                 bigserial,
 entity             text                    not null,
 moment             timestamp               not null,
 moment_tz          interval hour to minute not null,
 sensor_type        text                    not null,
 sensor_attribute   text                    not null,
 sensor_value       numeric                 not null,
 sensor_value_extra text                    null,
-- SEE BELOW. unique (entity, moment, moment_tz, sensor_type, sensor_attribute, sensor_value, sensor_value_extra),
 primary key (id),
 foreign key (entity) references entity (id) on delete cascade
);
comment on table collected is
 'The collected data from all sensors. The entity local moment is'
 ' moment + moment_tz (all collected timestamps are UTC).'
 ' The sensor_value_extra provides exceptional text information'
 ' (e.g., geo location provider like "fused").';
-- alter table collected add constraint collected___unique unique (entity, moment, moment_tz, sensor_type, sensor_attribute, sensor_value, sensor_value_extra);
-- ISSUE: Currently not possible to implement this restriction.
-- REASON: app.sensor.activity has duplicate data due to
--         (1) Google ActivityRecognitionClient[*] for Android behaviour:
--             Duplicate activity transition delivered sometimes and randomly.
--             [*] https://developers.google.com/android/reference/com/google/android/gms/location/ActivityRecognitionClient
--         (2) SensAI bug on listening twice. Solved: 0.1.25+

create table parameter (
 id        bigserial,
 entity    text                    not null,
 moment    timestamp               not null,
 moment_tz interval hour to minute not null,
 key       text                    not null,
 value     text                    not null,
 primary key (id),
 foreign key (entity) references entity (id) on delete cascade
);
comment on table parameter is
 'The application non-identifying parameters (e.g., version, collecting rate).';

-- STATUS (Model, Pipeline, ...)

create table model (
 entity         text           not null,
 name           text           not null,
 estimator      bytea          not null,
 score          numeric        not null,
 moment         timestamp      not null,
 fit_duration   interval       not null,
 score_duration interval       not null,
 extra          json           null,
 primary key (entity, name),
 foreign key (entity) references entity (id) on delete cascade
);
comment on table model is
 'The Expanse cognition pipeline model data relevant values to assist'
 ' on machine learning. "name" is the classifier full name as in sklearn.'
 ' "estimator" is for whole object cache (GZip pickle dumps).';

create table pipeline (
 entity           text         not null,
 data_wide        bytea        not null,
 data_wide_shape  int ARRAY[2] not null,
 data_wide_moment timestamp    null,
 data_wide_extra  json         null,
 status           text         not null,
 status_moment    timestamp    not null,
 duration         interval     null,
 primary key (entity),
 foreign key (entity) references entity (id) on delete cascade
);
comment on table pipeline is
 'The Expanse cognition pipeline data relevant values to assist'
 ' on machine learning. "data_wide" is for cache (GZip pickle dumps)'
 ' and "status" is for entity pipeline step tracking. "data_wide_shape"'
 ' saves processing on "data_wide" when only data size and features'
 ' (n_tuples, n_features + 1_target + 1_target_class) is required for analysis.'
 ' "data_wide_extra" for extra relevant information (e.g., class imbalance).';

--
--                            F U N C T I O N S
--

-- LIST OF VALUES (LOV)

create or replace function gender_names(language___p text)
returns table("gender" text, "name" text) as $$
 select gender, name from gender___translation
 where "language" = language___p;
$$ language sql stable strict;
comment on function gender_names(text) is
 'Get the list (LOV) of gender id and name in a specific language.'
 ' E.g., select gender_names(''eng'')';

create or replace function valence_colours()
returns table("name" text, "rgb" float ARRAY[3]) as $$
 select name, rgb from colour
 where "name" in ('negative', 'neutral', 'positive');
$$ language sql stable strict;
comment on function valence_colours() is
 'Get the list (LOV) of name and RGB of emotional valence colours.'
 ' E.g., select valence_colours()';

-- EXTRACT

create or replace function moment_entity(moment___p timestamp,
                                         moment_tz___p interval hour to minute)
returns timestamp as $$
 select moment___p + moment_tz___p as moment_entity;
$$ language sql immutable strict;
comment on function moment_entity(timestamp, interval hour to minute) is
 'Get the entity local moment, i.e., sum the given moment (all collected'
 ' timestamps are UTC) and entity time zone (restricted to minutes).'
 ' E.g.: select moment_entity(''2018-08-01 12:00:00.000'', ''+03:00'')';

create or replace function extract_collected(entity___p text,
                                             moment_begin___p timestamp,
                                             moment_end___p timestamp)
returns table("id" bigint,
              "moment" timestamp, "moment_utc" timestamp,
              "sensor_type" text, "sensor_attribute" text,
              "sensor_value" numeric, "sensor_value_extra" text) as $$
 select id,
        moment_entity(moment, moment_tz) as moment, moment as moment_utc,
        sensor_type, sensor_attribute,
        sensor_value, sensor_value_extra
 from collected
 where entity = entity___p
       and moment_entity(moment, moment_tz) >= moment_begin___p
       and moment_entity(moment, moment_tz) <= moment_end___p;
$$ language sql stable strict;
comment on function extract_collected(text, timestamp, timestamp) is
 'Get collected data restricted by entity and bounded by two moments'
 ' without time zone. Begin and end moments are converted to entity local'
 ' moments: ts+tz. E.g., select * from'
 ' extract_collected(''entity'', ''2018-08-01 12:00'', ''2018-08-01 14:00'')';

create or replace function extract_collected(sensor_type___p text,
                                             sensor_attribute___p text)
returns table("id" bigint, "entity" text,
              "moment" timestamp, "moment_utc" timestamp,
              "sensor_type" text, "sensor_attribute" text,
              "sensor_value" numeric, "sensor_value_extra" text) as $$
 select id, entity,
        moment_entity(moment, moment_tz) as moment, moment as moment_utc,
        sensor_type, sensor_attribute, sensor_value, sensor_value_extra
 from collected
 where sensor_type = sensor_type___p
       and sensor_attribute = sensor_attribute___p;
$$ language sql stable strict;
comment on function extract_collected(text, text) is
 'Get collected data restricted by sensor type and sensor attribute.'
 ' E.g., select extract_collected(''app.human.smile'', ''polarity'')';

create or replace function moment_from_min_eligible_version(entity___p text,
                                                            version___p int[])
returns timestamp as $$
 select moment
 from parameter
 where entity = entity___p
       and key = 'app_version'
       and string_to_array(value, '.', 'debug')::int[] >= version___p
       and array_length(string_to_array(value, '.', 'debug'), 1) = 3
 order by moment asc
 limit 1;
$$ language sql stable strict;
comment on function moment_from_min_eligible_version(text, int[]) is
 'Get moment from the entity minimum eligible version (>= version___p). E.g.,'
 ' select moment_from_min_eligible_version(''entity_hash'', ARRAY[0,1,16])';

-- STATUS (Model, Pipeline, ...)

create or replace function model_status(entity___p text)
returns table("name" text, "estimator" bytea, "score" numeric, "moment" timestamp,
              "fit_duration" interval, "score_duration" interval, "extra" json) as $$
 select name, estimator, score, moment, fit_duration, score_duration, extra
 from model
 where entity = entity___p;
$$ language sql stable strict;
comment on function model_status(text) is
 'Get all entity learning models, i.e., name, estimator, score, moment,'
 'fit_duration, score_duration, and extra data such as split.'
 ' E.g., select model_status(''entity_hash'')';

create or replace function model_status_update(
    entity___p text,
    name___p text, estimator___p bytea, score___p numeric, moment___p timestamp,
    fit_duration___p interval, score_duration___p interval, extra___p json)
returns boolean as $$
begin
 insert into model (entity, name, estimator, score, moment,
                    fit_duration, score_duration, extra)
  values (entity___p, name___p, estimator___p, score___p, moment___p,
          fit_duration___p, score_duration___p, extra___p)
  on conflict (entity, name) do update
   set estimator = excluded.estimator,
       score = excluded.score,
       moment = excluded.moment,
       fit_duration = excluded.fit_duration,
       score_duration = excluded.score_duration,
       extra = excluded.extra;
 return FOUND;
end;
$$ language plpgsql;
comment on function model_status_update(text, text, bytea, numeric, timestamp, interval, interval, json) is
 'Set or update an entity model status from current learning data process.'
 ' E.g., select model_status_update(''entity_hash'',''name'','
 ' ''\x00''::bytea, 0.9321, ''2002-02-02 02:02:00'', ''01:00'', ''01:30'', {"split": [0.8, 0.2]}::json)';

create or replace function pipeline_status(entity___p text)
returns table("data_wide" bytea, "data_wide_shape" int[], "data_wide_moment" timestamp,
              "data_wide_extra" json,
              "status" text, "status_moment" timestamp, "duration" interval) as $$
 select data_wide, data_wide_shape, data_wide_moment, data_wide_extra, status, status_moment, duration
 from pipeline
 where entity = entity___p
 limit 1;
$$ language sql stable strict;
comment on function pipeline_status(text) is
 'Get the entity pipeline status, moments and last data processed with success.'
 ' E.g., select pipeline_status(''entity_hash'')';

create or replace function pipeline_status_update(
    entity___p text,
    data_wide___p bytea, data_wide_shape___p int[], data_wide_moment___p timestamp, data_wide_extra___p json,
    status___p text, status_moment___p timestamp, duration___p interval)
returns boolean as $$
begin
 insert into pipeline
   (entity,
    data_wide, data_wide_shape, data_wide_moment, data_wide_extra,
    status, status_moment, duration)
  values
   (entity___p,
    data_wide___p, data_wide_shape___p, data_wide_moment___p, data_wide_extra___p,
    status___p, status_moment___p, duration___p)
  on conflict (entity) do update
   set data_wide = excluded.data_wide,
       data_wide_shape = excluded.data_wide_shape,
       data_wide_moment = excluded.data_wide_moment,
       data_wide_extra = excluded.data_wide_extra,
       status = excluded.status,
       status_moment = excluded.status_moment,
       duration = excluded.duration;
 return FOUND;
end;
$$ language plpgsql;
comment on function pipeline_status_update(text, bytea, int[], timestamp, json, text, timestamp, interval) is
 'Update entity pipeline status, moment and last data processed with success.'
 ' E.g., select pipeline_status_update(''entity_hash'','
 ' ''\x00''::bytea, ARRAY[123456, 42], ''2000-01-01 00:00'', ''{"imbalance": 0.0}''::json,'
 ' ''learning'', ''2002-02-02 02:02'', ''02:23'')';

create or replace function pipeline_status_step_update(
    entity___p text,
    data_wide_extra___p json,
    status___p text, status_moment___p timestamp, duration___p interval)
returns boolean as $$
begin
 update pipeline
  set data_wide_extra = data_wide_extra___p,
      status = status___p,
      status_moment = status_moment___p,
      duration = duration___p
  where entity = entity___p;
 return FOUND;
end;
$$ language plpgsql;
comment on function pipeline_status_step_update(text, json, text, timestamp, interval) is
 'Update entity pipeline status and moment for last process with success.'
 ' E.g., select pipeline_status_step_update(''entity_hash'','
 ' ''{"imbalance": 0.0}''::json, ''OK'', ''2002-02-02 02:02'', ''02:23'')';

-- STATISTICS

create or replace function entity_data_moments()
returns table("entity" text,
              "first" timestamp, "last" timestamp, "last_from_now" interval,
              "duration" interval) as $$
 select
  entity,
  date_trunc('second', min(moment)) as first,
  date_trunc('second', max(moment)) as last,
  date_trunc('second', (max(moment) - CURRENT_TIMESTAMP)) as last_from_now,
  date_trunc('second', (max(moment) - min(moment))) as duration
 from collected
 group by entity;
$$ language sql stable strict;
comment on function entity_data_moments() is
 'Get key moments from collected data for all entities, i.e., entity id,'
 ' first write, last write, last how long from now, total duration.'
 ' E.g., select * from entity_data_moments()';

create or replace function entity_demographics()
returns table("entity" text, "age" int, "gender" text) as $$
 select id, (current_date - birthday) / 365 as age, gender
 from entity;
$$ language sql stable strict;
comment on function entity_demographics() is
 'Get demographics for all entities, i.e., entity id, age in years and gender'
 ' in a numerical code as text. E.g., select entity_demographics()';

create or replace function entity_demographics(entity___p text)
returns table("age" int, "gender" text) as $$
 select (current_date - birthday) / 365 as age, gender
 from entity
 where id = entity___p
       and birthday is not null
 limit 1;
$$ language sql stable strict;
comment on function entity_demographics(text) is
 'Get the entity demographics, i.e., age in years and gender in a numerical'
 ' code as text. E.g., select entity_demographics(''entity_hash'')';

create or replace function entity_valence_reports_count()
returns table("entity" text,
              "negative" bigint, "neutral" bigint, "positive" bigint) as $$
 select
  entity,
  count(*) filter (where sensor_type = 'app.human.smile'
                   and sensor_attribute = 'polarity' and sensor_value = '-1')
   as negative,
  count(*) filter (where sensor_type = 'app.human.smile'
                   and sensor_attribute = 'polarity' and sensor_value = '0')
   as neutral,
  count(*) filter (where sensor_type = 'app.human.smile'
                   and sensor_attribute = 'polarity' and sensor_value = '1')
   as positive
 from collected
 group by entity;
$$ language sql stable strict;
comment on function entity_valence_reports_count() is
 'Get (emotional) valence reports count for all entities, i.e., entity id,'
 ' negative, neutral, positive count. E.g., select * from'
 ' entity_valence_reports_count()';

create or replace function entity_demographics_valence_reports_data_moments()
returns table("entity" text, "age" int, "gender" text,
              "negative" bigint, "neutral" bigint, "positive" bigint,
              "first" timestamp, "last" timestamp, "last_from_now" interval,
              "duration" interval) as $$
 select d.entity, d.age, d.gender, v.negative, v.neutral, v.positive,
        m.first, m.last, m.last_from_now, m.duration
 from entity_demographics() as d,
      entity_valence_reports_count() as v,
      entity_data_moments() as m
 where d.entity = v.entity
       and v.entity = m.entity;
$$ language sql stable strict;
comment on function entity_demographics_valence_reports_data_moments() is
 'Get demographics, (emotional) valence reports count, and data moments for all'
 ' entities, i.e., entity, age, gender, valence (negative, neutral, positive)'
 ' count, key (e.g., first write) moments and duration.'
 ' E.g., select * from entity_demographics_valence_reports_data_moments()';
