import sys
sys.path.insert(0, "/opt/sensei-expanse")


activate_this = "/opt/sensei-expanse/.venv/bin/activate_this.py"
with open(activate_this) as file_:
    exec(file_.read(), dict(__file__=activate_this))


from memory.secure.web_service import app as application
