#  Copyright 2019 Nuno A. C. Henriques [nunoachenriques.net]
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

"""
SensAI Expanse secure layer as a Python Web application for Apache Web Server
(WSGI) using Flask:

 * Authenticate: use Google OAuth2. Receive an ID token from SensAI app
   authenticated user to check and secure access only to the data owner.

 * Anonymize: use Google user unique ID (legacy: email) to hash and
   create an anonymous unique ID for data storage.

This service JSON request format::

 {
     ID_TOKEN_NAME: "...",
     EXPANSE_REQUEST_NAME: {
         ACTION_NAME: [
             {
                 FIRST ENTRY
             },
             {
                 LAST ENTRY
             }
         ]
     }
 }

See `web_service.schema.json`.

See https://developers.google.com/identity/sign-in/android/backend-auth

Requires: flask, jsonschema, psycopg2,
          google.auth.transport.requests, google.oauth2.id_token
"""

import configparser
import hashlib
import json
import logging as log
import os
import re
from datetime import datetime
from typing import Any, Dict, List, Tuple

import flask
import jsonschema
import psycopg2 as pg
import psycopg2.extras as pge
from google.auth.transport import requests as google_req
from google.oauth2 import id_token

from cognition.pipeline import Pipeline
from cognition.prediction import Prediction

# CONFIGURE
PATH = os.path.dirname(__file__)
NAME = os.path.splitext(os.path.basename(__file__))[0]
INI_FILE = os.path.join(PATH, f"{NAME}.ini")
_config = configparser.ConfigParser()
if not _config.read(INI_FILE):
    raise RuntimeError("CONFIGURATION file read FAILED!"
                       f" Hint: create {INI_FILE} file with proper values.")
CACHE_ROOT = _config["cache"].get("root", ".")

# PROTOCOL
with open(os.path.join(PATH, f"{NAME}.schema.json")) as schema:
    SCHEMA = json.load(schema)
with open(os.path.join(PATH, f"{NAME}.json.secret")) as secrets:
    CLIENT_ID_LIST = json.load(secrets)["client_id_list"]
DATABASE = _config["database"]["name"]
DATABASE_USER = _config["database"]["user"]
REQUEST_NAME = _config["request"]["name"]
ID_TOKEN_NAME = _config["request"]["id_token_name"]
RESPONSE_OK = "ok"
RESPONSE_ERROR = "error"
WS_PATH = "/expanse" if __name__ == "__main__" else "/"

# PREDICTION
_prediction = Prediction()

# SQL (API) queries
SET_ENTITY_SQL = """
insert into entity
 (id, birthday, gender)
values
 (%s, %s, %s)
on conflict (id)
 do update set
  birthday = excluded.birthday,
  gender = excluded.gender;
"""
SET_PARAMETER_SQL = """
insert into parameter
 (entity, moment, moment_tz, key, value)
values
 %s
"""
SET_COLLECTED_SQL = """
insert into collected
 (entity, moment, moment_tz, sensor_type, sensor_attribute, sensor_value, sensor_value_extra)
values
 %s
"""

# DEBUG
DEBUG = _config["debug"].getboolean("mode")
DEBUG_USER = _config["debug"].get("user", None)

# LOGGING
LOG_SETTINGS = {
    "format": "%(asctime)s | %(levelname)-8s %(module)s: %(message)s",
    "datefmt": "%Y-%m-%d %H:%M:%S",
    "level": log.DEBUG if DEBUG else log.INFO
}
LOG_FILE = _config["logging"].get("output", None)
if LOG_FILE is not None:
    os.makedirs(CACHE_ROOT, mode=0o755, exist_ok=True)
    LOG_SETTINGS.update({"filename": os.path.join(CACHE_ROOT, LOG_FILE),
                         "filemode": "w"})
log.basicConfig(**LOG_SETTINGS)
VERBOSE = _config["logging"].getboolean("verbose")
if not VERBOSE:
    log.getLogger().setLevel(log.CRITICAL)
else:
    log.getLogger().setLevel(log.DEBUG if DEBUG else log.INFO)
log.debug(f"\nlog_settings {json.dumps(obj=LOG_SETTINGS, indent=4)}"
          f"\nconfig"
          f" {json.dumps([(s, i) for s, i in ((s, dict(_config.items(s))) for s in _config.sections())], indent=4)}")
log.debug(f"CLIENT_ID_LIST: {CLIENT_ID_LIST}")
log.info(f"DEBUG: {DEBUG}")
log.debug(f"DEBUG_USER: {DEBUG_USER}")
log.info(f"PATH: {PATH}")
log.info(f"NAME: {NAME}")
log.info(f"CACHE_ROOT: {CACHE_ROOT}")
log.info(f"WS_PATH: {WS_PATH}")

# SESSION including WSGI such as Apache2
app = flask.Flask(__name__)
# Flask session key: http://flask.pocoo.org/docs/1.0/quickstart/#sessions
app.secret_key = f"{os.urandom(16)}"
app.logger.setLevel(log.DEBUG if DEBUG else log.INFO)


# SERVICE
@app.route(WS_PATH, methods=["POST"])  # 405 Method Not Allowed if GET
def expanse():
    """
    Get data, validate data, verify ID token, SYNC data, return RESPONSE.

    :return: Response in JSON format.
    """
    data_in = flask.request.get_json()
    log.debug(f"data_in {json.dumps(obj=data_in, indent=4, sort_keys=False)}")
    try:
        jsonschema.validate(instance=data_in, schema=SCHEMA)
        # WARNING DEBUG opens an access to a default user for testing!
        #         Request OAuth ID token is NOT verified!
        #         This is safe as long as (a) the USER defined in .ini is a
        #         DUMMY one; (b) the DATA is CREATED for testing purposes; and
        #         (c) there is a local http server + database.
        if DEBUG:
            user = dict(uid=DEBUG_USER, email=DEBUG_USER)
        else:
            user = validate_google_id_token(data_in[ID_TOKEN_NAME])
        log.debug(f"user {json.dumps(obj=user, indent=4)}")
        action, result, by = sync_expanse(request=data_in[REQUEST_NAME], user_id=user["email"])
    except (jsonschema.exceptions.SchemaError, jsonschema.exceptions.ValidationError) as e:
        flask.abort(400, e)
    except (KeyError, ValueError) as e:
        flask.abort(403, e)
    except (IOError, pg.Error) as e:
        flask.abort(500, e)
    else:
        log.info(f"{action} by {by[:10]}...")
        if re.match("set_", action):
            log.debug(f"Number of records: {result}")
            return flask.jsonify({f"{RESPONSE_OK}": f"{action} {result} records"}), 200
        elif re.match("get_", action):
            log.debug(f"Result:\n{result}")
            response = {f"{RESPONSE_OK}": result}
            log.debug(f"Response:\n{response}")
            response_json = json.dumps(response)
            log.debug(f"Response JSON:\n{response_json}")
            return response_json, 200
        else:
            flask.abort(501)


@app.errorhandler(400)
def abort_bad_request(error):
    log.debug(error)
    return flask.jsonify({f"{RESPONSE_ERROR}": f"{error}"}), 400


@app.errorhandler(403)
def abort_forbidden(error):
    log.debug(error)
    return flask.jsonify({f"{RESPONSE_ERROR}": f"{error}"}), 403


@app.errorhandler(500)
def abort_internal_server_error(error):
    log.error(error)
    return flask.jsonify({f"{RESPONSE_ERROR}": f"{error}"}), 500


@app.errorhandler(501)
def abort_not_implemented(error):
    log.error(error)
    return flask.jsonify({f"{RESPONSE_ERROR}": f"{error}"}), 501


def validate_google_id_token(token: str) -> Dict:
    """
    Verify Google sign in OAuth2 server issued ID token.
    Moreover, check `audience` (client_id from the requesting app), `issuer`
    from Google, and email provided by app user.

    :param token: Token char sequence encoded data from request.
    :return: User data (e.g., user unique identifier, email).
    :raise ValueError: on error.
    """
    id_info = id_token.verify_oauth2_token(token, google_req.Request())
    if id_info['aud'] not in CLIENT_ID_LIST:
        raise ValueError("Audience is WRONG")
    if id_info["iss"] not in ["accounts.google.com", "https://accounts.google.com"]:
        raise ValueError("Issuer is WRONG")
    if not id_info["email_verified"]:
        raise ValueError("Email is WRONG")
    return dict(uid=id_info["sub"], email=id_info["email"])


def sync_expanse(request: Dict, user_id: str) -> Tuple[str, Any, str]:
    """
    Synchronize data between SensAI and Expanse resources.
    Set user collected data. On error call abort with HTTP error code.

    :param request: Expanse request using a JSON schema.
    :param user_id: User identifier from ID token (e.g., email).
    :return: Action name, result data, entity id (e.g., "set_entity", 1, "x").
    :raise HTTPException: on error calling flask.abort(status_code).
    """
    # WARNING DEBUG opens an access to a default user for testing!
    #         CHECK expanse() for more information.
    if DEBUG:
        entity = user_id
    else:
        entity = hashlib.sha256(user_id.encode()).hexdigest()
    if "set_entity" in request:
        data: Dict = request.get("set_entity")[0]
        with get_db():
            with get_db().cursor() as cursor:
                cursor.execute(SET_ENTITY_SQL, (entity, data["birthday"], data["gender"]))
        return "set_entity", 1, entity
    elif "set_parameter" in request:
        data: List[Dict] = request.get("set_parameter")
        with get_db():
            with get_db().cursor() as cursor:
                pge.execute_values(cursor, SET_PARAMETER_SQL, data,
                                   f"('{entity}', %(moment)s, cast (%(moment_tz)s as interval), %(key)s, %(value)s)")
        return "set_parameter", len(data), entity
    elif "set_collected" in request:
        data: List[Dict] = request.get("set_collected")
        with get_db():
            with get_db().cursor() as cursor:
                pge.execute_values(cursor, SET_COLLECTED_SQL, data,
                                   f"('{entity}', %(moment)s, cast (%(moment_tz)s as interval),"
                                   f" %(sensor_type)s, %(sensor_attribute)s, %(sensor_value)s, %(sensor_value_extra)s)")
        return "set_collected", len(data), entity
    elif "get_prediction" in request:
        result = list()
        data: Dict = request.get("get_prediction")
        data_context: List[Dict] = data.get("context")
        data_context_prediction: List[Tuple[float, float, datetime]] = list()
        for item in data_context:
            data_context_prediction.append(
                (
                    float(item["latitude"]),
                    float(item["longitude"]),
                    datetime.fromisoformat(item["moment"])
                ),
            )
        data_mode: str = data.get("mode")
        if data_mode == "all":
            data_probabilities: bool = data.get("probabilities")
            data_feature_importances: bool = data.get("feature_importances")
            result = _prediction.valence(
                entity, data_context_prediction,
                Pipeline.MGRS_PRECISION, Pipeline.MGRS_PRECISION_PREFIX,
                data_probabilities, data_feature_importances
            )
        elif data_mode == "best":
            data_probabilities: bool = data.get("probabilities")
            data_feature_importances: bool = data.get("feature_importances")
            result = _prediction.valence_best(
                entity, data_context_prediction,
                Pipeline.MGRS_PRECISION, Pipeline.MGRS_PRECISION_PREFIX,
                data_probabilities, data_feature_importances
            )
        elif data_mode == "estimator":
            data_estimator: str = data.get("estimator")
            data_probabilities: bool = data.get("probabilities")
            data_feature_importances: bool = data.get("feature_importances")
            result = _prediction.valence_estimator(
                data_estimator,
                entity, data_context_prediction,
                Pipeline.MGRS_PRECISION, Pipeline.MGRS_PRECISION_PREFIX,
                data_probabilities, data_feature_importances
            )
        elif data_mode == "voting":
            result = _prediction.valence_voting(
                entity, data_context_prediction,
                Pipeline.MGRS_PRECISION, Pipeline.MGRS_PRECISION_PREFIX
            )
        return "get_prediction", result, entity
    else:
        flask.abort(501)


def get_db() -> Any:
    if "expanse_database" not in flask.g:
        flask.g.expanse_database = pg.connect(dbname=DATABASE, user=DATABASE_USER)
    return flask.g.expanse_database


@app.teardown_appcontext
def teardown_db(error):
    if error is not None:
        log.error(error)
    db = flask.g.pop("expanse_database", None)
    if db is not None:
        db.close()


# SERVER STANDALONE
if __name__ == "__main__":
    context = ("self_signed_crt.pem.secret", "self_signed_key.pem.secret")
    app.run(host="localhost", port=5000, ssl_context=context, threaded=True, debug=DEBUG)
