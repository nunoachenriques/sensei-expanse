#  Copyright 2019 Nuno A. C. Henriques [nunoachenriques.net]
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

"""
Test ``prediction`` module.

TODO database mock up to avoid database dependency.
"""

import configparser
import datetime as dt
import gzip
import os
import unittest

import numpy as np

from cognition.expanse import Expanse
from cognition.pipeline import Pipeline
from cognition.prediction import Prediction


class TestPrediction(unittest.TestCase):
    _expanse = None
    _db_settings = None
    _ENTITY = "x"
    _ENTITY_NON_EXISTENT = "x_non_existent"

    @classmethod
    def setUpClass(cls):
        config = configparser.ConfigParser()
        path = os.path.dirname(__file__)
        ini_file = os.path.join(path, "test_expanse.ini")
        if not config.read(ini_file):
            raise RuntimeError("CONFIGURATION file read FAILED!"
                               f" Hint: create {ini_file} file with proper values.")
        # NOTICE: create user and database, give permissions in pg_hba.conf.
        cls._expanse = Expanse()
        cls._expanse.connect(config["database"]["name"], config["database"]["user"])
        cls._db_settings = dict()
        cls._db_settings["name"] = config["database"]["name"]
        cls._db_settings["user"] = config["database"]["user"]
        # CLEAN values (delete entity cascades to all)
        with cls._expanse.connection as connection:
            with connection.cursor() as cursor:
                cursor.execute("delete from entity where id = %s", (cls._ENTITY,))
        # DATA SET
        with cls._expanse.connection as connection:
            with connection.cursor() as cursor:
                cursor.execute(
                    "insert into entity (id, birthday, gender) values (%s, %s, %s)",
                    (cls._ENTITY, dt.date(2000, 1, 1), "0")
                )
                with gzip.open(os.path.join(path, "test_prediction_model.copy.gz")) as file:
                    cursor.copy_from(file, "model")
                with gzip.open(os.path.join(path, "test_prediction_pipeline.copy.gz")) as file:
                    cursor.copy_from(file, "pipeline")

    @classmethod
    def tearDownClass(cls):
        # NOTICE: last data inserted kept for forensics
        cls._expanse.disconnect()

    def test_cache_update_required(self):
        prediction = Prediction(db_name=self._db_settings["name"], db_user=self._db_settings["user"])
        result = prediction.cache_update_required(self._ENTITY)
        self.assertTrue(result)

    def test_cache_update(self):
        prediction = Prediction(db_name=self._db_settings["name"], db_user=self._db_settings["user"])
        # Test FALSE: NO DATABASE MODEL or PIPELINE DATA for _ENTITY_NON_EXISTENT
        result = prediction.cache_update(self._ENTITY_NON_EXISTENT)
        self.assertFalse(result)
        # Test TRUE: DATABASE MODEL and PIPELINE DATA for _ENTITY
        result = prediction.cache_update(self._ENTITY)
        self.assertTrue(result)

    def test_valence(self):
        prediction = Prediction(db_name=self._db_settings["name"], db_user=self._db_settings["user"])
        # Test FAIL: NO DATABASE MODEL or PIPELINE DATA for this context
        result1 = prediction.valence(self._ENTITY,
                                     [(38.0, 9.0, dt.datetime(2020, 2, 2, 1, 0)),
                                      (38.0, 9.0, dt.datetime(2020, 2, 2, 2, 0))],
                                     Pipeline.MGRS_PRECISION,
                                     Pipeline.MGRS_PRECISION_PREFIX)
        self.assertEqual([None, None], result1, "Prediction result1 FAILED!")
        # Test SUCCEED: DATABASE MODEL and PIPELINE DATA for this context
        result2 = prediction.valence(self._ENTITY,
                                     [(38.72, -9.34, dt.datetime(2018, 11, 17, 1, 26))],
                                     Pipeline.MGRS_PRECISION,
                                     Pipeline.MGRS_PRECISION_PREFIX)
        for k, v in iter(result2[0].items()):
            if k in ["sklearn.dummy.DummyClassifier",
                     "sklearn.linear_model.logistic.LogisticRegression"]:
                prediction_expected = [1]
            else:
                prediction_expected = [0]
            self.assertEqual(prediction_expected, v["prediction"], f"Prediction result2 from {k} FAILED!")
        # Test SUCCEED: FIRST OK, SECOND None
        result3 = prediction.valence(self._ENTITY,
                                     [(38.72, -9.34, dt.datetime(2018, 11, 17, 1, 26)),
                                      (38.0, 9.0, dt.datetime(2020, 2, 2, 1, 0))],
                                     Pipeline.MGRS_PRECISION,
                                     Pipeline.MGRS_PRECISION_PREFIX)
        for i, result in enumerate(result3):
            if i == 0:
                for k, v in iter(result.items()):
                    if k in ["sklearn.dummy.DummyClassifier",
                             "sklearn.linear_model.logistic.LogisticRegression"]:
                        prediction_expected = [1]
                    else:
                        prediction_expected = [0]
                    self.assertEqual(prediction_expected, v["prediction"], f"Prediction result3 from {k} FAILED!")
            else:
                self.assertIsNone(result, f"Prediction result3={result} FAILED!")
        # Test SUCCEED: DATABASE MODEL and PIPELINE DATA for this context
        result4 = prediction.valence(self._ENTITY,
                                     [(38.72, -9.34, dt.datetime(2018, 11, 17, 1, 26))],
                                     Pipeline.MGRS_PRECISION,
                                     Pipeline.MGRS_PRECISION_PREFIX,
                                     True)
        prediction_classes_expected = [0, 1]
        for k, v in iter(result4[0].items()):
            if k == "sklearn.dummy.DummyClassifier":
                prediction_expected = [0.0, 1.0]
            elif k == "sklearn.linear_model.logistic.LogisticRegression":
                prediction_expected = [0.000107, 0.999893]
            elif k == "xgboost.sklearn.XGBClassifier":
                prediction_expected = [0.954152, 0.045848]
            elif k == "cognition.learn.KerasClassifier":
                prediction_expected = [1.0, 0.0]
            else:
                prediction_expected = [None, None]
            self.assertEqual(prediction_expected, np.round(v["prediction"][0], 6).tolist(),
                             f"Prediction result4 for {k} FAILED!")
            self.assertEqual(prediction_classes_expected, v["prediction_classes"],
                             f"Prediction result4 classes for {k} FAILED!")

    def test_valence_best(self):
        prediction = Prediction(db_name=self._db_settings["name"], db_user=self._db_settings["user"])
        # Test FAIL: NO DATABASE MODEL or PIPELINE DATA for this context
        result1 = prediction.valence_best(self._ENTITY,
                                          [(38.0, 9.0, dt.datetime(2020, 2, 2, 1, 0))],
                                          Pipeline.MGRS_PRECISION,
                                          Pipeline.MGRS_PRECISION_PREFIX)
        self.assertEqual([None], result1, "Prediction result1 FAILED!")
        # Test SUCCEED: FIRST None, SECOND OK
        result2 = prediction.valence_best(self._ENTITY,
                                          [(38.0, 9.0, dt.datetime(2020, 2, 2, 1, 0)),
                                           (38.72, -9.34, dt.datetime(2018, 11, 17, 1, 26))],
                                          Pipeline.MGRS_PRECISION,
                                          Pipeline.MGRS_PRECISION_PREFIX)
        self.assertEqual(
            [None, [0]], [result2[0], result2[1]["xgboost.sklearn.XGBClassifier"]["prediction"]],
            "Prediction result2 FAILED!"
        )
        # Test SUCCEED: FIRST None, SECOND OK now with PROBABILITIES
        result3 = prediction.valence_best(self._ENTITY,
                                          [(38.0, 9.0, dt.datetime(2020, 2, 2, 1, 0)),
                                           (38.72, -9.34, dt.datetime(2018, 11, 17, 1, 26))],
                                          Pipeline.MGRS_PRECISION,
                                          Pipeline.MGRS_PRECISION_PREFIX,
                                          True)
        # result3 0
        self.assertIsNone(result3[0], "Prediction result3[0] FAILED!")
        # result3 1
        result3_1_prediction_classes = result3[1]["xgboost.sklearn.XGBClassifier"]["prediction_classes"]
        self.assertEqual([0, 1], result3_1_prediction_classes, "Prediction classes result3[1] FAILED!")
        result3_1_prediction = result3[1]["xgboost.sklearn.XGBClassifier"]["prediction"][0]
        self.assertEqual([0.954152, 0.045848], np.round(result3_1_prediction, 6).tolist(),
                         "Prediction result3[1] FAILED!")
        # Test SUCCEED: FIRST None, SECOND OK now with PROBABILITIES and FEATURES
        result4 = prediction.valence_best(self._ENTITY,
                                          [(38.0, 9.0, dt.datetime(2020, 2, 2, 1, 0)),
                                           (38.72, -9.34, dt.datetime(2018, 11, 17, 1, 26))],
                                          Pipeline.MGRS_PRECISION,
                                          Pipeline.MGRS_PRECISION_PREFIX,
                                          True,
                                          True)
        # result4 0
        self.assertIsNone(result4[0], "Prediction result4[0] FAILED!")
        # result4 1
        result4_1_prediction_classes = result4[1]["xgboost.sklearn.XGBClassifier"]["prediction_classes"]
        self.assertEqual([0, 1], result4_1_prediction_classes, "Prediction classes result4[1] FAILED!")
        result4_1_prediction = result4[1]["xgboost.sklearn.XGBClassifier"]["prediction"][0]
        self.assertEqual([0.954152, 0.045848], np.round(result4_1_prediction, 6).tolist(),
                         "Prediction result4[1] FAILED!")
        result4_1_features = result4[1]["xgboost.sklearn.XGBClassifier"]["feature_importances"].keys()
        self.assertEqual(46, len(result4_1_features), "Feature importances result4[1] FAILED!")

    def test_valence_estimator(self):
        prediction = Prediction(db_name=self._db_settings["name"], db_user=self._db_settings["user"])
        # Test FAIL: NO DATABASE MODEL or PIPELINE DATA for this context
        result1 = prediction.valence_estimator("sklearn.dummy.DummyClassifier",
                                               self._ENTITY,
                                               [(38.0, 9.0, dt.datetime(2020, 2, 2, 1, 0))],
                                               Pipeline.MGRS_PRECISION,
                                               Pipeline.MGRS_PRECISION_PREFIX)
        self.assertEqual([None], result1, "Prediction result1 FAILED!")
        # Test SUCCEED: DATABASE MODEL and PIPELINE DATA for this context
        result2 = prediction.valence_estimator("sklearn.dummy.DummyClassifier",
                                               self._ENTITY,
                                               [(38.0, 9.0, dt.datetime(2020, 2, 2, 1, 0)),
                                                (38.72, -9.34, dt.datetime(2018, 11, 17, 1, 26))],
                                               Pipeline.MGRS_PRECISION,
                                               Pipeline.MGRS_PRECISION_PREFIX)
        self.assertEqual([None, [1]], [result2[0], result2[1]["prediction"]], "Prediction result2 FAILED!")

    def test_valence_voting(self):
        prediction = Prediction(db_name=self._db_settings["name"], db_user=self._db_settings["user"])
        # Test FAIL: NO DATABASE MODEL or PIPELINE DATA for this context
        result1 = prediction.valence_voting(self._ENTITY,
                                            [(38.0, 9.0, dt.datetime(2020, 2, 2, 1, 0))],
                                            Pipeline.MGRS_PRECISION,
                                            Pipeline.MGRS_PRECISION_PREFIX)
        self.assertEqual([None], result1, "Prediction result1 FAILED!")
        # Test SUCCEED: DATABASE MODEL and PIPELINE DATA for this context
        result2 = prediction.valence_voting(self._ENTITY,
                                            [(38.72, -9.34, dt.datetime(2018, 11, 17, 1, 26))],
                                            Pipeline.MGRS_PRECISION,
                                            Pipeline.MGRS_PRECISION_PREFIX)
        self.assertEqual([0], result2, "Prediction result2 FAILED!")
        # Test SUCCEED: FIRST OK, SECOND None
        result3 = prediction.valence_voting(self._ENTITY,
                                            [(38.72, -9.34, dt.datetime(2018, 11, 17, 1, 26)),
                                             (38.0, 9.0, dt.datetime(2020, 2, 2, 1, 0))],
                                            Pipeline.MGRS_PRECISION,
                                            Pipeline.MGRS_PRECISION_PREFIX)
        self.assertEqual([0, None], result3, "Prediction result3 FAILED!")


if __name__ == "__main__":
    unittest.main()
