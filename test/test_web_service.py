#  Copyright 2019 Nuno A. C. Henriques [nunoachenriques.net]
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

"""
Test ``web_service`` app.

TODO: proper testing and coverage. Currently, only request schema is tested!
NOTICE: however, if in DEBUG mode and local http server + database then
        some basic set and get testing happens. See ``test_a_debug``!
"""

import unittest
import warnings

from memory.secure.web_service import app


class TestWebService(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        app.testing = True
        app.debug = (app.logger.getEffectiveLevel() == 10)  # 10 = logging.DEBUG
        cls._app = app.test_client()

    def test_a_debug(self):
        # NOTICE: entity ID is the one set in web_service.ini DEBUG_USER!
        if app.debug:
            with self._app.post("/",
                                json={
                                   "oauth_id_token": "x.x.x",
                                   "expanse_request": {
                                       "set_entity": [
                                           {"birthday": "2000-01-01", "gender": "0"}
                                       ]
                                   }
                                }) as response:
                app.logger.info("%s", response.get_json())
                self.assertEqual(200, response.status_code)
            with self._app.post("/",
                                json={
                                   "oauth_id_token": "x.x.x",
                                   "expanse_request": {
                                       "set_collected": [
                                           {"moment": "2000-01-01T00:00:00.000+00:00",
                                            "moment_tz": "+01:00",
                                            "sensor_type": "app.sensor.geopositioning",
                                            "sensor_attribute": "latitude",
                                            "sensor_value": 0.12345,
                                            "sensor_value_extra": "DEBUG"},
                                           {"moment": "2000-01-01T00:00:00.000+00:00",
                                            "moment_tz": "+01:00",
                                            "sensor_type": "app.sensor.geopositioning",
                                            "sensor_attribute": "longitude",
                                            "sensor_value": 0.67890,
                                            "sensor_value_extra": "DEBUG"}
                                       ]
                                   }
                                }) as response:
                app.logger.info("%s", response.get_json())
                self.assertEqual(200, response.status_code)
            with self._app.post("/",
                                json={
                                    "oauth_id_token": "x.x.x",
                                    "expanse_request": {
                                        "get_prediction": {
                                            "mode": "estimator",
                                            "context": [
                                                {"moment": "2001-01-01T01:01:01.000+00:00",
                                                 "latitude": 1.12345,
                                                 "longitude": 1.54321},
                                                {"moment": "2002-02-02T02:02:02.000+00:00",
                                                 "latitude": 2.12345,
                                                 "longitude": 2.54321}
                                            ],
                                            "estimator": "xgboost.sklearn.XGBClassifier",
                                            "probabilities": True,
                                            "feature_importances": True
                                        }
                                    }
                                }) as response:
                app.logger.info("%s", response.get_json())
                self.assertEqual(200, response.status_code)

    def test_a_request_method_not_allowed(self):
        with self._app.get("/") as response:
            self.assertEqual(405, response.status_code)

    def test_a_request_empty(self):
        with self._app.post("/", json={}) as response:
            app.logger.info("%s", response.get_json())
            self.assertEqual(400, response.status_code)

    def test_a_request_empty_expanse(self):
        with self._app.post("/",
                            json={
                               "oauth_id_token": "x.x.x",
                               "expanse_request": {}
                            }) as response:
            app.logger.info("%s", response.get_json())
            self.assertEqual(400, response.status_code)

    def test_a_request_empty_oauth_token(self):
        # Google id_token.verify_oauth2_token [...] unclosed <ssl.SSLSocket [...]
        warnings.filterwarnings(action="ignore", message="unclosed", category=ResourceWarning)
        with self._app.post("/",
                            json={
                               "oauth_id_token": "",
                               "expanse_request": {
                                   "set_entity": [
                                       {"birthday": "2000-01-01", "gender": "1"}
                                   ]
                               }
                            }) as response:
            app.logger.info("%s", response.get_json())
            if app.debug:
                self.assertEqual(200, response.status_code)
            else:
                self.assertEqual(403, response.status_code)

    def test_get_prediction_all(self):
        # Google id_token.verify_oauth2_token [...] unclosed <ssl.SSLSocket [...]
        warnings.filterwarnings(action="ignore", message="unclosed", category=ResourceWarning)
        with self._app.post("/",
                            json={
                               "oauth_id_token": "x.x.x",
                               "expanse_request": {
                                   "get_prediction": {
                                       "mode": "all",
                                       "context": [
                                           {"moment": "2001-01-01T01:01:01.000+00:00",
                                            "latitude": 1.12345,
                                            "longitude": 1.54321},
                                           {"moment": "2002-02-02T02:02:02.000+00:00",
                                            "latitude": 2.12345,
                                            "longitude": 2.54321}
                                       ],
                                       "estimator": None,
                                       "probabilities": True,
                                       "feature_importances": True
                                   }
                               }
                            }) as response:
            app.logger.info("%s", response.get_json())
            if app.debug:
                self.assertEqual(200, response.status_code)
            else:
                self.assertEqual(403, response.status_code)

    def test_get_prediction_estimator(self):
        # Google id_token.verify_oauth2_token [...] unclosed <ssl.SSLSocket [...]
        warnings.filterwarnings(action="ignore", message="unclosed", category=ResourceWarning)
        with self._app.post("/",
                            json={
                               "oauth_id_token": "x.x.x",
                               "expanse_request": {
                                   "get_prediction": {
                                       "mode": "estimator",
                                       "context": [
                                           {"moment": "2001-01-01T01:01:01.000+00:00",
                                            "latitude": 1.12345,
                                            "longitude": 1.54321},
                                           {"moment": "2002-02-02T02:02:02.000+00:00",
                                            "latitude": 38.72,
                                            "longitude": -9.34}
                                       ],
                                       "estimator": "xgboost.sklearn.XGBClassifier",
                                       "probabilities": True,
                                       "feature_importances": True
                                   }
                               }
                            }) as response:
            app.logger.info("%s", response.get_json())
            if app.debug:
                self.assertEqual(200, response.status_code)
            else:
                self.assertEqual(403, response.status_code)

    # TODO in future proper testing, a get_prediction_best request/response:
    #
    # REQUEST:
    # {
    #   "oauth_id_token": "x.x.x",
    #   "expanse_request": {
    #     "get_prediction": {
    #       "mode": "best",
    #       "context": [
    #         {
    #           "moment": "2001-01-01T01:01:01.000+00:00",
    #           "latitude": 38.0,
    #           "longitude": 9.0
    #         },
    #         {
    #           "moment": "2002-02-02T02:02:02.000+00:00",
    #           "latitude": 38.72,
    #           "longitude": -9.34
    #         }
    #       ],
    #       "estimator": null,
    #       "probabilities": True,
    #       "feature_importances": True
    #     }
    #   }
    # }
    #
    # RESPONSE (after JSON):
    # {
    #   "ok": [
    #     null,
    #     {"xgboost.sklearn.XGBClassifier": {
    #       "score": "0.9883720930232558",
    #       "prediction_classes": [0, 1],
    #       "prediction": "[[0.95415205 0.04584794]]",
    #       "feature_importances": "{"moment_dow": 0.934567, ...}"}}
    #   ]
    # }

    def test_set_collected_extra_entity(self):
        with self._app.post("/",
                            json={
                               "oauth_id_token": "x.x.x",
                               "expanse_request": {
                                   "set_collected": [
                                       {"entity": "x",
                                        "moment": "2000-01-01T00:00:00.000+00:00",
                                        "moment_tz": "+01:00",
                                        "sensor_type": "sensor_type",
                                        "sensor_attribute": "sensor_attribute",
                                        "sensor_value": 0.123456789,
                                        "sensor_value_extra": "sensor_value_extra"}
                                   ]
                               }
                            }) as response:
            app.logger.info("%s", response.get_json())
            self.assertEqual(400, response.status_code)

    def test_set_collected_value_none_to_null(self):
        # Google id_token.verify_oauth2_token [...] unclosed <ssl.SSLSocket [...]
        warnings.filterwarnings(action="ignore", message="unclosed", category=ResourceWarning)
        with self._app.post("/",
                            json={
                               "oauth_id_token": "x.x.x",
                               "expanse_request": {
                                   "set_collected": [
                                       {"moment": "2000-01-01T00:00:00.000+00:00",
                                        "moment_tz": "+01:00",
                                        "sensor_type": "sensor_type",
                                        "sensor_attribute": "sensor_attribute",
                                        "sensor_value": 0.123456789,
                                        "sensor_value_extra": None}
                                   ]
                               }
                            }) as response:
            app.logger.info("%s", response.get_json())
            if app.debug:
                self.assertEqual(200, response.status_code)
            else:
                self.assertEqual(403, response.status_code)

    def test_set_entity_extra_id(self):
        with self._app.post("/",
                            json={
                               "oauth_id_token": "x.x.x",
                               "expanse_request": {
                                   "set_entity": [
                                       {"id": "x", "birthday": "2000-01-01", "gender": "1"}
                                   ]
                               }
                            }) as response:
            app.logger.info("%s", response.get_json())
            self.assertEqual(400, response.status_code)

    def test_set_entity_more(self):
        with self._app.post("/",
                            json={
                               "oauth_id_token": "x.x.x",
                               "expanse_request": {
                                   "set_entity": [
                                       {"birthday": "2000-01-01", "gender": "1"},
                                       {"birthday": "2000-01-01", "gender": "1"}
                                   ]
                               }
                            }) as response:
            app.logger.info("%s", response.get_json())
            self.assertEqual(400, response.status_code)

    def test_set_parameter_extra_entity(self):
        with self._app.post("/",
                            json={
                               "oauth_id_token": "x.x.x",
                               "expanse_request": {
                                   "set_parameter": [
                                       {"entity": "x",
                                        "moment": "2000-01-01T00:00:00.000+00:00",
                                        "moment_tz": "+01:00",
                                        "key": "key",
                                        "value": "value"}
                                   ]
                               }
                            }) as response:
            app.logger.info("%s", response.get_json())
            self.assertEqual(400, response.status_code)


if __name__ == "__main__":
    unittest.main()
