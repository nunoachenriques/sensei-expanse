#  Copyright 2019 Nuno A. C. Henriques [nunoachenriques.net]
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

"""
Test ``valence`` module.
"""

import unittest

from cognition.valence import Valence


class TestValence(unittest.TestCase):

    def test_to_class(self):
        valence = [-2.0, -1.0, -0.5, -0.49, None, 0.0, None, 0.49, 0.5, 1.0, 2.0]
        valence_class_expected = [None, -1, -1, 0, None, 0, None, 0, 1, 1, None]
        valence_class_result = Valence.to_class(valence)
        self.assertEqual(valence_class_expected, valence_class_result, "Valence to class FAILED!")

    def test_to_class_and_colour(self):
        valence = [-2.0, -1.0, -0.5, -0.49, None, 0.0, None, 0.49, 0.5, 1.0, 2.0]
        valence_class_and_colour_expected = [
            None,
            (-1, Valence.COLOUR_HEX["Negative"]),
            (-1, Valence.COLOUR_HEX["Negative"]),
            (0, Valence.COLOUR_HEX["Neutral"]),
            None,
            (0, Valence.COLOUR_HEX["Neutral"]),
            None,
            (0, Valence.COLOUR_HEX["Neutral"]),
            (1, Valence.COLOUR_HEX["Positive"]),
            (1, Valence.COLOUR_HEX["Positive"]),
            None
        ]
        valence_class_and_colour_result = Valence.to_class_and_colour(valence)
        self.assertEqual(valence_class_and_colour_expected, valence_class_and_colour_result,
                         "Valence to class and colour FAILED!")


if __name__ == "__main__":
    unittest.main()
