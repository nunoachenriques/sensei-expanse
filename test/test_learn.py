#  Copyright 2018 Nuno A. C. Henriques [nunoachenriques.net]
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

"""
Test ``learn`` module.

TODO database mock up to avoid database dependency.
"""

import configparser
import datetime as dt
import gzip
import logging as log
import os
import unittest

import dill as pickle
import numpy as np
import pandas as pd
import tensorflow as tf
import xgboost
from sklearn import dummy
from sklearn import metrics
from sklearn import model_selection

from cognition.expanse import Expanse
from cognition.learn import Learn, KerasClassifier


class TestLearn(unittest.TestCase):
    _expanse = None
    _db_settings = None
    _data_wide = None
    _ENTITY = "x"
    _RANDOM_SEED = 42
    _VERBOSE = False
    _DEBUG = False

    @classmethod
    def setUpClass(cls):
        cls._learn = Learn(debug=cls._DEBUG, verbose=cls._VERBOSE, max_cpu=Learn.max_cpu_allowed(spare=True))
        config = configparser.ConfigParser()
        path = os.path.dirname(__file__)
        ini_file = os.path.join(path, "test_expanse.ini")
        if not config.read(ini_file):
            raise RuntimeError("CONFIGURATION file read FAILED!"
                               f" Hint: create {ini_file} file with proper values.")
        # NOTICE: create user and database, give permissions in pg_hba.conf.
        cls._expanse = Expanse()
        cls._expanse.connect(config["database"]["name"], config["database"]["user"])
        cls._db_settings = dict()
        cls._db_settings["name"] = config["database"]["name"]
        cls._db_settings["user"] = config["database"]["user"]
        # CLEAN values (delete entity cascades to all)
        with cls._expanse.connection as connection:
            with connection.cursor() as cursor:
                cursor.execute("delete from entity where id = %s", (cls._ENTITY,))
        # DATA SET
        with cls._expanse.connection as connection:
            with connection.cursor() as cursor:
                cursor.execute(
                    "insert into entity (id, birthday, gender) values (%s, %s, %s)",
                    (cls._ENTITY, dt.date(2000, 1, 1), "0")
                )
                with gzip.open(os.path.join(path, "test_learn_pipeline.copy.gz")) as file:
                    cursor.copy_from(file, "pipeline")
        status = cls._expanse.pipeline_status(cls._ENTITY)
        cls._data_wide = pd.DataFrame(data=pickle.loads(gzip.decompress(status.data_wide)))
        # LOGGING
        if cls._VERBOSE:
            if cls._DEBUG:
                log_level = log.DEBUG
            else:
                log_level = log.INFO
        else:
            log_level = log.CRITICAL
        log_settings = {
            "format": "%(asctime)s %(module)-20s: %(levelname)-8s %(message)s",
            "datefmt": "%Y-%m-%d %H:%M:%S",
            "level": log_level
        }
        log.basicConfig(**log_settings)
        # TensorFlow issue on logging | INFO=0, WARNING=1, ERROR=2, FATAL=3
        os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"
        tf.get_logger().setLevel(log_level)

    def test_max_cpu_allowed(self):
        self.assertEqual(Learn.max_cpu_allowed(spare=False), len(os.sched_getaffinity(0)))
        self.assertEqual(Learn.max_cpu_allowed(spare=True), len(os.sched_getaffinity(0)) - 1)

    def test_hdbscan_auto_params(self):
        coordinates = [[0, 0], [1, 1], [1, 1]]
        min_samples_ref = 1
        min_samples = [1, 2, 3]
        mcs, mcs_i, mcs_range, r = self._learn.hdbscan_auto_params(
            coordinates=coordinates,
            hdbscan_min_samples=min_samples
        )
        self.assertEqual(mcs, 2, f"Min cluster size FAILED! {min_samples}")
        self.assertEqual(mcs_range, range(2, 3), "Min cluster size range FAILED!")
        self.assertEqual(r, {'1': [1], '2': [1], '3': [1]}, "Result FAILED!")
        self.assertEqual(r[f"{min_samples_ref}"][mcs_i], [1], "Number of clusters FAILED!")

    def test_hdbscan_geo_cluster(self):
        coordinates = [[0, 0], [1, 1], [1, 1]]
        cluster_model, cluster_n = self._learn.hdbscan_geo_cluster(
            coordinates=coordinates,
            hdbscan_min_samples=1,
            hdbscan_min_cluster_size=2,
            hdbscan_prediction_data=True
        )
        self.assertEqual([0., 1., 1.], list(cluster_model.probabilities_), "Cluster model/probabilities FAILED!")
        self.assertEqual([-1, 0, 0], list(cluster_model.labels_), "Cluster mapping FAILED!")
        self.assertEqual(1, cluster_n, "Cluster N FAILED!")

    @staticmethod
    def _hdbscan_auto_params_callback(hap_mcs, hap_mcs_i, hap_mcs_range, hap_result):
        log.debug(f"{hap_mcs, hap_mcs_i, hap_mcs_range, hap_result}")

    def test_geo_clusters(self):
        coordinates = [[0, 0], [1, 1], [1, 2], [1, 3]]
        cluster_model, cluster_n = self._learn.geo_clusters(
            coordinates=coordinates,
            hdbscan_min_samples=[1, 2, 3, 4],
            hdbscan_min_samples_pref=1,
            hdbscan_auto_params_callback=self._hdbscan_auto_params_callback
        )
        self.assertEqual([-1, 0, 0, 0], list(cluster_model.labels_), "Cluster mapping FAILED!")
        self.assertEqual(1, cluster_n, "Cluster N FAILED!")

    def test_eligible_split(self):
        x = pd.DataFrame([[0, 1, 2, 3, 4], [5, 6, 7, 8, 9], [0, 1, 2, 3, 4], [5, 6, 7, 8, 9], [1, 3, 5, 7, 9],
                          [1, 3, 5, 7, 9], [5, 6, 7, 8, 9], [0, 1, 2, 3, 4], [5, 6, 7, 8, 9], [0, 1, 2, 3, 4]],
                         pd.date_range('2000', freq='D', periods=10))
        y = pd.DataFrame({"valence_class": [0, 1, 0, 1, 2,
                                            2, 1, 0, 1, 0]})
        x_train, x_test, y_train, y_test = model_selection.train_test_split(x, y, test_size=0.2, shuffle=False)
        splitter = Learn.eligible_split(
            x_train=x_train,
            y_train=y_train,
            splitter=model_selection.TimeSeriesSplit
        )
        self.assertEqual(2, splitter.get_n_splits(), "Splitter FAILED!")

    def test_fit_callback_early_stop(self):
        search_result = {"fun": 0.8, "func_vals": []}
        early_stop = Learn.fit_callback_early_stop(search_result)
        self.assertIsNone(early_stop)
        search_result = {"fun": 0.8, "func_vals": [0.6, 0.8, 0.8]}
        early_stop = Learn.fit_callback_early_stop(search_result)
        self.assertFalse(early_stop)
        search_result = {"fun": 0.8, "func_vals": [0.6, 0.8, 0.8, 0.9]}
        early_stop = Learn.fit_callback_early_stop(search_result)
        self.assertFalse(early_stop)
        search_result = {"fun": 0.8, "func_vals": [0.8, 0.8, 0.8]}
        early_stop = Learn.fit_callback_early_stop(search_result)
        self.assertTrue(early_stop)

    def test_model_estimator(self):
        x = self._data_wide.drop(columns=["valence", "valence_class"]).astype(dtype="int32", copy=True)
        y = self._data_wide["valence_class"]
        x_train, x_test, y_train, y_test =\
            model_selection.train_test_split(x, y, test_size=0.2,
                                             shuffle=True, stratify=y, random_state=self._RANDOM_SEED)
        algorithms = {
            "dummy": {
                "default": dummy.DummyClassifier(random_state=self._RANDOM_SEED),
                "search": {
                    "strategy": ["stratified"]
                },
                "search_n_iter": 1,
                "split_n_class_min": 1,
                "early_stop": False,
                # TEST PARAMS
                "cv_splitter": Learn.eligible_split(
                    x_train=x_train,
                    y_train=y_train,
                    splitter=model_selection.StratifiedKFold,
                    n_classes=1
                ),
                "expected_test_score": 0.66
            },
            "xgboost": {
                "default": xgboost.sklearn.XGBClassifier(
                    random_state=self._RANDOM_SEED,
                    predictor="cpu_predictor"
                ),
                # https://xgboost.readthedocs.io/en/latest/tutorials/param_tuning.html
                "search": {
                    "max_depth": (1, 5),
                    "learning_rate": (10**-5, 10**0, "log-uniform"),
                    "gamma": (1e-6, 1e+1, "log-uniform"),
                    "max_delta_step": (0, 1)
                },
                "search_n_iter": None,
                "split_n_class_min": 2,
                "early_stop": True,
                # TEST PARAMS
                "cv_splitter": Learn.eligible_split(
                    x_train=x_train,
                    y_train=y_train,
                    splitter=model_selection.StratifiedKFold,
                    n_classes=2
                )
            },
            "nn": {
                "default": KerasClassifier(
                    build_fn=Learn.neural_network_model,
                    input_shape=(x.shape[-1],)
                ),
                "search": {
                    "units": [100, 200, 300, 400, 500],
                    "dropout_rate": (0.1, 0.5),
                    "batch_size": (16, 128),
                    "epochs": [100],
                    "verbose": [0]
                },
                "search_n_iter": None,
                "split_n_class_min": 1,
                "early_stop": True,
                # TEST PARAMS
                "cv_splitter": Learn.eligible_split(
                    x_train=x_train,
                    y_train=y_train,
                    splitter=model_selection.StratifiedKFold,
                    n_classes=1
                )
            }
        }
        scoring = metrics.make_scorer(metrics.matthews_corrcoef)
        for algorithm in algorithms:
            best_estimator, fit_score, test_score, _, _ = self._learn.model_estimator(
                estimator={
                    "default": algorithms[algorithm]["default"],
                    "search": algorithms[algorithm]["search"],
                    "search_n_iter": algorithms[algorithm]["search_n_iter"]
                },
                scoring=scoring,
                x_train=x_train,
                x_test=x_test,
                y_train=y_train,
                y_test=y_test,
                cv_splitter=algorithms[algorithm]["cv_splitter"],
                early_stop=algorithms[algorithm]["early_stop"]
            )
            if algorithm == "dummy":
                self.assertEqual(algorithms["dummy"]["expected_test_score"], round(test_score, 2), "Score FAILED!")
            else:
                self.assertLess(algorithms["dummy"]["expected_test_score"], round(test_score, 2), "Score FAILED!")
            if self._VERBOSE:
                print(f"FIT score: {round(fit_score, 2)} | TEST score: {round(test_score, 2)}")
                print(best_estimator.get_params())

    def test_estimator_full_name(self):
        self.assertEqual("sklearn.dummy.DummyClassifier", Learn.estimator_full_name(dummy.DummyClassifier()))

    def test_estimator_to_and_from_serialization(self):
        # REGULAR sklearn
        estimator_to = dummy.DummyClassifier(random_state=42, strategy="uniform")
        name, estimator_bytes = Learn.estimator_to_serialization(estimator_to)
        self.assertEqual("sklearn.dummy.DummyClassifier", name)
        estimator_from = Learn.estimator_from_serialization(estimator_bytes)
        self.assertEqual(estimator_to.get_params(), estimator_from.get_params())
        # SPECIAL TensorFlow Keras scikit-learn wrapper
        estimator_to = KerasClassifier(
            build_fn=Learn.neural_network_model,
            **{"input_shape": (3,), "units": 100, "dropout_rate": 0.1, "verbose": 1 if self._VERBOSE else 0}
        )
        estimator_to.fit(np.array([[0, 1, 0]]), np.array([0]))  # to instantiate build_fn in model
        name, estimator_bytes = Learn.estimator_to_serialization(estimator_to)
        self.assertEqual("cognition.learn.KerasClassifier", name)
        estimator_from = Learn.estimator_from_serialization(estimator_bytes)
        estimator_to_params = estimator_to.get_params()
        del estimator_to_params["build_fn"]  # Diff memory address for instance
        estimator_from_params = estimator_from.get_params()
        del estimator_from_params["build_fn"]  # Diff memory address for instance
        self.assertEqual(estimator_to_params, estimator_from_params)

    def test_neural_network_model(self):
        expected_input_shape = (42,)
        expected_units = 100
        expected_dropout_rate = 0.1
        nn_model = Learn.neural_network_model(expected_input_shape, expected_units, expected_dropout_rate)
        first_layer = nn_model.get_layer(name="first")
        self.assertEqual(expected_input_shape, (first_layer.input_shape[1],))
        self.assertEqual(expected_units, first_layer.units)
        first_layer_dropout = nn_model.get_layer(name="first_dropout")
        self.assertEqual(expected_dropout_rate, first_layer_dropout.rate)
        self.assertEqual("MLPClassifier", nn_model.name)


if __name__ == "__main__":
    unittest.main()
