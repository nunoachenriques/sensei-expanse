#  Copyright 2018 Nuno A. C. Henriques [nunoachenriques.net]
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

"""
Test ``expanse`` module.
"""

import configparser
import datetime as dt
import os
import unittest

import psycopg2.extras as pge

from cognition.expanse import Expanse


class TestExpanse(unittest.TestCase):
    _config = None
    _expanse = None
    _ENTITY = "x"

    @classmethod
    def setUpClass(cls):
        cls._expanse = Expanse()
        cls._config = configparser.ConfigParser()
        path = os.path.dirname(__file__)
        name = os.path.splitext(os.path.basename(__file__))[0]
        ini_file = os.path.join(path, f"{name}.ini")
        if not cls._config.read(ini_file):
            raise RuntimeError("CONFIGURATION file read FAILED!"
                               f" Hint: create {ini_file} file with proper values.")
        # NOTICE: create user and database, give permissions in pg_hba.conf.
        cls._expanse.connect(cls._config["database"]["name"], cls._config["database"]["user"])

    @classmethod
    def tearDownClass(cls):
        # NOTICE: last data inserted kept for forensics
        cls._expanse.disconnect()

    def setUp(self) -> None:
        """
        CLEAN values (delete entity cascades to all).
        INSERT entity.
        """
        with self._expanse.connection as connection:
            with connection.cursor() as cursor:
                cursor.execute("delete from entity where id = %s", (self._ENTITY,))
                cursor.execute("insert into entity (id, birthday, gender) values (%s, %s, %s)",
                               (self._ENTITY, dt.date(2000, 1, 1), "1"))

    def test_connect(self):
        self.assertTrue(self._expanse.connection is not None)

    def test_disconnect(self):
        self._expanse.disconnect()
        self.assertTrue(self._expanse.connection is None)
        self._expanse.connect(self._config["database"]["name"], self._config["database"]["user"])

    # EXTRACT *****************************************************************

    def test_extract(self):
        tz = dt.timedelta(hours=1)
        # insert values
        with self._expanse.connection as connection:
            with connection.cursor() as cursor:
                pge.execute_values(
                    cursor,
                    "insert into collected"
                    " (entity, moment, moment_tz, sensor_type, sensor_attribute, sensor_value, sensor_value_extra)"
                    " values %s", [
                        (self._ENTITY, dt.datetime(2020, 2, 2, 1, 0, 0, 0), tz, "sensor.x.1", "a.1", 1.1, None),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 1, 0, 0, 0), tz, "sensor.x.1", "a.2", 2.2, None),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 2, 0, 0, 0), tz, "sensor.x.2", "b.1", 1.0, None),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 2, 0, 0, 0), tz, "sensor.x.2", "b.2", 1.0, None),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 3, 0, 0, 0), tz, "sensor.x.3", "c.1", 1.0, None),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 3, 0, 0, 0), tz, "sensor.x.3", "c.2", 1.0, None),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 3, 0, 0, 0), tz, "sensor.x.3", "c.3", 1.0, None)
                    ]
                )
        # validate
        t = self._expanse.extract(self._ENTITY,
                                  dt.datetime(2020, 2, 2, 2, 0, 0, 0) + tz,
                                  dt.datetime(2020, 2, 2, 2, 0, 0, 0) + tz)
        row_count_expected = 2
        self.assertEqual(row_count_expected, len(t), "Row count FAILED!")

    def test_extract_from_version(self):
        tz = dt.timedelta(hours=1)
        # insert values
        with self._expanse.connection as connection:
            with connection.cursor() as cursor:
                cursor.execute(
                    "insert into parameter (entity, moment, moment_tz, key, value) values (%s, %s, %s, %s, %s)",
                    (self._ENTITY, dt.datetime(2020, 2, 2, 2, 0, 0, 0), tz, "app_version", "0.2.0")
                )
                pge.execute_values(
                    cursor,
                    "insert into collected"
                    " (entity, moment, moment_tz, sensor_type, sensor_attribute, sensor_value, sensor_value_extra)"
                    " values %s", [
                        (self._ENTITY, dt.datetime(2020, 2, 2, 1, 0, 0, 0), tz, "sensor.x.1", "a.1", 1.1, None),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 1, 0, 0, 0), tz, "sensor.x.1", "a.2", 2.2, None),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 2, 0, 0, 0), tz, "sensor.x.2", "b.1", 1.0, None),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 2, 0, 0, 0), tz, "sensor.x.2", "b.2", 1.0, None),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 3, 0, 0, 0), tz, "sensor.x.3", "c.1", 1.0, None),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 3, 0, 0, 0), tz, "sensor.x.3", "c.2", 1.0, None),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 4, 0, 1, 0), tz, "sensor.x.4", "d.1", 1.0, None)
                    ]
                )
        # validate
        t = self._expanse.extract_from_version(self._ENTITY, (0, 2, 0), dt.timedelta(hours=2))
        # Version UTC time is 02:00. Entity local time is HH:MM+01:00. Extracted from local to delta.
        row_count_expected = 6  # from local time 01:00+01:00 = 02:00 to 01:00+01:00+02:00 = 04:00
        self.assertEqual(row_count_expected, len(t), "Row count FAILED!")

    # GET *********************************************************************

    def test_collected(self):
        tz = dt.timedelta(hours=1)
        # insert values
        with self._expanse.connection as connection:
            with connection.cursor() as cursor:
                pge.execute_values(
                    cursor,
                    "insert into collected"
                    " (entity, moment, moment_tz, sensor_type, sensor_attribute, sensor_value, sensor_value_extra)"
                    " values %s", [
                        (self._ENTITY, dt.datetime(2020, 2, 2, 1, 0, 0, 0), tz, "sensor.x.1", "a.1", 1.1, None),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 1, 0, 0, 0), tz, "sensor.x.1", "a.2", 2.2, None),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 2, 0, 0, 0), tz, "sensor.x.2", "b.1", 1.0, None),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 2, 0, 0, 0), tz, "sensor.x.2", "b.2", 1.0, None),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 3, 0, 0, 0), tz, "sensor.x.3", "c.1", 1.0, None),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 3, 0, 0, 0), tz, "sensor.x.3", "c.2", 1.0, None),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 3, 0, 0, 0), tz, "sensor.x.3", "c.3", 1.0, None)
                    ]
                )
        row_count_expected = 1
        sensor_expected = "sensor.x.2"
        sensor_attribute_expected = "b.2"
        t = self._expanse.collected(sensor_expected, sensor_attribute_expected)
        sensor, sensor_attr = t[0].sensor_type, t[0].sensor_attribute
        # validate
        self.assertEqual(row_count_expected, len(t), "Row count FAILED!")
        self.assertEqual(sensor_expected, sensor, "Sensor GET FAILED!")
        self.assertEqual(sensor_attribute_expected, sensor_attr, "Sensor attribute GET FAILED!")

    def test_entity_data_moments(self):
        # insert values
        now = dt.datetime(2019, 9, 1, 0, 0)
        delta = dt.timedelta(days=1)
        first_expected = now
        last_expected = now + delta
        with self._expanse.connection as connection:
            with connection.cursor() as cursor:
                pge.execute_values(
                    cursor,
                    "insert into collected (entity, moment, moment_tz, sensor_type, sensor_attribute, sensor_value)"
                    " values %s", [
                        (self._ENTITY, now, "00:00", "st", "sa", 0.0),
                        (self._ENTITY, now + delta, "00:00", "st", "sa", 1.0)
                    ]
                )
        # validate
        data_moments = self._expanse.entity_data_moments()
        tuples = [row for row in data_moments if row[0] == self._ENTITY]
        first, last = tuples[0].first, tuples[0].last
        row_count_expected = 1
        self.assertEqual(row_count_expected, len(tuples), "Row count FAILED!")
        self.assertEqual(first_expected, first, "First GET FAILED!")
        self.assertEqual(last_expected, last, "Last GET FAILED!")
        self.assertEqual(first + delta, last, "Last = First + delta: DIFF FAILED!")

    def test_entity_demographics(self):
        # insert values
        year = 2000
        age_expected = dt.datetime.now().year - year
        gender_expected = "1"
        # validate
        t = self._expanse.entity_demographics(self._ENTITY)
        age, gender = t[0].age, t[0].gender
        row_count_expected = 1
        self.assertEqual(row_count_expected, len(t), "Row count FAILED!")
        self.assertEqual(age_expected, age, "Age GET FAILED!")
        self.assertEqual(gender_expected, gender, "Gender GET FAILED!")

    def test_entity_demographics_valence_reports_data_moments(self):
        # insert values
        now = dt.datetime.utcnow()
        year = 2000
        age_expected = now.year - year
        gender_expected = "1"
        with self._expanse.connection as connection:
            with connection.cursor() as cursor:
                cursor.execute(
                    "insert into collected (entity, moment, moment_tz, sensor_type, sensor_attribute, sensor_value)"
                    " values (%s, %s, %s, %s, %s, %s)",
                    (self._ENTITY, now.date(), "00:00", "st", "sa", 0.0))
        # validate
        data_moments = self._expanse.entity_demographics_valence_reports_data_moments()
        tuples = [row for row in data_moments if row[0] == self._ENTITY]
        age, gender = tuples[0].age, tuples[0].gender
        row_count_expected = 1
        self.assertEqual(row_count_expected, len(tuples), "Row count FAILED!")
        self.assertEqual(age_expected, age, "Age GET FAILED!")
        self.assertEqual(gender_expected, gender, "Gender GET FAILED!")

    def test_gender_names(self):
        # insert values
        language = "lll"
        with self._expanse.connection as connection:
            with connection.cursor() as cursor:
                cursor.execute(f"delete from language where alpha3b =  %s", (language,))
                cursor.execute("insert into language (alpha3b, alpha3t, alpha2, name)"
                               " values (%s, %s, %s, %s)", (language, None, None, "LLL"))
                pge.execute_values(
                    cursor,
                    "insert into gender___translation (language, gender, name)"
                    " values %s", [
                        (language, "1", "1 in LLL"),
                        (language, "2", "2 in LLL")
                    ]
                )
        # validate
        gn = self._expanse.gender_names(language=language)
        gn_values = gn.values()
        names_expected = ["1 in LLL", "2 in LLL"]
        row_count_expected = len(names_expected)
        self.assertEqual(row_count_expected, len(gn), "Row count FAILED!")
        for v in gn_values:
            self.assertIn(v, names_expected, "Name NOT in the EXPECTED!")

    def test_moment_of_version(self):
        tz = dt.timedelta(hours=0)
        # insert values
        moment_expected = dt.datetime(2020, 2, 2, 2, 0, 0, 0)
        with self._expanse.connection as connection:
            with connection.cursor() as cursor:
                cursor.execute("insert into parameter (entity, moment, moment_tz, key, value)"
                               " values (%s, %s, %s, %s, %s)",
                               (self._ENTITY, moment_expected, tz, "app_version", "0.2.0"))
        # validate
        moment_got = self._expanse.moment_of_version(self._ENTITY, (0, 2, 0))
        self.assertEqual(moment_expected, moment_got, "Moment GET FAILED!")

    def test_model_status(self):
        # insert values
        expected = [
            (self._ENTITY, "sklearn.dummy.DummyClassifier",
             bytes(10), 0.90, dt.datetime(2020, 2, 2, 2, 0),
             dt.timedelta(seconds=22), dt.timedelta(seconds=2), {"split": [0.8, 0.2]}),
            (self._ENTITY, "sklearn.linear_model.logistic.LogisticRegression",
             bytes(1), 0.09, dt.datetime(2010, 1, 1, 1, 0),
             dt.timedelta(seconds=11), dt.timedelta(seconds=1), {"split": [0.8, 0.2]})
        ]
        with self._expanse.connection as connection:
            with connection.cursor() as cursor:
                pge.execute_values(
                    cursor,
                    "insert into model"
                    "  (entity, name, estimator, score, moment, fit_duration, score_duration, extra)"
                    " values %s", expected
                )
        # validate
        models = self._expanse.model_status(self._ENTITY)
        models.sort()
        for i, model in enumerate(models):
            self.assertEqual(expected[i][1], model.name, "name FAILED!")
            self.assertEqual(expected[i][2], bytes(model.estimator), "model FAILED!")
            self.assertEqual(expected[i][3], float(model.score), "score FAILED!")
            self.assertEqual(expected[i][4], model.moment, "moment FAILED!")

    def test_model_status_update(self):
        # insert values
        expected = [
            (self._ENTITY, "sklearn.dummy.DummyClassifier",
             bytes(10), 0.90, dt.datetime(2020, 2, 2, 2, 0),
             dt.timedelta(seconds=22), dt.timedelta(seconds=2), {"split": [0.8, 0.2]}),
            (self._ENTITY, "sklearn.ensemble.RandomForrestClassifier",
             bytes(1), 0.09, dt.datetime(2010, 1, 1, 1, 0),
             dt.timedelta(seconds=11), dt.timedelta(seconds=1), {"split": [0.8, 0.2]})
        ]
        for e in expected:
            self._expanse.model_status_update(e[0], e[1], e[2], e[3], e[4], e[5], e[6], e[7])
        # validate
        models = self._expanse.model_status(self._ENTITY)
        models.sort()
        for i, model in enumerate(models):
            self.assertEqual(expected[i][1], model.name, "name FAILED!")
            self.assertEqual(expected[i][2], bytes(model.estimator), "model FAILED!")
            self.assertEqual(expected[i][3], float(model.score), "score FAILED!")
            self.assertEqual(expected[i][4], model.moment, "moment FAILED!")
            self.assertEqual(expected[i][5], model.fit_duration, "fit_duration FAILED!")
            self.assertEqual(expected[i][6], model.score_duration, "score_duration FAILED!")
            self.assertEqual(expected[i][7], model.extra, "extra FAILED!")

    def test_pipeline_status(self):
        # insert values
        data_expected = bytes(10)
        data_shape_expected = [1, 1]
        data_moment_expected = dt.datetime(2020, 2, 2, 2, 0, 0, 0)
        data_extra_expected = {"imbalance": -0.0, "class_count": [0, 1, 42]}
        status_expected = "ready"
        status_moment_expected = data_moment_expected + dt.timedelta(milliseconds=1)
        duration_expected = dt.timedelta(minutes=2)
        with self._expanse.connection as connection:
            with connection.cursor() as cursor:
                pge.execute_values(
                    cursor,
                    "insert into pipeline"
                    " (entity,"
                    "  data_wide, data_wide_shape, data_wide_moment, data_wide_extra,"
                    "  status, status_moment, duration)"
                    " values %s", [
                        (self._ENTITY,
                         data_expected, data_shape_expected, data_moment_expected, data_extra_expected,
                         status_expected, status_moment_expected, duration_expected)
                    ]
                )
        # validate
        pipeline = self._expanse.pipeline_status(self._ENTITY)
        self.assertEqual(data_expected, bytes(pipeline.data_wide), "Data GET FAILED!")
        self.assertEqual(data_shape_expected, pipeline.data_wide_shape, "Data shape GET FAILED!")
        self.assertEqual(data_moment_expected, pipeline.data_wide_moment, "Data moment GET FAILED!")
        self.assertEqual(data_extra_expected, pipeline.data_wide_extra, "Data extra GET FAILED!")
        self.assertEqual(status_expected, pipeline.status, "Status GET FAILED!")
        self.assertEqual(status_moment_expected, pipeline.status_moment, "Status moment GET FAILED!")
        self.assertEqual(duration_expected, pipeline.duration, "Duration GET FAILED!")

    def test_pipeline_status_update(self):
        # insert values
        data_expected = bytes(0)
        data_shape_expected = [0, 0]
        data_moment_expected = None
        data_extra_expected = {"imbalance": -0.0, "class_count": [0, 1, 42]}
        status_expected = "ready"
        status_moment_expected = dt.datetime(2020, 2, 2, 2, 0, 0, 0)
        duration_expected = dt.timedelta(minutes=2)
        self._expanse.pipeline_status_update(
            self._ENTITY,
            data_expected, list((123, 3)), dt.datetime.min, None,
            "not expected", status_moment_expected, duration_expected
        )
        self._expanse.pipeline_status_update(
            self._ENTITY,
            data_expected, data_shape_expected, data_moment_expected, data_extra_expected,
            status_expected, status_moment_expected, duration_expected
        )
        # validate
        pipeline = self._expanse.pipeline_status(self._ENTITY)
        self.assertEqual(data_expected, bytes(pipeline.data_wide), "Data GET FAILED!")
        self.assertEqual(data_shape_expected, pipeline.data_wide_shape, "Data shape GET FAILED!")
        self.assertEqual(data_moment_expected, pipeline.data_wide_moment, "Data moment GET FAILED!")
        self.assertEqual(data_extra_expected, pipeline.data_wide_extra, "Data extra GET FAILED!")
        self.assertEqual(status_expected, pipeline.status, "Status GET FAILED!")
        self.assertEqual(status_moment_expected, pipeline.status_moment, "Status moment GET FAILED!")
        self.assertEqual(duration_expected, pipeline.duration, "Duration GET FAILED!")

    def test_pipeline_status_step_update(self):
        # insert values
        data_expected = bytes(10)
        data_shape_expected = [1, 1]
        data_moment_expected = dt.datetime(2020, 2, 2, 2, 0, 0, 0)
        data_extra_expected = {"imbalance": -0.0, "class_count": [0, 1, 42]}
        status_expected = "ready"
        status_moment_expected = data_moment_expected + dt.timedelta(milliseconds=1)
        duration_expected = dt.timedelta(minutes=2)
        self._expanse.pipeline_status_update(
            self._ENTITY,
            data_expected, data_shape_expected, data_moment_expected, None,
            "learning", status_moment_expected - dt.timedelta(days=1), None
        )
        self._expanse.pipeline_status_step_update(
            self._ENTITY,
            data_extra_expected,
            status_expected, status_moment_expected, duration_expected
        )
        # validate
        pipeline = self._expanse.pipeline_status(self._ENTITY)
        self.assertEqual(data_expected, bytes(pipeline.data_wide), "Data GET FAILED!")
        self.assertEqual(data_shape_expected, pipeline.data_wide_shape, "Data shape GET FAILED!")
        self.assertEqual(data_moment_expected, pipeline.data_wide_moment, "Data moment GET FAILED!")
        self.assertEqual(data_extra_expected, pipeline.data_wide_extra, "Data extra GET FAILED!")
        self.assertEqual(status_expected, pipeline.status, "Status GET FAILED!")
        self.assertEqual(status_moment_expected, pipeline.status_moment, "Status moment GET FAILED!")
        self.assertEqual(duration_expected, pipeline.duration, "Duration GET FAILED!")

    def test_valence_colours(self):
        # extract
        vc = self._expanse.valence_colours()
        vc_keys = vc.keys()
        # validate
        names_expected = ["negative", "neutral", "positive"]
        row_count_expected = len(names_expected)
        self.assertEqual(row_count_expected, len(vc), "Row count FAILED!")
        for k in vc_keys:
            self.assertIn(k, names_expected, "Name NOT in the EXPECTED!")


if __name__ == "__main__":
    unittest.main()
