#  Copyright 2018 Nuno A. C. Henriques [nunoachenriques.net]
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

"""
Test ``pipeline`` module.

TODO database mock up to avoid database dependency.
"""

import configparser
import datetime as dt
import gzip
import os
import unittest

import pandas as pd
from sklearn import dummy

from cognition.expanse import Expanse
from cognition.pipeline import Pipeline
from cognition.valence import Valence


class TestPipeline(unittest.TestCase):
    _expanse = None
    _db_settings = None
    _ENTITY = "x"

    @classmethod
    def setUpClass(cls):
        config = configparser.ConfigParser()
        path = os.path.dirname(__file__)
        ini_file = os.path.join(path, "test_expanse.ini")
        if not config.read(ini_file):
            raise RuntimeError("CONFIGURATION file read FAILED!"
                               f" Hint: create {ini_file} file with proper values.")
        # NOTICE: create user and database, give permissions in pg_hba.conf.
        cls._expanse = Expanse()
        cls._expanse.connect(config["database"]["name"], config["database"]["user"])
        cls._db_settings = dict()
        cls._db_settings["name"] = config["database"]["name"]
        cls._db_settings["user"] = config["database"]["user"]
        # CLEAN values (delete entity cascades to all)
        with cls._expanse.connection as connection:
            with connection.cursor() as cursor:
                cursor.execute("delete from entity where id = %s", (cls._ENTITY,))
        # DATA SET
        with cls._expanse.connection as connection:
            with connection.cursor() as cursor:
                cursor.execute(
                    "insert into entity (id, birthday, gender)"
                    " values (%s, %s, %s)",
                    (cls._ENTITY, dt.date(2000, 1, 1), "0")
                )
                cursor.execute(
                    "insert into parameter (entity, moment, moment_tz, key, value)"
                    " values (%s, %s, %s, %s, %s)",
                    (cls._ENTITY, dt.datetime(2018, 10, 31, 0, 0), dt.timedelta(hours=0), "app_version", "0.2.0")
                )
                with gzip.open(os.path.join(path, "test_pipeline_collected.copy.gz")) as file:
                    cursor.copy_from(file, "collected")

    @classmethod
    def tearDownClass(cls):
        # NOTICE: last data inserted kept for forensics
        cls._expanse.disconnect()

    def test_entity_short_id(self):
        identifier = "abcdef123456"
        expected_short_id = "abcdef1234"
        self.assertEqual(expected_short_id, Pipeline.entity_short_id(identifier, None))
        default_trailing = "..."
        expected_short_id = f"abcdef1234{default_trailing}"
        self.assertEqual(expected_short_id, Pipeline.entity_short_id(identifier))
        trailing = "---"
        expected_short_id = f"abcdef1234{trailing}"
        self.assertEqual(expected_short_id, Pipeline.entity_short_id(identifier, trailing))

    def test_likelihood_ratio_imbalance_degree(self):
        expected_lrid = -0.0
        series = pd.Series([0, 0, 0])
        self.assertEqual(expected_lrid, Pipeline.likelihood_ratio_imbalance_degree(series))
        series = pd.Series([-1, -1, -1, 0, 0, 0])
        self.assertEqual(expected_lrid, round(Pipeline.likelihood_ratio_imbalance_degree(series), 8))
        series = pd.Series([-1, -1, -1, 0, 0, 0, 1, 1, 1])
        self.assertEqual(expected_lrid, round(Pipeline.likelihood_ratio_imbalance_degree(series), 8))
        expected_lrid = -0.26162407
        series = pd.Series([0, 0, 0, 1])
        self.assertEqual(expected_lrid, round(Pipeline.likelihood_ratio_imbalance_degree(series), 8))
        expected_lrid = -1.0
        series = pd.Series([-1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1])
        self.assertEqual(expected_lrid, round(Pipeline.likelihood_ratio_imbalance_degree(series), 2))

    def test_do_data_class_imbalance_check(self):
        # FALSE only one class check
        x_train = pd.DataFrame({"a": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]})
        x_test = pd.DataFrame({"a": [12, 13, 14]})
        y_train = pd.Series([-1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1])
        y_test = pd.Series([0, 0, -1])
        check, info, x_train_check, x_test_check, y_train_check, y_test_check = \
            Pipeline.do_data_class_imbalance_check(x_train, x_test, y_train, y_test, classes=Valence.CLASS_VALUES)
        self.assertFalse(check)
        self.assertEqual([0, 9, 0], info["class_count_check"])
        x_train_check_expected =\
            pd.DataFrame({"a": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]}).drop(index=[0, 10])
        self.assertEqual(list(x_train_check_expected["a"].to_numpy()), list(x_train_check["a"].to_numpy()))
        # TRUE two classes check
        x_train = pd.DataFrame({"a": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]})
        x_test = pd.DataFrame({"a": [12, 13, 14]})
        y_train = pd.Series([-1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1])
        y_test = pd.Series([0, 0, -1])
        check, info, x_train_check, x_test_check, y_train_check, y_test_check = \
            Pipeline.do_data_class_imbalance_check(x_train, x_test, y_train, y_test, classes=Valence.CLASS_VALUES)
        self.assertTrue(check)
        self.assertEqual([0, 8, 2], info["class_count_check"])
        x_train_check_expected =\
            pd.DataFrame({"a": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]}).drop(index=[0])
        self.assertEqual(list(x_train_check_expected["a"].to_numpy()), list(x_train_check["a"].to_numpy()))

    def test_metrics_extra(self):
        # TRUE two classes check
        x_train = pd.DataFrame({"a": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]})
        x_test = pd.DataFrame({"a": [12, 13, 14]})
        y_train = pd.Series([-1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1])
        y_test = pd.Series([0, 0, -1])
        estimator = dummy.DummyClassifier(random_state=Pipeline.RANDOM_SEED)
        estimator.fit(x_train, y_train)
        extra = Pipeline.metrics_extra(estimator, x_test, y_test, classes=Valence.CLASS_VALUES)
        confusion_matrix_expected = [[0, 1, 0], [0, 1, 1], [0, 0, 0]]
        self.assertEqual(confusion_matrix_expected, extra["confusion_matrix"])

    def test_do_all(self):
        data_wide_shape_expected = (427, 48)
        # DO ALL PIPELINE
        pipeline = Pipeline(db_name=self._db_settings["name"], db_user=self._db_settings["user"])
        pipeline.do_all(force_data=True, force_learn=True)
        # TRANSFORM data wide shape
        self.assertEqual(data_wide_shape_expected, pipeline.transformed["data_wide"].shape, "Data wide FAILED!")
        # MODEL
        with self._expanse.connection as connection:
            with connection.cursor() as cursor:
                cursor.execute("select * from model where entity = %s", (self._ENTITY,))
                models = cursor.fetchall()
        # MODEL score
        dummy_score_max_expected = 0.80
        for m in models:
            if pipeline.debug:
                print(f"{round(m.score, 2)} {m.name}")
            if m.name == "sklearn.dummy.DummyClassifier":
                self.assertGreater(dummy_score_max_expected, round(float(m.score), 2), f"Score ({m.name}) FAILED!")
            else:  # all scores should be greater than baseline
                self.assertLess(dummy_score_max_expected, round(float(m.score), 2), f"Score ({m.name}) FAILED!")
        # PIPELINE
        with self._expanse.connection as connection:
            with connection.cursor() as cursor:
                cursor.execute("select * from pipeline where entity = %s limit 1", (self._ENTITY,))
                pipeline_status = cursor.fetchone()
        # PIPELINE data wide shape
        self.assertEqual(list(data_wide_shape_expected), pipeline_status.data_wide_shape,
                         f"Shape ({pipeline_status.data_wide_shape}) FAILED!")


if __name__ == "__main__":
    unittest.main()
