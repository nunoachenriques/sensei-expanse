#  Copyright 2019 Nuno A. C. Henriques [nunoachenriques.net]
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

"""
Test ``analysis`` module.
"""

import datetime as dt
import unittest

import matplotlib.pyplot as plt
import pandas as pd

from cognition.analysis import Analysis


class TestAnalysis(unittest.TestCase):
    _VERBOSE = False
    _DEMOGRAPHICS = pd.DataFrame(
        data=[("e1", 18, "2"),
              ("e2", 20, "1"),
              ("e3", 25, "1"),
              ("e4", 25, "1"),
              ("e5", 44, "2"),
              ("e6", 47, "2")],
        columns=pd.Index(data=["entity", "age", "gender"])
    )
    _MOMENTS = pd.DataFrame(
        data=[("e1", dt.timedelta(weeks=4)),
              ("e2", dt.timedelta(weeks=1)),
              ("e3", dt.timedelta(weeks=1)),
              ("e4", dt.timedelta(weeks=1)),
              ("e5", dt.timedelta(weeks=1)),
              ("e6", dt.timedelta(weeks=1))],
        columns=pd.Index(data=["entity", "duration"])
    )
    delta = dt.timedelta(minutes=1)
    _COLLECTED = pd.DataFrame(
        data=[("e1", dt.datetime.utcnow() + delta * 0, "app.human.smile", "polarity", 1.0, "POSITIVE"),
              ("e1", dt.datetime.utcnow() + delta * 1, "app.human.smile", "polarity", 1.0, "POSITIVE"),
              ("e1", dt.datetime.utcnow() + delta * 2, "app.human.smile", "polarity", 1.0, "POSITIVE"),
              ("e1", dt.datetime.utcnow() + delta * 3, "app.human.smile", "polarity", 1.0, "POSITIVE"),
              ("e1", dt.datetime.now() + delta * 4, "app.human.smile", "polarity", 0.0, "NEUTRAL"),
              ("e1", dt.datetime.now() + delta * 5, "app.human.smile", "polarity", 0.0, "NEUTRAL"),
              ("e1", dt.datetime.utcnow() + delta * 6, "app.human.smile", "polarity", -1.0, "NEGATIVE"),
              ("e1", dt.datetime.utcnow() + delta * 7, "app.human.smile", "polarity", -1.0, "NEGATIVE"),
              ("e1", dt.datetime.utcnow() + delta * 8, "app.human.smile", "polarity", -1.0, "NEGATIVE"),
              ("e2", dt.datetime.utcnow() + delta * 0, "app.human.smile", "polarity", 1.0, "POSITIVE"),
              ("e2", dt.datetime.utcnow() + delta * 1, "app.human.smile", "polarity", 1.0, "POSITIVE"),
              ("e2", dt.datetime.now() + delta * 2, "app.human.smile", "polarity", 0.0, "NEUTRAL"),
              ("e2", dt.datetime.utcnow() + delta * 3, "app.human.smile", "polarity", -1.0, "NEGATIVE"),
              ("e2", dt.datetime.utcnow() + delta * 4, "app.human.smile", "polarity", -1.0, "NEGATIVE"),
              ("e3", dt.datetime.utcnow() + delta * 0, "app.human.smile", "polarity", 1.0, "POSITIVE"),
              ("e3", dt.datetime.utcnow() + delta * 1, "app.human.smile", "polarity", 1.0, "POSITIVE"),
              ("e3", dt.datetime.now() + delta * 2, "app.human.smile", "polarity", 0.0, "NEUTRAL"),
              ("e3", dt.datetime.now() + delta * 3, "app.human.smile", "polarity", 0.0, "NEUTRAL"),
              ("e3", dt.datetime.utcnow() + delta * 4, "app.human.smile", "polarity", -1.0, "NEGATIVE"),
              ("e3", dt.datetime.utcnow() + delta * 5, "app.human.smile", "polarity", -1.0, "NEGATIVE"),
              ("e3", dt.datetime.utcnow() + delta * 6, "app.human.smile", "polarity", -1.0, "NEGATIVE"),
              ("e4", dt.datetime.utcnow() + delta * 0, "app.human.smile", "polarity", 1.0, "POSITIVE"),
              ("e4", dt.datetime.utcnow() + delta * 1, "app.human.smile", "polarity", 1.0, "POSITIVE"),
              ("e4", dt.datetime.now() + delta * 2, "app.human.smile", "polarity", 0.0, "NEUTRAL"),
              ("e4", dt.datetime.now() + delta * 3, "app.human.smile", "polarity", 0.0, "NEUTRAL"),
              ("e4", dt.datetime.utcnow() + delta * 4, "app.human.smile", "polarity", -1.0, "NEGATIVE"),
              ("e4", dt.datetime.utcnow() + delta * 5, "app.human.smile", "polarity", -1.0, "NEGATIVE"),
              ("e4", dt.datetime.utcnow() + delta * 6, "app.human.smile", "polarity", -1.0, "NEGATIVE"),
              ("e5", dt.datetime.utcnow() + delta * 0, "app.human.smile", "polarity", 1.0, "POSITIVE"),
              ("e5", dt.datetime.utcnow() + delta * 1, "app.human.smile", "polarity", 1.0, "POSITIVE"),
              ("e5", dt.datetime.utcnow() + delta * 2, "app.human.smile", "polarity", 1.0, "POSITIVE"),
              ("e5", dt.datetime.utcnow() + delta * 3, "app.human.smile", "polarity", 1.0, "POSITIVE"),
              ("e5", dt.datetime.now() + delta * 4, "app.human.smile", "polarity", 0.0, "NEUTRAL"),
              ("e5", dt.datetime.now() + delta * 5, "app.human.smile", "polarity", 0.0, "NEUTRAL"),
              ("e5", dt.datetime.utcnow() + delta * 6, "app.human.smile", "polarity", -1.0, "NEGATIVE"),
              ("e5", dt.datetime.utcnow() + delta * 7, "app.human.smile", "polarity", -1.0, "NEGATIVE"),
              ("e5", dt.datetime.utcnow() + delta * 8, "app.human.smile", "polarity", -1.0, "NEGATIVE"),
              ("e6", dt.datetime.utcnow() + delta * 0, "app.human.smile", "polarity", 1.0, "POSITIVE"),
              ("e6", dt.datetime.utcnow() + delta * 1, "app.human.smile", "polarity", 1.0, "POSITIVE"),
              ("e6", dt.datetime.utcnow() + delta * 2, "app.human.smile", "polarity", 1.0, "POSITIVE"),
              ("e6", dt.datetime.utcnow() + delta * 3, "app.human.smile", "polarity", 1.0, "POSITIVE"),
              ("e6", dt.datetime.now() + delta * 4, "app.human.smile", "polarity", 0.0, "NEUTRAL"),
              ("e6", dt.datetime.now() + delta * 5, "app.human.smile", "polarity", 0.0, "NEUTRAL"),
              ("e6", dt.datetime.utcnow() + delta * 6, "app.human.smile", "polarity", -1.0, "NEGATIVE"),
              ("e6", dt.datetime.utcnow() + delta * 7, "app.human.smile", "polarity", -1.0, "NEGATIVE"),
              ("e6", dt.datetime.utcnow() + delta * 8, "app.human.smile", "polarity", -1.0, "NEGATIVE")],
        columns=pd.Index(data=["entity", "moment",
                               "sensor_type", "sensor_attribute",
                               "sensor_value", "sensor_value_extra"])
    )
    _GENDER_NAME = dict([("1", "Female"),
                         ("12", "Intersex"),
                         ("2", "Male"),
                         ("0", "Neutral")])
    _VALENCE_COLOUR = dict([("negative", (0.050980392, 0.278431373, 0.631372549)),
                            ("neutral", (0.180392157, 0.490196078, 0.196078431)),
                            ("positive", (0.960784314, 0.498039216, 0.090196078))])

    @classmethod
    def setUpClass(cls):
        cls._analysis = Analysis(
            demographics=cls._DEMOGRAPHICS,
            moments=cls._MOMENTS,
            collected=cls._COLLECTED,
            gender_name=cls._GENDER_NAME,
            valence_colour=cls._VALENCE_COLOUR
        )

    def test_age_range_bins(self):
        age_range, age_bins = self._analysis.age_range_bins(
            age_data=self._DEMOGRAPHICS["age"],
            age_range_step=5
        )
        age_range_expected = [15, 20, 25, 30, 35, 40, 45, 50]
        self.assertSequenceEqual(age_range_expected, list(age_range), "Range FAILED!")
        age_bins_expected = 7
        self.assertEqual(age_bins_expected, pd.Categorical(age_bins).categories.shape[0], "Bins FAILED!")

    def test_plt_valence_by_age_gender(self):
        # REGULAR
        figure = plt.figure(num="regular", figsize=(10, 5), tight_layout=True)
        data = self._analysis.plt_valence_by_age_gender(
            df=self._analysis.data_report_count,
            figure=figure
        )
        if self._VERBOSE:
            print(f"Female\n{data['Female']}")
            print(f"Male\n{data['Male']}")
        bars_sum_expected = 3  # Sum of 3 bars (height=1.0 each)
        bars_sum = 0
        ax = plt.gca()
        for i in range(24):  # 3 (valence) x 2 (gender) x 4 (age range)
            bars_sum += ax.get_children()[i].get_height()
        self.assertEqual(bars_sum_expected, bars_sum, "Bars regular FAILED!")
        if self._VERBOSE:
            plt.show()
        plt.close("regular")
        # DICHOTOMIC
        figure = plt.figure(num="dichotomic", figsize=(10, 5), tight_layout=True)
        data = self._analysis.plt_valence_by_age_gender(
            df=(self._analysis.data_report_count
                .drop(columns="age_range_bin")
                .rename(columns={"age_range_dic_bin": "age_range_bin"})),
            figure=figure,
            dichotomic=True
        )
        if self._VERBOSE:
            print(f"Female\n{data['Female']}")
            print(f"Male\n{data['Male']}")
        bars_sum_expected = 4  # Sum of 4 bars (height=1.0 each)
        bars_sum = 0
        ax = plt.gca()
        for i in range(12):  # 3 (valence) x 2 (gender) x 2 (age range)
            bars_sum += ax.get_children()[i].get_height()
        self.assertEqual(bars_sum_expected, bars_sum, "Bars dichotomic FAILED!")
        if self._VERBOSE:
            plt.show()
        plt.close("dichotomic")

    def test_tidy(self):
        categories_expected = 2
        self._analysis.tidy(dichotomic=True)
        categories = len(self._analysis.eligible["age_range_bin"].dtypes.categories)
        self.assertEqual(categories_expected, categories, "eligible categories FAILED!")
        categories = len(self._analysis.data_eligible["age_range_bin"].dtypes.categories)
        self.assertEqual(categories_expected, categories, "data_eligible categories FAILED!")
        categories = len(self._analysis.data_report_count["age_range_bin"].dtypes.categories)
        self.assertEqual(categories_expected, categories, "data_report_count categories FAILED!")
        # 2nd consecutive call
        self._analysis.tidy(dichotomic=True)
        categories = len(self._analysis.eligible["age_range_bin"].dtypes.categories)
        self.assertEqual(categories_expected, categories, "eligible categories FAILED!")
        categories = len(self._analysis.data_eligible["age_range_bin"].dtypes.categories)
        self.assertEqual(categories_expected, categories, "data_eligible categories FAILED!")
        categories = len(self._analysis.data_report_count["age_range_bin"].dtypes.categories)
        self.assertEqual(categories_expected, categories, "data_report_count categories FAILED!")


if __name__ == "__main__":
    unittest.main()
