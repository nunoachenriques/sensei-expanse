#  Copyright 2018 Nuno A. C. Henriques [nunoachenriques.net]
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

"""
Test ``transform`` module.

TODO database mock up to avoid database dependency.
"""

import configparser
import datetime as dt
import os
import unittest

import pandas as pd
import psycopg2.extras as pge

from cognition.expanse import Expanse
from cognition.transform import Reconstruct, Ready


class TestTransform(unittest.TestCase):
    _expanse = None
    _ENTITY = "x"

    @classmethod
    def setUpClass(cls):
        config = configparser.ConfigParser()
        path = os.path.dirname(__file__)
        ini_file = os.path.join(path, "test_expanse.ini")
        if not config.read(ini_file):
            raise RuntimeError("CONFIGURATION file read FAILED!"
                               f" Hint: create {ini_file} file with proper values.")
        # NOTICE: create user and database, give permissions in pg_hba.conf.
        cls._expanse = Expanse()
        cls._expanse.connect(config["database"]["name"], config["database"]["user"])

    @classmethod
    def tearDownClass(cls):
        # NOTICE: last data inserted kept for forensics
        cls._expanse.disconnect()
 
    def setUp(self) -> None:
        """
        CLEAN values (delete entity cascades to all).
        INSERT entity.
        """
        with self._expanse.connection as connection:
            with connection.cursor() as cursor:
                cursor.execute("delete from entity where id = %s", (self._ENTITY,))
                cursor.execute("insert into entity (id, birthday, gender) values (%s, %s, %s)",
                               (self._ENTITY, dt.date(2000, 1, 1), "1"))

    def test_long_to_wide(self):
        # SMILE ***************************************************************
        sensor = "app.human.smile"
        tz = dt.timedelta(hours=1)
        # INSERT values
        with self._expanse.connection as connection:
            with connection.cursor() as cursor:
                pge.execute_values(
                    cursor,
                    "insert into collected"
                    " (entity, moment, moment_tz, sensor_type, sensor_attribute, sensor_value, sensor_value_extra)"
                    " values %s", [
                        (self._ENTITY, dt.datetime(2020, 2, 2, 1, 0, 0, 0), tz, sensor, "polarity", 1.0, None),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 1, 1, 0, 0), tz, sensor, "polarity", 0.7184, "EXTRA"),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 1, 2, 0, 0), tz, sensor, "polarity", 0.0, None),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 1, 3, 0, 0), tz, sensor, "polarity", 1.0, None),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 1, 4, 0, 0), tz, sensor, "polarity", 0.75, None),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 1, 5, 0, 0), tz, sensor, "polarity", -0.5, None),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 1, 6, 0, 0), tz, sensor, "polarity", -1.0, None)
                    ])
        # VALIDATE
        t = self._expanse.extract(self._ENTITY,
                                  dt.datetime(2020, 2, 2, 1, 0, 0, 0) + tz,
                                  dt.datetime(2020, 2, 2, 2, 0, 0, 0) + tz)
        transform = Reconstruct(tuples=t)
        pivot = transform.df.pivot(index="moment",
                                   columns="sensor_attribute",
                                   values="sensor_value")
        long_to_wide = transform.long_to_wide(data=transform.df)
        self.assertEqual(pivot.shape, long_to_wide.shape, f"{sensor} long_to_wide FAILED!")
        # VALIDATE EXTRA
        pivot = transform.df.pivot(index="moment",
                                   columns="sensor_attribute",
                                   values=["sensor_value", "sensor_value_extra"])
        long_to_wide = transform.long_to_wide(data=transform.df, extra_attr=["polarity"])
        self.assertEqual(pivot.shape, long_to_wide.shape, f"{sensor} long_to_wide FAILED!")

    def test_ready_geopositioning(self):
        tz = dt.timedelta(hours=0)
        sensor_geo = "app.sensor.geopositioning"
        # INSERT values
        with self._expanse.connection as connection:
            with connection.cursor() as cursor:
                pge.execute_values(
                    cursor,
                    "insert into collected"
                    " (entity, moment, moment_tz, sensor_type, sensor_attribute, sensor_value, sensor_value_extra)"
                    " values %s", [
                        (self._ENTITY, dt.datetime(2020, 2, 2, 1, 0), tz, sensor_geo, "latitude", 38.0, None),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 1, 0), tz, sensor_geo, "longitude", 9.0, None),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 1, 5), tz, sensor_geo, "latitude", 38.02, None),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 1, 5), tz, sensor_geo, "longitude", 9.02, None),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 1, 20), tz, sensor_geo, "latitude", 38.02, None),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 1, 20), tz, sensor_geo, "longitude", 9.02, None),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 1, 35), tz, sensor_geo, "latitude", 38.02, None),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 1, 35), tz, sensor_geo, "longitude", 9.02, None),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 2, 0), tz, sensor_geo, "latitude", 38.0, None),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 2, 0), tz, sensor_geo, "longitude", 9.0, None)
                    ])
        # VALIDATE
        t = self._expanse.extract(self._ENTITY,
                                  dt.datetime(2020, 2, 2, 1, 0, 0, 0) + tz,
                                  dt.datetime(2020, 2, 2, 3, 0, 0, 0) + tz)
        ready = Ready(tuples=t)
        geo_latlon, geo_latlon_rad = ready.geopositioning()
        self.assertEqual(5 + 15 + 15 + (15 + 1) + 1, len(geo_latlon_rad), "Geopositioning resample FAILED!")
        self.assertEqual(geo_latlon.shape[0], len(geo_latlon_rad), "Geopositioning ready FAILED!")

    def test_ready_valence(self):
        tz = dt.timedelta(hours=0)
        sensor_chat = "app.human.chat"
        sensor_smile = "app.human.smile"
        chat_polarity_nan_value_index_expected = "20200202020000"
        # INSERT values
        with self._expanse.connection as connection:
            with connection.cursor() as cursor:
                pge.execute_values(
                    cursor,
                    "insert into collected"
                    " (entity, moment, moment_tz, sensor_type, sensor_attribute, sensor_value, sensor_value_extra)"
                    " values %s", [
                        # CHAT
                        (self._ENTITY, dt.datetime(2020, 2, 2, 1, 0, 0), tz, sensor_chat, "transition", 1.0, "ENTER"),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 1, 1, 0), tz, sensor_chat, "polarity", 0.7184, "pt"),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 1, 2, 0), tz, sensor_chat, "transition", 0.0, "EXIT"),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 1, 3, 0), tz, sensor_chat, "transition", 1.0, "ENTER"),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 1, 4, 0), tz, sensor_chat, "polarity", 0.75, "en"),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 1, 5, 0), tz, sensor_chat, "transition", 0.0, "EXIT"),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 1, 6, 0), tz, sensor_chat, "transition", 1.0, "ENTER"),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 1, 7, 0), tz, sensor_chat, "transition", 0.0, "EXIT"),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 1, 8, 0), tz, sensor_chat, "transition", 1.0, "ENTER"),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 1, 9, 0), tz, sensor_chat, "polarity", 0.6, "en"),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 1, 10, 0), tz, sensor_chat, "polarity", 0.51, None),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 1, 11, 0), tz, sensor_chat, "transition", 0.0, "EXIT"),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 2, 0, 0), tz, sensor_chat, "polarity", 34028234663852,
                         None),
                        # SMILE
                        (self._ENTITY, dt.datetime(2020, 2, 2, 1, 0, 0), tz, sensor_smile, "polarity", 1.0, None),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 1, 10, 0), tz, sensor_smile, "polarity", 0.7184, None),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 2, 0, 0), tz, sensor_smile, "polarity", -0.5, None)
                    ])
        # VALIDATE
        t = self._expanse.extract(self._ENTITY,
                                  dt.datetime(2020, 2, 2, 1, 0, 0, 0) + tz,
                                  dt.datetime(2020, 2, 2, 3, 0, 0, 0) + tz)
        ready = Ready(tuples=t)
        valence_all_sensors = ready.valence()
        self.assertEqual(60 + 1, valence_all_sensors.shape[0], "Valence ready FAILED!")
        self.assertTrue(pd.isna(valence_all_sensors["chat_polarity"][chat_polarity_nan_value_index_expected]),
                        "Chat polarity reconstruct DISCARD wrong value FAILED!")

    def test_ready(self):
        # CHAT transition *****************************************************
        sensor = "app.human.chat"
        tz = dt.timedelta(hours=1)
        # INSERT values
        with self._expanse.connection as connection:
            with connection.cursor() as cursor:
                pge.execute_values(
                    cursor,
                    "insert into collected"
                    " (entity, moment, moment_tz, sensor_type, sensor_attribute, sensor_value, sensor_value_extra)"
                    " values %s", [
                        (self._ENTITY, dt.datetime(2020, 2, 2, 1, 0, 0, 0), tz, sensor, "transition", 1.0, "ENTER"),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 1, 1, 0, 0), tz, sensor, "polarity", 0.7184, "pt"),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 1, 2, 0, 0), tz, sensor, "transition", 0.0, "EXIT"),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 1, 3, 0, 0), tz, sensor, "transition", 1.0, "ENTER"),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 1, 4, 0, 0), tz, sensor, "polarity", 0.75, "en"),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 1, 5, 0, 0), tz, sensor, "transition", 0.0, "EXIT"),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 1, 6, 0, 0), tz, sensor, "transition", 1.0, "ENTER"),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 1, 7, 0, 0), tz, sensor, "transition", 0.0, "EXIT"),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 1, 8, 0, 0), tz, sensor, "transition", 1.0, "ENTER"),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 1, 9, 0, 0), tz, sensor, "polarity", 0.6, "en"),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 1, 10, 0, 0), tz, sensor, "polarity", 0.51, None),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 1, 11, 0, 0), tz, sensor, "transition", 0.0, "EXIT")
                    ])
        # VALIDATE
        t = self._expanse.extract(self._ENTITY,
                                  dt.datetime(2020, 2, 2, 1, 0, 0, 0) + tz,
                                  dt.datetime(2020, 2, 2, 2, 0, 0, 0) + tz)
        ready = Ready(tuples=t)
        transition = ready.data(sensor=sensor,
                                attr="transition",
                                fill_limit=ready.reconstruct.MEMORY_FILL_LIMIT_M)
        self.assertEqual(12, transition.shape[0], f"{sensor} transition ready FAILED!")
        transition_explicit = ready.data(sensor=sensor,
                                         attr="transition",
                                         freq="1min",
                                         fill_limit=ready.reconstruct.MEMORY_FILL_LIMIT_M)
        self.assertEqual(12, transition_explicit.shape[0], f"{sensor} transition (explicit freq) ready FAILED!")
        # SMILE ***************************************************************
        self.setUp()
        sensor = "app.human.smile"
        tz = dt.timedelta(hours=1)
        # INSERT values
        with self._expanse.connection as connection:
            with connection.cursor() as cursor:
                pge.execute_values(
                    cursor,
                    "insert into collected"
                    " (entity, moment, moment_tz, sensor_type, sensor_attribute, sensor_value, sensor_value_extra)"
                    " values %s", [
                        (self._ENTITY, dt.datetime(2020, 2, 2, 1, 0, 0, 0), tz, sensor, "polarity", 1.0, None),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 1, 1, 0, 0), tz, sensor, "polarity", 0.7184, None),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 1, 2, 0, 0), tz, sensor, "polarity", 0.0, None),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 1, 3, 0, 0), tz, sensor, "polarity", 1.0, None),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 1, 4, 0, 0), tz, sensor, "polarity", 0.75, None),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 1, 5, 0, 0), tz, sensor, "polarity", -0.5, None),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 1, 6, 0, 0), tz, sensor, "polarity", -1.0, None)
                    ])
        # VALIDATE
        t = self._expanse.extract(self._ENTITY,
                                  dt.datetime(2020, 2, 2, 1, 0, 0, 0) + tz,
                                  dt.datetime(2020, 2, 2, 2, 0, 0, 0) + tz)
        ready = Ready(tuples=t)
        ready_data = ready.data(sensor=sensor,
                                freq="10S",
                                fill_limit=ready.reconstruct.VALENCE_FILL_LIMIT_M * 60 // 10)
        transformed = ready.reconstruct.smile(sensor=sensor,
                                              freq="10S",
                                              fill_limit=ready.reconstruct.VALENCE_FILL_LIMIT_M * 60 // 10)
        self.assertEqual(ready_data.shape, transformed.shape, f"{sensor} ready FAILED!")

    # TODO test_reconstruct_android()
    # TODO test_reconstruct_app_agent()
    # TODO test_reconstruct_app_transition()
    # TODO test_reconstruct_activity()

    def test_reconstruct_chat_polarity(self):
        sensor = "app.human.chat"
        tz = dt.timedelta(hours=1)
        # INSERT values
        with self._expanse.connection as connection:
            with connection.cursor() as cursor:
                pge.execute_values(
                    cursor,
                    "insert into collected"
                    " (entity, moment, moment_tz, sensor_type, sensor_attribute, sensor_value, sensor_value_extra)"
                    " values %s", [
                        (self._ENTITY, dt.datetime(2020, 2, 2, 1, 0, 0, 0), tz, sensor, "transition", 1.0, "ENTER"),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 1, 1, 0, 0), tz, sensor, "polarity", 0.7184, "pt"),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 1, 2, 0, 0), tz, sensor, "transition", 0.0, "EXIT"),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 1, 3, 0, 0), tz, sensor, "transition", 1.0, "ENTER"),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 1, 4, 0, 0), tz, sensor, "polarity", 0.75, "en"),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 1, 5, 0, 0), tz, sensor, "transition", 0.0, "EXIT"),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 1, 6, 0, 0), tz, sensor, "transition", 1.0, "ENTER"),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 1, 7, 0, 0), tz, sensor, "transition", 0.0, "EXIT"),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 1, 8, 0, 0), tz, sensor, "transition", 1.0, "ENTER"),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 1, 9, 0, 0), tz, sensor, "polarity", 0.6, "en"),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 1, 10, 0, 0), tz, sensor, "polarity", 0.51, None),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 1, 11, 0, 0), tz, sensor, "transition", 0.0, "EXIT")
                    ])
        # VALIDATE
        t = self._expanse.extract(self._ENTITY,
                                  dt.datetime(2020, 2, 2, 1, 0, 0, 0) + tz,
                                  dt.datetime(2020, 2, 2, 2, 0, 0, 0) + tz)
        reconstruct = Reconstruct(tuples=t)
        self.assertEqual(12, len(t), "Row count FAILED!")
        polarity = reconstruct.chat_polarity(sensor=sensor,
                                             freq="1min",
                                             fill_limit=reconstruct.VALENCE_FILL_LIMIT_M)
        self.assertEqual(10, polarity.shape[0], f"{sensor} polarity transformation FAILED!")

    def test_reconstruct_smile(self):
        sensor = "app.human.smile"
        tz = dt.timedelta(hours=1)
        # INSERT values
        with self._expanse.connection as connection:
            with connection.cursor() as cursor:
                pge.execute_values(
                    cursor,
                    "insert into collected"
                    " (entity, moment, moment_tz, sensor_type, sensor_attribute, sensor_value, sensor_value_extra)"
                    " values %s", [
                        (self._ENTITY, dt.datetime(2020, 2, 2, 1, 0, 0, 0), tz, sensor, "polarity", 1.0, None),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 1, 1, 0, 0), tz, sensor, "polarity", 0.7184, None),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 1, 2, 0, 0), tz, sensor, "polarity", 0.0, None),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 1, 3, 0, 0), tz, sensor, "polarity", 1.0, None),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 1, 4, 0, 0), tz, sensor, "polarity", 0.75, None),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 1, 5, 0, 0), tz, sensor, "polarity", -0.5, None),
                        (self._ENTITY, dt.datetime(2020, 2, 2, 1, 6, 0, 0), tz, sensor, "polarity", -1.0, None)
                    ])
        t = self._expanse.extract(self._ENTITY,
                                  dt.datetime(2020, 2, 2, 1, 0, 0, 0) + tz,
                                  dt.datetime(2020, 2, 2, 2, 0, 0, 0) + tz)
        self.assertEqual(7, len(t), "Row count FAILED!")
        reconstruct = Reconstruct(tuples=t)
        transformed = reconstruct.smile(sensor=sensor,
                                        freq="10S",
                                        fill_limit=reconstruct.VALENCE_FILL_LIMIT_M * 60 // 10)
        self.assertEqual(7 * 6 - 1 * 6 + 1, transformed.shape[0], f"{sensor} transformation FAILED!")

    # TODO test_reconstruct_tweet()


if __name__ == "__main__":
    unittest.main()
